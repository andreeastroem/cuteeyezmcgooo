#include "stdafx.h"
#include "BTTree.h"
#include "GnollScript.h"
#include "GameObject.h"
#include "BTGnollLeafNodes.h"
#include "ControllerComponent.h"




void PassInformation::Initialise()
{
	m_controller = m_tree->GetData()->GetComponent<ControllerComponent>(Component::CONTROLLER);
	m_script = m_tree->GetData()->GetComponent<GnollScript>(Component::SCRIPT);
	if (!m_script)
		Log::Message("Failed to get GnollScript", Log::ERROR);
}

void PassInformation::Deinitialise()
{

}

BTNode::BTResult PassInformation::Process()
{
	if (CheckConditions() != BTNode::SUCCESS)
		return BTNode::FAILURE;

	std::vector<GnollScript*> gnollsInTalkRange = m_script->GetGnollsOutOfGroup(m_script->GetTalkRange());
	int g, gC = gnollsInTalkRange.size();
	if (gC == 0)
		return BTNode::FAILURE;
	for (g = 0; g < gC; g++)
	{
		gnollsInTalkRange[g]->SpotHero(m_script->GetSpottedPosition());
		gnollsInTalkRange[g]->AddToGroup(m_script->GetOwner());
	}
	return BTNode::SUCCESS;
}
