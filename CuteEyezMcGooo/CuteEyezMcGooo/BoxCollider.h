#pragma once

#include "Collider.h"

class BoxCollider : public Collider
{
public:
	BoxCollider();
	~BoxCollider();

	virtual void EnterCollision(Collider* pCollider) override;
	virtual void LeaveCollision(Collider* pCollider) override;

	bool Initialise(b2World& pWorld, sf::Vector2f pPosition, sf::Vector2f pSize,
		bool pDynamic = false, bool pIsSensor = false, float pDegrees = 0.0f);
	
private:
	b2PolygonShape m_b2shape;
};