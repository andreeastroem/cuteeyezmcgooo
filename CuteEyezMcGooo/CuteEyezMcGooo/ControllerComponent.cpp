#include "stdafx.h"

#include "ServiceLocator.h"
#include "WorldSystem.h"
#include "Time.h"

#include "Collider.h"

#include "ControllerComponent.h"
#include "GameObject.h"

ControllerComponent::ControllerComponent()
{
	m_startValues.damage = 0;
	m_startValues.damaged = false;
	m_startValues.hasTargetPosition = false;
	m_startValues.hp = 1;
	m_startValues.hpLoss = 0;
	m_startValues.position = sf::Vector2f();
	m_startValues.speed = 0;
	m_startValues.speedMod = 1;
	m_startValues.targetPosition = sf::Vector2f();
	m_hasBeenReset = false;

	m_targetObject = nullptr;
	m_level = 0;
	m_experience = 0;
}
ControllerComponent::~ControllerComponent()
{

}

ControllerComponent::EResult ControllerComponent::Move(sf::Vector2f pTargetPosition)
{
	float deltatime = ServiceLocator<Time>::GetService()->GetDeltaTime();
	float dirx = pTargetPosition.x - m_owner->position.x;
	float diry = pTargetPosition.y - m_owner->position.y;

	if (dirx != 0 || diry != 0)
	{
		sf::Vector2f dir = math::Normalise(dirx, diry);


		m_collider->SetVelocity(dir * m_actualValues.speed * m_actualValues.speedMod);
		m_owner->LookTowards(m_owner->position + dir);

		if (ServiceLocator<WorldSystem>::GetService()->SnapToGrid(m_owner->position) !=
			ServiceLocator<WorldSystem>::GetService()->SnapToGrid(pTargetPosition))
		{
			return RUNNING;
		}

		if (ServiceLocator<WorldSystem>::GetService()->SnapToGrid(m_owner->position) ==
			ServiceLocator<WorldSystem>::GetService()->SnapToGrid(m_actualValues.targetPosition))
		{
			m_actualValues.hasTargetPosition = false;
		}
	}

	return SUCCESS;
}
void ControllerComponent::SetVelocity(sf::Vector2f pVelocity)
{
	m_collider->SetVelocity(pVelocity);
}

void ControllerComponent::Initialise(ControllerValues pValues)
{
	m_startValues = pValues;
	m_actualValues = pValues;
	m_targetObject = nullptr;
}

void ControllerComponent::Update()
{
	int q, qC = m_que.size();
	for (q = 0; q < qC; q++)
	{
		m_que[q]->Run();
		delete m_que[q];
	}
	m_que.clear();

	if (!m_collider)
	{
		m_collider = m_owner->GetComponent<Collider>(Component::COLLIDER);
		if (!m_collider)
		{
			Log::Message("Collider is null pointer inside Controller component", Log::ERROR);
		}
	}
}
void ControllerComponent::Cleanup()
{

}

void ControllerComponent::SetTargetPosition(sf::Vector2f pTargetPosition)
{
	if (pTargetPosition.x > 0.0f && pTargetPosition.y > 0.0f)
	{
		m_actualValues.hasTargetPosition = true;
		m_actualValues.targetPosition = pTargetPosition;
	}
}

sf::Vector2f ControllerComponent::GetTargetPosition()
{
	return m_actualValues.targetPosition;
}

void ControllerComponent::SetSpeed(float pSpeed)
{
	m_actualValues.speed = pSpeed;
}

void ControllerComponent::ModifySpeedModifier(float pMod)
{
	m_actualValues.speedMod += pMod;
}

float ControllerComponent::GetSpeed()
{
	return m_actualValues.speed * m_actualValues.speedMod;
}

bool ControllerComponent::IsDamaged()
{
	return m_actualValues.damaged;
}

unsigned int ControllerComponent::GetHPLoss()
{
	m_actualValues.damaged = false;
	return m_actualValues.hpLoss;
}

void ControllerComponent::SetHp(unsigned int pHp)
{
	m_actualValues.hp = pHp;
}

void ControllerComponent::Damage(unsigned int pDmg)
{
	m_actualValues.hpLoss += pDmg;
	m_actualValues.damaged = true;
	if (m_actualValues.hpLoss > m_actualValues.hp)
	{
		m_owner->SetActive(false);
		m_owner->SetPosition(sf::Vector2f(500, -500));
	}
}

void ControllerComponent::Heal(unsigned int pHeal)
{
	if (m_actualValues.hpLoss > 0)
	{
		if (m_actualValues.hpLoss > pHeal)
		{
			m_actualValues.hpLoss -= pHeal;
		}
		else
			m_actualValues.hpLoss = 0;
	}
}

unsigned int ControllerComponent::GetDamage()
{
	return m_actualValues.damage + m_level * m_actualValues.damage;
}

bool ControllerComponent::HasTargetPosition()
{
	return m_actualValues.hasTargetPosition;
}

void ControllerComponent::ArrivedToTargetPosition()
{
	m_actualValues.hasTargetPosition = false;
}

void ControllerComponent::Reset()
{
	m_actualValues = m_startValues;
	m_hasBeenReset = true;
	m_owner->SetPosition(m_startValues.position);
	m_owner->SetActive(true);
	m_targetObject = nullptr;

	m_level = 0;
	m_experience = 0;
}

bool ControllerComponent::BeenReset()
{
	bool temp = m_hasBeenReset;
	m_hasBeenReset = false;
	return temp;
}

void ControllerComponent::SetKnownObject(std::vector<GameObject*> pObjects)
{
	m_knownObjects = pObjects;
}

std::vector<GameObject*> ControllerComponent::GetKnownObjects()
{
	return m_knownObjects;
}

GameObject* ControllerComponent::GetKnownObject()
{
	GameObject* target = nullptr;
	float minDist = 0, dist = 0;
	int o, oC = m_knownObjects.size();
	for (o = 0; o < oC; o++)
	{
		dist = math::Distance(m_owner->position, m_knownObjects[o]->position);
		if (dist < minDist || minDist == 0)
		{
			minDist = dist;
			target = m_knownObjects[o];
		}
	}
	return target;
}

void ControllerComponent::SetTargetObject(GameObject* pObject)
{
	m_targetObject = pObject;
}

GameObject* ControllerComponent::GetTargetObject()
{
	return m_targetObject;
}

unsigned int ControllerComponent::GetLevel()
{
	return m_level;
}

void ControllerComponent::GiveExperience(unsigned int pXP)
{
	m_experience += pXP;
	m_level = m_experience / 50;
}

float ControllerComponent::GetHPPercentage()
{
	if (m_actualValues.hp > m_actualValues.hpLoss)
		return (m_actualValues.hp - m_actualValues.hpLoss) / m_actualValues.hp;
	else return 0.0f;
}
