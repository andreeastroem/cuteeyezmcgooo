#include "stdafx.h"

#include "CollisionSystem.h"

#include "ServiceLocator.h"
#include "Factory.h"
#include "RenderSystem.h"

#include "GameObject.h"

CollisionSystem::CollisionSystem()
{
	b2Vec2 gravity = b2Vec2(0.0f, 0.0f);
	m_world = new b2World(gravity);

	m_contactListener = new ContactListener();
	m_world->SetContactListener(m_contactListener);

	m_debug = new DebugDraw();
	m_world->SetDebugDraw(m_debug);


	m_timestep = 1.0f / 60;
	m_velocityIterations = 6;
	m_positionIterations = 2;

	ServiceLocator<Factory>::GetService()->Setb2World(m_world);
}
CollisionSystem::~CollisionSystem()
{
	Cleanup();
}

/*
----------------------------------------------
public functions
----------------------------------------------
*/
CollisionSystem::ptr CollisionSystem::Create()
{
	return CollisionSystem::ptr(new CollisionSystem());
}
void CollisionSystem::Update()
{
	m_world->Step(m_timestep, m_velocityIterations, m_positionIterations);
}

void CollisionSystem::Cleanup()
{
	if (m_world)
	{
		delete m_world;
		m_world = nullptr;
	}
	if (m_contactListener)
	{
		delete m_contactListener;
		m_contactListener = nullptr;
	}
	if (m_debug)
	{
		m_debug->Clear();
		delete m_debug;
		m_debug = nullptr;
	}

}

void CollisionSystem::DrawDebug()
{
	m_world->DrawDebugData();
	
	/************************************************************************/
	/* WHACK
	/************************************************************************/
	ServiceLocator<RenderSystem>::GetService()->AddToQueue(static_cast<sf::Drawable*>(m_debug), 0 , RenderSystem::GAME);
}

sf::Vector2f CollisionSystem::IntersectRaycast(sf::Vector2f pStart, sf::Vector2f pEnd)
{
	RaycastCallback rccb;
	m_world->RayCast(&rccb, b2Vec2(pStart.x, -pStart.y), b2Vec2(pEnd.x, -pEnd.y));
	sf::Vector2f intersect = rccb.GetIntersectionPoint();
	
	if (math::Distance(intersect) > 0.0f)
		return intersect;
	else
		return pEnd;
}

/*
----------------------------------------------
private functions
----------------------------------------------
*/

/************************************************************************/
/* Contact Listener
/************************************************************************/

ContactListener::ContactListener()
{

}

void ContactListener::BeginContact(b2Contact* pContact)
{
	Collider* colA = nullptr;
	Collider* colB = nullptr;
	void* userdata = pContact->GetFixtureA()->GetBody()->GetUserData();
	if (userdata)
		colA = static_cast<Collider*>(userdata);

	userdata = pContact->GetFixtureB()->GetBody()->GetUserData();
	if (userdata)
		colB = static_cast<Collider*>(userdata);

	if (colA->GetOwner() && colB->GetOwner())
	{
		colA->EnterCollision(colB);
		colB->EnterCollision(colA);
	}

}

void ContactListener::EndContact(b2Contact* pContact)
{
	Collider* colA = nullptr;
	Collider* colB = nullptr;
	void* userdata = pContact->GetFixtureA()->GetBody()->GetUserData();
	if (userdata)
		colA = static_cast<Collider*>(userdata);

	userdata = pContact->GetFixtureB()->GetBody()->GetUserData();
	if (userdata)
		colB = static_cast<Collider*>(userdata);

	if (colA->GetOwner() && colB->GetOwner())
	{
		colA->LeaveCollision(colB);
		colB->LeaveCollision(colA);
	}
}

void DebugDraw::DrawPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color)
{

}

void DebugDraw::DrawSolidPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color)
{
	for (int i = 0; i < vertexCount; i++)
	{
		sf::Vertex vertex = sf::Vertex(sf::Vector2f(vertices[i].x, -vertices[i].y), sf::Color::Blue);
		m_vertices.append(vertex);
	}
}

void DebugDraw::DrawCircle(const b2Vec2& center, float32 radius, const b2Color& color)
{

}

void DebugDraw::DrawSolidCircle(const b2Vec2& center, float32 radius, const b2Vec2& axis, const b2Color& color)
{

}

void DebugDraw::DrawSegment(const b2Vec2& p1, const b2Vec2& p2, const b2Color& color)
{

}

void DebugDraw::DrawTransform(const b2Transform& xf)
{

}

void DebugDraw::draw(sf::RenderTarget& target, sf::RenderStates states) const
{
	states.texture = NULL;

	target.draw(m_vertices, states);
}

DebugDraw::DebugDraw()
{
	m_vertices.setPrimitiveType(sf::PrimitiveType::Quads);
}

void DebugDraw::Clear()
{
	m_vertices.clear();
}
