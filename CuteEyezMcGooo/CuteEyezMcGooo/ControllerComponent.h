//Controls an object (AI or player control) and selects appropriate actions
//Movement etc
#pragma once

#include "Component.h"
#include "Events.h"
#include "SFML/Graphics.hpp"

class Collider;

class ControllerComponent : public Component
{
public:
	enum EResult
	{
		ERROR,
		SUCCESS,
		RUNNING
	};
	struct ControllerValues
	{
		sf::Vector2f position;
		float speed;
		float speedMod;

		bool hasTargetPosition;
		sf::Vector2f targetPosition;

		bool damaged;
		int hpLoss;
		int hp;
		unsigned int damage;
	};

public:
	ControllerComponent();
	~ControllerComponent();

	EResult Move(sf::Vector2f pTargetPosition);
	void SetVelocity(sf::Vector2f pVelocity);

	//needs to be run
	void Initialise(ControllerValues pValues);

	virtual void Update() override;
	virtual void Cleanup() override;

	void SetTargetPosition(sf::Vector2f pTargetPosition);
	sf::Vector2f GetTargetPosition();
	bool HasTargetPosition();
	void ArrivedToTargetPosition();

	virtual void AddToQue(TaskBase* p_task){ m_que.push_back(p_task); };

	void SetSpeed(float pSpeed);
	float GetSpeed();
	void ModifySpeedModifier(float pMod);

	unsigned int GetDamage();
	void Damage(unsigned int pDmg);
	void Heal(unsigned int pHeal);
	bool IsDamaged();
	unsigned int GetHPLoss();
	void SetHp(unsigned int pHp);

	void Reset();
	bool BeenReset();

	void SetKnownObject(std::vector<GameObject*> pObjects);
	std::vector<GameObject*> GetKnownObjects();
	GameObject* GetKnownObject();

	void SetTargetObject(GameObject* pObject);
	GameObject* GetTargetObject();

	unsigned int GetLevel();
	void GiveExperience(unsigned int pXP);

	float GetHPPercentage();
private:
	std::vector<TaskBase*> m_que;

	//values
	ControllerValues m_startValues;
	ControllerValues m_actualValues;
	bool m_hasBeenReset;

	std::vector<GameObject*> m_knownObjects;
	GameObject* m_targetObject;

	unsigned int m_level;
	unsigned int m_experience;
protected:
	Collider* m_collider;
};