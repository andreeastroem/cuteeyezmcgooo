#pragma once

#include "Collider.h"

#include <memory>
#include "Box2D.h"
#include "SFML/Graphics.hpp"

class ContactListener;
class DebugDraw;

class CollisionSystem
{
public:
	typedef std::unique_ptr<CollisionSystem> ptr;
	static ptr Create();
	~CollisionSystem();

	void Update();
	void Cleanup();
	void DrawDebug();

private:
	CollisionSystem();

public:
	sf::Vector2f IntersectRaycast(sf::Vector2f pStart, sf::Vector2f pEnd);

private:
	b2World* m_world;
	ContactListener* m_contactListener;
	DebugDraw* m_debug;

	float32 m_timestep;
	int32 m_velocityIterations;
	int32 m_positionIterations;
};
/************************************************************************/
/* Contact Listener
/************************************************************************/
class ContactListener : public b2ContactListener
{
public:
	ContactListener();

private:

protected:

	void BeginContact(b2Contact* pContact);

	void EndContact(b2Contact* pContact);

};

/************************************************************************/
/* Debug draw
/************************************************************************/
class DebugDraw : public b2Draw, public sf::Drawable
{
public:
	DebugDraw();

	virtual void DrawPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color) override;

	virtual void DrawSolidPolygon(const b2Vec2* vertices, int32 vertexCount, const b2Color& color) override;

	virtual void DrawCircle(const b2Vec2& center, float32 radius, const b2Color& color) override;

	virtual void DrawSolidCircle(const b2Vec2& center, float32 radius, const b2Vec2& axis, const b2Color& color) override;

	virtual void DrawSegment(const b2Vec2& p1, const b2Vec2& p2, const b2Color& color) override;

	virtual void DrawTransform(const b2Transform& xf) override;

	virtual void draw(sf::RenderTarget& target, sf::RenderStates states) const override;

	void Clear();

private:
	sf::VertexArray m_vertices;
};