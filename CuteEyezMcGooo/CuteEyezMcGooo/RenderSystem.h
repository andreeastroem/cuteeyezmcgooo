//Rendersystem
#pragma once

#include <SFML/Graphics.hpp>
#include <memory>

class RenderComponent;

class RenderSystem
{
public:
	enum EViewport
	{
		GAME,
		EDITOR,
		EDITORTOP,
		SIZE
	};
public:			//Functions
	typedef std::unique_ptr<RenderSystem> ptr;
	static ptr Create(sf::RenderWindow& pWindow);

	~RenderSystem();
	void Update();

	sf::RenderWindow* GetRenderWindow(){ return m_window; };
	sf::View* GetView(EViewport p_view){ return m_cameras[p_view]; };
	void AddToQueue(sf::Drawable* pObject, unsigned int pLayer, EViewport pViewport);
	void AddToQueue(std::vector<sf::Drawable*> pObjects, std::vector<unsigned int> pLayers, std::vector<EViewport> pViewports);
	void AddToCameras(sf::View* pCamera);
private:
	RenderSystem(sf::RenderWindow& pWindow);

	void Sort();

	void ClearScreen(sf::Uint8 pRed, sf::Uint8 pGreen, sf::Uint8 pBlue);
	void Draw(sf::Drawable* pObject);
public:			//Variables

private:
	sf::RenderWindow* m_window;
	std::vector<std::vector<sf::Drawable*>*> m_renderQueues;
	std::vector<unsigned int> m_layers;
	std::vector<EViewport> m_renderQueueViewport;
	std::vector<sf::View*> m_cameras;
};