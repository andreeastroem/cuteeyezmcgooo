//Script for the hound, an aggressive monster with an average HP pool
#pragma once 
#include "ScriptComponent.h"

#include "ControllerComponent.h"

class HoundScript : public ScriptComponent
{
public:
	HoundScript();
	~HoundScript();

	void Initialise(unsigned int pHp, unsigned int pDmg);
	//Inherited functions
	virtual void Update() override;
	virtual void Cleanup() override;


private:
	unsigned int m_hp, m_dmg;
	ControllerComponent* m_controller;

protected:
};