#pragma once
#ifndef BUTTON_SCRIPT_H_
#define BUTTON_SCRIPT_H_
#include "ScriptComponent.h"
#include "Events.h"
#include "SFML/Graphics.hpp"
#include "RenderComponent.h"

class ButtonScript : public ScriptComponent
{
public:
	ButtonScript(GameObject* p_owner);
	~ButtonScript(){ Cleanup(); }

	void Update();
	void Cleanup(){ UnRegisterOnClick(); };
	void RegisterOnClick(TaskBase* p_onClick){ if (m_onClick == nullptr){ UnRegisterOnClick(); } m_onClick = p_onClick; };
	void UnRegisterOnClick(){ delete m_onClick; m_onClick = nullptr; };

protected:
private:
	TaskBase* m_onClick;
	RenderSystem* m_render;
	RenderSystem::EViewport m_view;	
	const float m_clickCD = 0.2f;
	float m_lastClick;
};

#endif