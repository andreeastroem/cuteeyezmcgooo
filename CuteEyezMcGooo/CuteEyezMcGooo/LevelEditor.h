#pragma once

#ifndef LEVEL_EDITOR_H_
#define LEVEL_EDITOR_H_
#include "GameObject.h"
#include "ObjectManager.h"
#include "WorldSystem.h"
#include "Factory.h"
#include "Events.h"
#include <vector>

class GameObject;
class Time;


class LevelEditor
{
public:
	LevelEditor();
	~LevelEditor();
	typedef std::unique_ptr<LevelEditor> ptr;
	static ptr Create(){ return ptr(new LevelEditor()); };

	enum EBrushType
	{
		ENVIRONMENTS,
		MONSTERS,
		TRAPS,
		COUNT
	};

	void Update();
	void ChangeBrush(int p_subtype);
	void ChangeToType(EBrushType p_type);
	void Play();
	void Pause();
	void Stop();
	

private:
	struct Brush 
	{
		Brush(GameObject::EObjectType p_type, int p_cost, float p_toughness, bool p_walkable, int p_amout)
		{
			m_type = p_type;
			m_cost = p_cost;
			m_toughness = p_toughness;
			m_walkable = p_walkable;
			m_amout = p_amout;
		};
		Brush()
		{
			m_type = GameObject::WALL;
			m_cost = 1;
		}

		GameObject::EObjectType m_type;
		int m_cost, m_amout;
		float m_toughness;
		bool m_walkable;
	};

	bool m_isPlaying, m_panning, m_leftBtn, m_rightBtn;
	int m_resources;
	const float m_moveSpeed = 100;
	const float m_mouseSense = 5;
	GameObject* m_resourceText;

	int m_brushSize;
	Brush m_brush;
	EBrushType m_brushType;
	int m_subType;
	std::vector<std::vector<Brush>> m_brushes;
	std::vector<std::vector<GameObject*>> m_brushObjects;


	sf::Vector2f m_tileHalf;
	sf::Vector2i m_lastMouse;
	ObjectManager* m_objectManager;
	WorldSystem* m_world;
	Factory* m_factory;
	sf::RenderWindow* m_window;
	RenderSystem* m_renderer;
	sf::View* m_gameView;
	Time* m_time;

	typedef TaskM1<LevelEditor, int> ChangeBrushClick;
	typedef TaskM1<LevelEditor, EBrushType> ChangeBrushTypeClick;

	void Draw();
	void Erase();
	void MoveCamera();
};

#endif