#pragma once
#ifndef BTGNOLL_LEAF_NODE
#define BTGNOLL_LEAF_NODE
#include "BTLeafNode.h"
#include "BTNode.h"

class PassInformation : public BTLeafNode
{
public:

	void Initialise();
	void Deinitialise();
	BTNode::BTResult Process();

private:
	GnollScript* m_script;
	ControllerComponent* m_controller;
};

#endif 