//Base for defining a behaviour tree
//example: attack monster, range for attack -> melee/ranged -> have weapon etc
#pragma once

#include "BTNode.h"

class GameObject;

class BTTree
{
public:
	BTTree();
	~BTTree();

	virtual void Initialise();
	BTNode::BTResult Process();

	void SetRoot(BTNode* pNode);
	void SetData(GameObject* pData);
	GameObject* GetData();

	void SetFlag(bool pState);
	bool GetFlag();
	void Reset();

protected:
	BTNode* m_root;

	bool m_flag;

	GameObject* m_data;
};