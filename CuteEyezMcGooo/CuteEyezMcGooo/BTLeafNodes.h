//List of starting leaf nodes
#pragma once

#include "BTLeafNode.h"
#include "WorldSystem.h"
#include "ControllerComponent.h"

class GameObject;
class GnollScript;
class ObjectManager;
class AISystem;

class BTWalkAlongPath : public BTLeafNode
{
public:
	BTWalkAlongPath();
	~BTWalkAlongPath();

	virtual BTResult Process() override;
	virtual void Initialise() override;
	virtual void Deinitialise() override;

private:
	std::vector<Node*> m_path;
	unsigned int m_index;

	sf::Vector2f m_targetPosition;

	GameObject* m_actor;
};

class BTPickUpItem : public BTLeafNode
{
public:
	BTPickUpItem();
	~BTPickUpItem();

	virtual BTResult Process() override;

	virtual void Initialise() override;
	virtual void Deinitialise() override;


};

class BTAttackEnemy : public BTLeafNode
{
public:
	BTAttackEnemy();
	~BTAttackEnemy();

	virtual BTResult Process() override;

	virtual void Initialise() override;
	virtual void Deinitialise() override;

};

class BTWander : public BTLeafNode
{
public:
	BTWander();
	~BTWander();

	virtual BTResult Process() override;

	virtual void Initialise() override;
	virtual void Deinitialise() override;

private:
	unsigned int width, height;
};

class BTLookForGnolls : public BTLeafNode
{
public:
	BTLookForGnolls();
	~BTLookForGnolls();

	virtual BTResult Process() override;

	virtual void Initialise() override;
	virtual void Deinitialise() override;
private:
	GnollScript* m_script;
	ObjectManager* m_objectManager;
};

class BTGnollWander : public BTLeafNode
{
public:
	BTGnollWander();
	~BTGnollWander();

	virtual void Initialise();

	virtual void Deinitialise();

	virtual BTResult Process() override;

private:
	ControllerComponent* m_controller;
	WorldSystem* m_world;
	AISystem* m_aiSystem;
	GameObject* m_me;
	sf::Vector2f m_targetPosition;
	sf::Vector2f m_direction;
	std::vector<Node*> m_path;
	Node* m_target;
	int m_pathIndex;
};

class BTAttackHero : public BTLeafNode
{
public:
	BTAttackHero(){};
	~BTAttackHero(){};

	virtual void Initialise();
	virtual void Deinitialise();

	virtual BTResult Process() override;
private:
	GnollScript* m_script;
	float m_lastAttack, m_attackRate;
	GameObject* m_hero;
	ControllerComponent* m_heroController;
};