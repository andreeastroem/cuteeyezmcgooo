#include "stdafx.h"
#include "BTSelector.h"
#include "BTLeafNode.h"
#include "BTLeafNodes.h"
#include "BTSequencer.h"
#include "BTTreeGnoll.h"
#include "GnollScript.h"
#include "GameObject.h"
#include "BTGnollLeafNodes.h"
#include "ObjectManager.h"
#include "ServiceLocator.h"
#include "Math.h"
#include "BTTreeWarrior.h"
#include "Collider.h"


BTTreeGnoll::BTTreeGnoll()
{

}

BTTreeGnoll::~BTTreeGnoll()
{

}

void BTTreeGnoll::Initialise()
{
	GameObject* owner = nullptr;
	owner = m_data;
	
	#pragma region Tree
		#pragma region Agressive
			#pragma region EnoughDamageSequence
				//Hero Still There - Attack - Leaf
				BTAttackHero* attack = new BTAttackHero();
				auto LHeroThere = [&](GameObject* owner)->BTNode::BTResult
				{
					GnollScript* gnoll = owner->GetComponent<GnollScript>(Component::SCRIPT);
					sf::Vector2f lastPos = gnoll->GetSpottedPosition();
					sf::Vector2f actPos = ServiceLocator<ObjectManager>::GetService()->GetObjectWithType(GameObject::WARRIOR)->position;
					WorldSystem& world = *ServiceLocator<WorldSystem>::GetService();
					if (world.SnapToGrid(actPos) == world.SnapToGrid(lastPos))
						return BTNode::SUCCESS;

					gnoll->UnSpotHero();
					return BTNode::FAILURE;
				};
				BTTaskL1<decltype(LHeroThere), GameObject*>* THeroThere = new BTTaskL1<decltype(LHeroThere), GameObject*>(LHeroThere, owner);
				attack->SetTree(this);
				attack->AddCondition(THeroThere);

				//Leaf
				BTWalkTo* walkToLast = new BTWalkTo();
				auto LSetWalkToPoint = [&](GameObject* owner)->BTNode::BTResult
				{
					GameObject* warrior = ServiceLocator<ObjectManager>::GetService()->GetObjectWithType(GameObject::WARRIOR);
					if (warrior)
					{
						Collider* warroiroCollider = warrior->GetComponent<Collider>(Component::COLLIDER);

						owner->GetComponent<ControllerComponent>(Component::CONTROLLER)->SetTargetPosition(owner->GetComponent<GnollScript>(Component::SCRIPT)->GetSpottedPosition() + (math::Normalise(warroiroCollider->GetVelocity()) * 5.f));
						return BTNode::SUCCESS;
					}
					return BTNode::FAILURE;
				};
				BTTaskL1<decltype(LSetWalkToPoint), GameObject*>* TSetWalkToPoint = new BTTaskL1<decltype(LSetWalkToPoint), GameObject*>(LSetWalkToPoint, owner);
				walkToLast->AddCondition(TSetWalkToPoint);
				walkToLast->SetTree(this);

			//Group Damage High Enough? - Sequencer
			BTSelector* enoughDamage = new BTSelector();
			auto LEnoughDamage = [&](GameObject* owner)->BTNode::BTResult
			{
				GnollScript* script = owner->GetComponent<GnollScript>(Component::SCRIPT);
				if(script->HasEnoughDamage())
					return BTNode::SUCCESS;
				return BTNode::FAILURE;
			};
			BTTaskL1<decltype(LEnoughDamage), GameObject*>* TEnoughDamage = new BTTaskL1<decltype(LEnoughDamage), GameObject*>(LEnoughDamage, owner);
			enoughDamage->AttachChildNode(attack);
			enoughDamage->AttachChildNode(walkToLast);
			enoughDamage->AddCondition(TEnoughDamage);
			enoughDamage->SetTree(this);
			#pragma endregion
				
			#pragma region LookForGnollsSelector
					//Find closest Gnoll
					BTLookForGnolls* findClosestGnoll = new BTLookForGnolls();
					findClosestGnoll->SetTree(this);
					//Walk to Gnoll
					BTWalkTo* walkToGnoll = new BTWalkTo();
					walkToGnoll->SetTree(this);
				//Sequencer
				BTSequencer* getToGnoll = new BTSequencer();
				getToGnoll->SetTree(this);
				getToGnoll->AttachChildNode(findClosestGnoll);
				getToGnoll->AttachChildNode(walkToGnoll);
				getToGnoll->SetTree(this);

				//Gnoll in Range - Tell Gnoll Hero Position - Leaf
				PassInformation* tellHeroPosition = new PassInformation();
				auto LInRange = [&](GameObject* owner)->BTNode::BTResult
				{
					GnollScript* gnoll = owner->GetComponent<GnollScript>(Component::SCRIPT);
					ControllerComponent* controller = owner->GetComponent<ControllerComponent>(Component::CONTROLLER);
					WorldSystem& world = *ServiceLocator<WorldSystem>::GetService();
					GameObject* target = controller->GetTargetObject();
					if (target && math::Distance(controller->GetTargetObject()->position, owner->position) < gnoll->GetTalkRange())
					{
						controller->SetTargetPosition(controller->GetTargetObject()->position);
						return BTNode::SUCCESS;
					}
					return BTNode::FAILURE;
				};
				BTTaskL1<decltype(LInRange), GameObject*>* TInRange = new BTTaskL1<decltype(LInRange), GameObject*>(LInRange, owner);
				tellHeroPosition->AddCondition(TInRange);
				tellHeroPosition->SetTree(this);
			//Selector
			BTSelector* lookForGnolls = new BTSelector();
			lookForGnolls->AttachChildNode(tellHeroPosition);
			lookForGnolls->AttachChildNode(getToGnoll);
			lookForGnolls->SetTree(this);
			#pragma endregion

		//Aggressive Selector
		BTSelector* agressive = new BTSelector();
		auto LSpottedHero = [&](GameObject* owner)->BTNode::BTResult
		{
			if (owner)
			{
				if (owner->GetComponent<GnollScript>(Component::SCRIPT)->IsHeroSpotted())
					return BTNode::SUCCESS;
			}
			return BTNode::FAILURE;
		};
		BTTaskL1<decltype(LSpottedHero), GameObject*>* TSpottedHero = new BTTaskL1<decltype(LSpottedHero), GameObject*>(LSpottedHero, owner);
		agressive->AddCondition(TSpottedHero);
		agressive->AttachChildNode(enoughDamage);
		agressive->AttachChildNode(lookForGnolls);
		agressive->SetTree(this);
		#pragma endregion

		#pragma region Wander
		//Choose Direction
		BTGnollWander* wander = new BTGnollWander();
		wander->SetTree(this);
		#pragma endregion


	//Root Selector
	BTSelector* root = new BTSelector();
	root->AttachChildNode(agressive);
	root->AttachChildNode(wander);
	root->SetTree(this);

	m_root = root;
	root->Initialise();
	#pragma endregion
}
