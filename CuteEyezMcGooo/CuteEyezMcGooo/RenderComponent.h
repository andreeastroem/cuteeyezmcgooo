//Render component
#pragma once

#include "Component.h"
#include "SFML/Graphics.hpp"
#include "RenderSystem.h"

class RenderComponent : public Component
{
public:
	RenderComponent();
	~RenderComponent();

	//Inherited functions
	virtual void Update() override;
	virtual void Cleanup() override;
	void SetSize(sf::Vector2f p_size){ m_size = p_size; };
	sf::Vector2f GetSize(){ return m_size; };
	
	void SetLayer(unsigned int pLayer);
	unsigned int GetLayer();

	//Pure virtual functions
	virtual sf::Drawable* GetDrawable() = 0;
	virtual sf::Vector2f GetPosition() = 0;
	virtual void SetColour(sf::Color pColour) = 0;
	virtual void SetScale(sf::Vector2f pScale) = 0;
	virtual sf::Vector2f GetScale() = 0;



	RenderSystem::EViewport m_view;

private:
	sf::Vector2f m_size;
protected:
	unsigned int m_layer;
};