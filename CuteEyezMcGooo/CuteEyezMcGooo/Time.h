//Time managing

#include "SFML/System.hpp"
#include <memory>

class Time
{
	Time();
public:
	~Time();
	typedef std::unique_ptr<Time> ptr;
	static ptr Create();

	void Update();
	float GetDeltaTime();

private:
	sf::Clock m_clock;
	sf::Time m_previousTime;
	float m_deltatime;
};