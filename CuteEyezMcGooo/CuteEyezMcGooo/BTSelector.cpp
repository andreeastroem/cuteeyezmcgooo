#include "stdafx.h"

#include "BTTree.h"

#include "BTSelector.h"

#include "GameObject.h"
#include "ControllerComponent.h"

BTSelector::BTSelector()
{
}

BTSelector::~BTSelector()
{

}

BTNode::BTResult BTSelector::Process()
{
	if (CheckConditions() != BTNode::SUCCESS)
		return BTNode::FAILURE;

	m_index = 0;
	while (m_index < m_childNodes.size())
	{
		BTNode::BTResult result = ProcessNode(m_childNodes[m_index]);
		if (result == BTNode::FAILURE)
			m_index++;
		else
			return result;
	}
	return BTNode::FAILURE;
}

void BTSelector::Initialise()
{
	m_currentNode = nullptr;
}
