#include "stdafx.h"

#include "TextComponent.h"
#include "GameObject.h"

#include "ServiceLocator.h"
#include "ResourceManager.h"

TextComponent::TextComponent()
{
	sf::Font* font = ServiceLocator<ResourceManager>::GetService()->LoadFont("FSEX300.ttf");
	m_text = new sf::Text;
	m_text->setFont(*font);
}

TextComponent::~TextComponent()
{
	if (m_text)
	{
		delete m_text;
		m_text = nullptr;
	}
}

sf::Drawable* TextComponent::GetDrawable()
{
	return m_text;
}

sf::Vector2f TextComponent::GetPosition()
{
	return m_text->getPosition();
}

void TextComponent::SetColour(sf::Color pColour)
{
	m_text->setColor(pColour);
}

void TextComponent::SetScale(sf::Vector2f pScale)
{
	m_text->setScale(pScale);
}

sf::Vector2f TextComponent::GetScale()
{
	return m_text->getScale();
}

void TextComponent::Update()
{
	if (m_text->getString().isEmpty())
		m_text->setString(m_owner->name);
}

void TextComponent::SetPosition(sf::Vector2f pPosition)
{
	m_text->setPosition(pPosition);
}
