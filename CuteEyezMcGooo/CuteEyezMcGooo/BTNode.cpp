#include "stdafx.h"
#include "Events.h"
#include "BTTree.h"
#include "BTNode.h"


BTNode::BTNode()
{
}
BTNode::~BTNode()
{
	int c, cC = m_conditions.size();
	for (c = 0; c < cC; c++)
		delete m_conditions[c];
}

void BTNode::SetTree(BTTree* pTree)
{
	m_tree = pTree;
}

void BTNode::Initialise()
{

}

void BTNode::Deinitialise()
{

}

void BTNode::AddCondition(BehaviourTaskBase* p_condition)
{
	/*auto name = [&]("Argument", "Argument")->"ReturnType"
	{

	}; */
	m_conditions.push_back(p_condition); 
}

BTNode::BTResult BTNode::CheckConditions()
{
	int c, cC = m_conditions.size();
	for (c = 0; c < cC; c++)
	{
		BTResult r = m_conditions[c]->Run();
		if (r == FAILURE)
			return r;
	}
	return SUCCESS;
}

std::string BTNode::GetName()
{
	return m_name;
}

void BTNode::SetName(std::string pName)
{
	m_name = pName;
}

void BTNode::Reset()
{
	Deinitialise();
}
