#include "stdafx.h"
#include "Time.h"

Time::Time()
{
	m_clock.restart();
}

Time::~Time()
{

}

Time::ptr Time::Create()
{
	return Time::ptr(new Time());
}
void Time::Update()
{
	//Create a temporary time variable
	sf::Time tempTime = m_clock.getElapsedTime();

	//set the delta time
	m_deltatime = tempTime.asSeconds() - m_previousTime.asSeconds();

	//Change the previous time variable
	m_previousTime = tempTime;
}

float Time::GetDeltaTime()
{
	return m_deltatime;
}

