//A component for showing a single texture (or part of a texture)
#pragma once

#include "RenderComponent.h"

class SpriteComponent : public RenderComponent
{
public:
	SpriteComponent();
	~SpriteComponent();

	void Initialise(unsigned int pTexture, sf::Vector2f pScale = sf::Vector2f(1, 1));

	virtual void Update() override;
	virtual void Cleanup() override;
	virtual sf::Drawable* GetDrawable() override;
	
	unsigned int GetTexture();

	virtual void SetPosition(sf::Vector2f pPosition) override;
	virtual void SetRotation(float pRotation) override;

	virtual sf::Vector2f GetPosition() override;
	virtual void SetColour(sf::Color pColour) override;
	virtual void SetScale(sf::Vector2f pScale) override;

	virtual sf::Vector2f GetScale() override;

private:
	sf::Sprite* m_sprite;
	unsigned int m_textureID;
};