#include "stdafx.h"
#include "Hound.h"

#include "GameObject.h"

HoundScript::HoundScript()
{

}

HoundScript::~HoundScript()
{

}

void HoundScript::Initialise(unsigned int pHp, unsigned int pDmg)
{
	m_hp = pHp;
	m_dmg = pDmg;

	m_controller = m_owner->GetComponent<ControllerComponent>(Component::CONTROLLER);

	m_controller->SetHp(m_hp);
	m_controller->SetSpeed(100);
}
void HoundScript::Update()
{

}
void HoundScript::Cleanup()
{
	throw std::logic_error("The method or operation is not implemented.");
}

