#include "stdafx.h"

#include "BTNodeFactory.h"
#include "BTTreeGnoll.h"

/************************************************************************/
/* Composite nodes
/************************************************************************/
#include "BTSelector.h"
#include "BTSequencer.h"

/************************************************************************/
/* Decorator nodes
/************************************************************************/

/************************************************************************/
/* Leaf nodes
/************************************************************************/
#include "BTLeafNodes.h"

/************************************************************************/
/* BT trees
/************************************************************************/
#include "BTTreeWarrior.h"

/************************************************************************/
/* debug
/************************************************************************/
#include <iostream>

BTNodeFactory::BTNodeFactory()
{

}
BTNodeFactory::~BTNodeFactory()
{

}

BTNodeFactory::ptr BTNodeFactory::Create()
{
	return BTNodeFactory::ptr(new BTNodeFactory);
}


BTTree* BTNodeFactory::CreateBTTree(AISystem::EBTType pType, GameObject* pOwner)
{
	BTTree* tree = nullptr;

	switch (pType)
	{
	case AISystem::WARRIORHERO:
		/*std::cout << "Create BT tree: " << pOwner->GetType() << std::endl;*/
		return CreateWarriorHero(tree, pOwner);
		break;
	case AISystem::GNOLL:
		return CreateGnollTree(tree, pOwner);
	default:
		printf("default\n");
		break;
	}

	Log::Message("Error creating tree", Log::ERROR);

	return nullptr;
}

BTTree* BTNodeFactory::CreateWarriorHero(BTTree* pTree, GameObject* pOwner)
{
//	pTree = new BTTree;
//	pTree->SetData(pOwner);
//
//	if (pOwner->GetType() == GameObject::WALL)
//	{
//		printf("facck me\n");
//	}
//
//	BTSelector* selectorRoot = new BTSelector;
//	selectorRoot->SetTree(pTree);
//
//	BTWalkAlongPath* walk = new BTWalkAlongPath;
//	walk->SetTree(pTree);
//
//	selectorRoot->AttachChildNode(walk);
//
//	BTWander* wander = new BTWander;
//	wander->SetTree(pTree);
//
//	selectorRoot->AttachChildNode(wander);
//
//	//Add nodes to tree
//	pTree->SetRoot(selectorRoot);

	pTree = new BTTreeWarrior();
	pTree->SetData(pOwner);
	pTree->Initialise();

	return pTree;
}

BTTree* BTNodeFactory::CreateGnollTree(BTTree* p_tree, GameObject* p_owner)
{
	if (p_owner)
	{
		p_tree = new BTTreeGnoll();
		p_tree->SetData(p_owner);
		p_tree->Initialise();
	}
	return p_tree;
}
