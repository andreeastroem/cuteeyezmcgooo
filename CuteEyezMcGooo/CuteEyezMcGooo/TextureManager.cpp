#include "stdafx.h"

#include "TextureManager.h"

TextureManager::TextureManager()
{

}
TextureManager::TextureManager(const std::string& pDirectory)
{
	m_directory = pDirectory;
}
TextureManager::~TextureManager()
{

}

TextureManager::ptr TextureManager::Create(const std::string& pDirectory)
{
	return TextureManager::ptr(new TextureManager(pDirectory));
}

int TextureManager::LoadTexture(const std::string& pFilename)
{
	for (unsigned int i = 0; i < m_filenames.size(); i++)
		if (pFilename == m_filenames[i])
			return i;
	Log::Message("Texture not already loaded, trying to load.", Log::NORMAL);

	sf::Texture* texture = new sf::Texture;
	std::string path = m_directory + pFilename + ".png";
	std::string message;

	if (texture->loadFromFile(path))
	{
		m_filenames.push_back(pFilename);
		m_textures.push_back(texture);
		message = "Loaded texture from path: " + path;
		Log::Message(message.c_str(), Log::SUCCESSFUL);
		return m_textures.size() - 1;
	}
	else
	{
		message = "Could not load texture from path: " + path;
		Log::Message(message.c_str(), Log::ERROR);

		texture = nullptr;
		return -1;
	}
}

sf::Texture* TextureManager::GetTexture(unsigned int pIndex)
{
	if (pIndex < m_textures.size())
		return m_textures[pIndex];
	else
	{
		Log::Message("Texture index was too big, try a smaller one", Log::ERROR);
		return nullptr;
	}
}

void TextureManager::Cleanup()
{
	for (unsigned int i = 0; i < m_textures.size(); i++)
	{
		delete m_textures[i];
		m_textures[i] = nullptr;
	}
}