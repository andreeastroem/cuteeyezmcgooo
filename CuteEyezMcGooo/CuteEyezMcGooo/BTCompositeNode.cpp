#include "stdafx.h"

#include "BTCompositeNode.h"

BTCompositeNode::BTCompositeNode()
{
}
BTCompositeNode::~BTCompositeNode()
{
	for (unsigned int i = 0; i < m_childNodes.size(); i++)
	{
		delete m_childNodes[i];
		m_childNodes[i] = nullptr;
	}
}

BTNode::BTResult BTCompositeNode::Process()
{
	return BTNode::FAILURE;
}

void BTCompositeNode::AttachChildNode(BTNode* pNode)
{
	m_childNodes.push_back(pNode);
}

BTNode::BTResult BTCompositeNode::ProcessNode(BTNode* pNode)
{
	if (!m_currentNode)
	{
		m_currentNode = pNode;
		m_currentNode->Initialise();
	}

	BTNode::BTResult result = pNode->Process();


	if (result != BTNode::FAILURE && m_currentNode != pNode)
	{
		if (m_currentNode)
			m_currentNode->Deinitialise();
		pNode->Initialise();
		m_currentNode = pNode;
	}

	if (result != BTNode::RUNNING && m_currentNode == pNode)
	{
		m_currentNode->Deinitialise();
		m_currentNode = nullptr;
		return result;
	} 
	
	return result;
}

void BTCompositeNode::Reset()
{
	int c, cC = m_childNodes.size();
	for (c = 0; c < cC; c++)
		m_childNodes[c]->Reset();

	Deinitialise();
	m_currentNode = nullptr;
}
