#include "stdafx.h"

#include "SpriteComponent.h"
#include "GameObject.h"

#include "ServiceLocator.h"
#include "TextureManager.h"

SpriteComponent::SpriteComponent()
{

}
SpriteComponent::~SpriteComponent()
{

}

void SpriteComponent::Initialise(unsigned int pTexture, sf::Vector2f pScale)
{
	m_textureID = pTexture;
	m_sprite = new sf::Sprite;
	m_sprite->setTexture(*ServiceLocator<TextureManager>::GetService()->GetTexture(pTexture));
	m_sprite->setOrigin(m_sprite->getTexture()->getSize().x / 2.0f, m_sprite->getTexture()->getSize().y / 2.0f);
	m_sprite->setScale(pScale);
}
void SpriteComponent::Update()
{
	
	
}
void SpriteComponent::Cleanup()
{
	Component::Cleanup();

	if (m_sprite)
	{
		delete m_sprite;
		m_sprite = nullptr;
	}
}

sf::Drawable* SpriteComponent::GetDrawable()
{
	return m_sprite;
}

unsigned int SpriteComponent::GetTexture()
{
	return m_textureID;
}

void SpriteComponent::SetPosition(sf::Vector2f pPosition)
{
	m_sprite->setPosition(pPosition);
}

void SpriteComponent::SetRotation(float pRotation)
{
	m_sprite->setRotation(pRotation);
}

sf::Vector2f SpriteComponent::GetPosition()
{
	return m_sprite->getPosition();
}

void SpriteComponent::SetColour(sf::Color pColour)
{
	m_sprite->setColor(pColour);
}

void SpriteComponent::SetScale(sf::Vector2f pScale)
{
	m_sprite->setScale(pScale);
}

sf::Vector2f SpriteComponent::GetScale()
{
	return m_sprite->getScale();
}


