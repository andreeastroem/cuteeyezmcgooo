//Completes nodes from first to last
#pragma once

#include "BTCompositeNode.h"

class BTSequencer : public BTCompositeNode
{
public:
	BTSequencer();
	~BTSequencer();

	virtual BTResult Process() override;

	virtual void Initialise() override;

	virtual void Deinitialise() override;

private:
	unsigned int m_index;
};