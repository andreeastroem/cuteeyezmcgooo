#include "stdafx.h"

#include "BTTree.h"
#include "Collider.h"
#include "ControllerComponent.h"

#include "WarriorScript.h"

#include "ServiceLocator.h"
#include "Factory.h"
#include "AISystem.h"
#include "ObjectManager.h"
#include "Time.h"

WarriorScript::WarriorScript()
{
	m_senseTimer = 0.0f;
	m_senseInterval = 0.1f;
	bool sense = true;
	m_senseRange = 500.0f;
	m_winText = nullptr;

}

WarriorScript::~WarriorScript()
{

}

void WarriorScript::Initialise(BTTree* pTree, sf::Vector2f pStartPosition)
{
	m_BTtree = pTree;
	m_startPosition = pStartPosition;
	m_pickedUpGold = false;
}

void WarriorScript::Update()
{
	if (!m_gold)
		m_gold = ServiceLocator<ObjectManager>::GetService()->GetObjectWithType(GameObject::GOLD);
	if (!m_controller)
	{
		m_controller = m_owner->GetComponent<ControllerComponent>(Component::CONTROLLER);
		ControllerComponent::ControllerValues values;
		values.damage = 10;
		values.damaged = false;
		values.hasTargetPosition = false;
		values.hp = 100;
		values.hpLoss = 0;
		values.position = m_startPosition;
		values.speed = 100;
		values.speedMod = 1;
		values.targetPosition = sf::Vector2f();
		m_controller->Initialise(values);
		m_controller->GiveExperience(250);
	}
	if (!m_collider)
		m_collider = m_owner->GetComponent<Collider>(Component::COLLIDER);

	bool foundGold = false;
	unsigned int goldIndex = 0;

	if (m_sense)
	{
		//sensing
		//printf("sensing\n");
		m_sensedObjects = ServiceLocator<ObjectManager>::GetService()->
			GetObjectsWithinRadius(m_owner->position, m_senseRange);

		//can I see any of the objects I saw last time I sensed?
		unsigned int index = 0;
		while (index < m_knownObjects.size())
		{
			if (!ServiceLocator<AISystem>::GetService()->CanSee(*m_collider->GetBody(), m_owner->rotation, m_knownObjects[index]->position, 90)
				&& m_knownObjects[index])
				m_knownObjects.erase(m_knownObjects.begin() + index);
			else
				index++;
		}
		for (unsigned int i = 0; i < m_sensedObjects.size(); i++)
		{
			if (m_sensedObjects[i] != m_owner && m_sensedObjects[i]->GetActive() &&
				ServiceLocator<AISystem>::GetService()->CanSee(*m_collider->GetBody(), m_owner->rotation, m_sensedObjects[i]->position, 90))
			{
				bool isAlreadyKnown = false;
				//Make sure it has not already been sensed or put in the known pile
				for (unsigned int j = 0; j < m_knownObjects.size(); j++)
				{
					if (m_knownObjects[j] == m_sensedObjects[i])
					{
						isAlreadyKnown = true;
						break;
					}
				}
				if (!isAlreadyKnown && m_sensedObjects[i] != m_owner && m_collider->CanSee(m_owner->position, m_sensedObjects[i]->position, m_sensedObjects[i]->GetType()))
				{
					m_knownObjects.push_back(m_sensedObjects[i]);
					if (m_sensedObjects[i]->GetType() == GameObject::GOLD)
					{
						foundGold = true;
						goldIndex = i;
						m_gold = m_sensedObjects[i];
					}
				}

				m_sense = false;

			}
		}
		m_controller->SetKnownObject(m_knownObjects);
	}
	else
	{
		m_senseTimer += ServiceLocator<Time>::GetService()->GetDeltaTime();
		if (m_senseTimer > m_senseInterval)
		{
			m_senseTimer = 0.0f;
			m_sense = true;
		}
	}

	if (foundGold)
	{
		m_controller->SetTargetPosition(m_sensedObjects[goldIndex]->position);
	}
}

void WarriorScript::Cleanup()
{
	m_BTtree->SetFlag(true);
	m_BTtree = nullptr;
	
	for each (GameObject* object in m_knownObjects)
	{
		object = nullptr;
	}
	for each (GameObject* object in m_sensedObjects)
	{
		object = nullptr;
	}
}

void WarriorScript::OnCollision(GameObject* pOther)
{
// 	if (pOther->GetType() == GameObject::GOLD && pOther->GetActive())
// 	{
// 		m_treasure += 2000;
// 		pOther->SetPosition(sf::Vector2f(0, -200));
// 		pOther->SetActive(false);
// 	}
}

void WarriorScript::ExitCollision(GameObject* pOther)
{
	/*if (pOther->GetType() == GameObject::GOLD)
	{
	pOther->SetFlag(true);
	m_gold = nullptr;
	}*/
}

bool WarriorScript::FoundGold()
{
	if (m_gold)
		return true;
	return false;
}

GameObject* WarriorScript::GetGold()
{
	return m_gold;
}

void WarriorScript::Reset()
{
	if (m_controller)
		m_controller->GiveExperience(250);
	m_pickedUpGold = false;
	if (m_winText)
		ServiceLocator<ObjectManager>::GetService()->DeleteGameObject(m_winText);
}

void WarriorScript::SetActive(bool pState)
{
	m_activeFlag = pState;
	if (!m_activeFlag)
		m_winText = ServiceLocator<Factory>::GetService()->CreateGameObject(GameObject::TEXT, m_owner->position + sf::Vector2f(0, 50), "You Win!");
}
