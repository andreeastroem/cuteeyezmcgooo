//Logic for GameObject
//
#pragma once

#include "Component.h"

class ScriptComponent : public Component
{
public:
	ScriptComponent();
	~ScriptComponent();

	virtual void Update() = 0;
	virtual void Cleanup() = 0;
private:

protected:

};