#include "stdafx.h"

#include "BTSequencer.h"

BTSequencer::BTSequencer()
{
	m_index = 0;
}

BTSequencer::~BTSequencer()
{

}

BTNode::BTResult BTSequencer::Process()
{
	if (CheckConditions() != BTNode::SUCCESS)
		return BTNode::FAILURE;

	while (m_index < m_childNodes.size())
	{
		BTNode::BTResult result = ProcessNode(m_childNodes[m_index]);
		
		if (result == BTNode::SUCCESS)
		{
			m_index++;
			return BTNode::RUNNING;
		}
		return result;
	}

	return BTNode::SUCCESS;
}

void BTSequencer::Initialise()
{
	m_index = 0;
	m_currentNode = nullptr;
}

void BTSequencer::Deinitialise()
{
	m_index = 0;
}
