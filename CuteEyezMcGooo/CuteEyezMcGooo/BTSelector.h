//Given a condition, calculates best option to do
#pragma once

#include "BTCompositeNode.h"
#include "Events.h"

class BTSelector : public BTCompositeNode
{
public:
	BTSelector();
	~BTSelector();

	
	virtual BTResult Process() override;
	virtual void Initialise() override;

private:
	unsigned int m_index;
};