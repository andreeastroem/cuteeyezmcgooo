//Component for drawing a rectangle

#pragma once

#include "RenderComponent.h"

class RectangleComponent : public RenderComponent
{
public:
	RectangleComponent();
	~RectangleComponent();

	void Initialise(sf::Vector2f pSize, sf::Color pColour);

	//Inherited functions
	virtual void Update() override;
	virtual void Cleanup() override;
	virtual sf::Drawable* GetDrawable() override;

	virtual void SetPosition(sf::Vector2f pPosition) override;
	virtual void SetRotation(float pRotation) override;

	virtual sf::Vector2f GetPosition() override;
	virtual void SetColour(sf::Color pColour) override;
	virtual void SetScale(sf::Vector2f pScale) override;
	virtual sf::Vector2f GetScale() override;

private:

protected:
	sf::RectangleShape* m_shape;
};