//Math class for utility functions for multidimensional vectors etc
#pragma once

#include "SFML/Graphics.hpp"

class math
{
public:
	static float Distance(sf::Vector2f pVector2);
	static float Distance(float x, float y);
	static float Distance(sf::Vector2f pStart, sf::Vector2f pEnd);

	static sf::Vector2f Normalise(float pX, float pY);
	static sf::Vector2f Normalise(sf::Vector2f pVector2);

	static float Dot(sf::Vector2f& p_a, sf::Vector2f& p_b);

	static float RADTODEG(float pRadians);
	static float DEGTORAD(float pDegrees);
};