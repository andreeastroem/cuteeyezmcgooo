//Application
#pragma once

#include <SFML/Graphics.hpp>

/************************************************************************/
/* Systems
/************************************************************************/
#include "RenderSystem.h"
#include "ObjectManager.h"
#include "CollisionSystem.h"
#include "TextureManager.h"
#include "Factory.h"
#include "LevelEditor.h"
#include "BTNodeFactory.h"
#include "Time.h"
#include "ResourceManager.h"

class Application
{
public:		//functions
	Application();
	~Application();

	bool Initialise();
	void Run();
	void Cleanup();

private:
	void HandleEvent(sf::Event& pEvent);

public:		//variables
	
private:
	sf::RenderWindow m_window;
	bool m_running;

	//Systems
	RenderSystem::ptr m_renderSystem;
	ObjectManager::ptr m_objectManager;
	CollisionSystem::ptr m_collisionSystem;
	TextureManager::ptr m_textureManager;
	Factory::ptr m_factory;
	LevelEditor::ptr m_editor;
	BTNodeFactory::ptr m_nodeFactory;
	Time::ptr m_clock;
	ResourceManager::ptr m_resourceManager;
};