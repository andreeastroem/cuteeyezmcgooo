#include "stdafx.h"

#include "TrollScript.h"

TrollScript::TrollScript()
{

}

TrollScript::~TrollScript()
{

}

void TrollScript::Update()
{
	throw std::logic_error("The method or operation is not implemented.");
}

void TrollScript::Cleanup()
{
	throw std::logic_error("The method or operation is not implemented.");
}

void TrollScript::OnCollision(GameObject* pOther)
{
	throw std::logic_error("The method or operation is not implemented.");
}

void TrollScript::ExitCollision(GameObject* pOther)
{
	throw std::logic_error("The method or operation is not implemented.");
}

