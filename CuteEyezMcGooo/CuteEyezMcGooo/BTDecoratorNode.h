//A node that manipulates the result of their child nodes
//Inverter, repeater, succeeder, repeater until fail etc
#pragma once

#include "BTNode.h"

class BTDecoratorNode : public BTNode
{
public:
	BTDecoratorNode();
	~BTDecoratorNode();

	virtual BTResult Process() override;
	virtual BTResult ProcessNode(BTNode* pNode);

private:
	BTNode* m_child;
};