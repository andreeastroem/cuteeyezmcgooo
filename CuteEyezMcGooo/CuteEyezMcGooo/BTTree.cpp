#include "stdafx.h"

#include "BTTree.h"
#include "GameObject.h"

BTTree::BTTree()
{
	m_flag = false;
	m_data = nullptr;
}

BTTree::~BTTree()
{
	if (m_root)
	{
		delete m_root;
		m_root = nullptr;
	}

	if (m_data)
		m_data = nullptr;
}

void BTTree::Initialise()
{
	
}

BTNode::BTResult BTTree::Process()
{
	BTNode::BTResult result = m_root->Process();

	return result;;
}

void BTTree::SetData(GameObject* pData)
{
	m_data = pData;
}

GameObject* BTTree::GetData()
{
	return m_data;
}

void BTTree::SetRoot(BTNode* pNode)
{
	m_root = pNode;
}

void BTTree::SetFlag(bool pState)
{
	m_flag = pState;
}
bool BTTree::GetFlag()
{
	return m_flag;
}

void BTTree::Reset()
{
	m_root->Reset();
}
