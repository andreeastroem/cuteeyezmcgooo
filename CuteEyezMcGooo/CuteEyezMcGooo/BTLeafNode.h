//This is the node that defines an action that the agent makes
//Could be defined as verbs
//walk, open, jump, attack, flee, etc
#pragma once

#include "BTNode.h"

class BTLeafNode : public BTNode
{
public:
	BTLeafNode();
	~BTLeafNode();

	virtual void Initialise() = 0;
	virtual void Deinitialise() = 0;
	virtual BTResult Process() override;

private:

};