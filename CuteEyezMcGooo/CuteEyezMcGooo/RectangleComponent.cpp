#include "stdafx.h"

#include "RectangleComponent.h"
#include "GameObject.h"

RectangleComponent::RectangleComponent()
{

}

RectangleComponent::~RectangleComponent()
{

}

void RectangleComponent::Update()
{
	m_shape->setPosition(m_owner->position);
}

void RectangleComponent::Cleanup()
{
	Component::Cleanup();

	if (m_shape)
	{
		delete m_shape;
		m_shape = nullptr;
	}
}

sf::Drawable* RectangleComponent::GetDrawable()
{
	return m_shape;
}

void RectangleComponent::Initialise(sf::Vector2f pSize, sf::Color pColour)
{
	m_shape = new sf::RectangleShape(pSize);
	m_shape->setOrigin(pSize.x / 2, pSize.y / 2);
	m_shape->setFillColor(pColour);
}

void RectangleComponent::SetPosition(sf::Vector2f pPosition)
{
	m_shape->setPosition(pPosition);
}

void RectangleComponent::SetRotation(float pRotation)
{
	m_shape->setRotation(pRotation);
}

sf::Vector2f RectangleComponent::GetPosition()
{
	return m_shape->getPosition();
}

void RectangleComponent::SetColour(sf::Color pColour)
{
	m_shape->setFillColor(pColour);
}

void RectangleComponent::SetScale(sf::Vector2f pScale)
{
	m_shape->setScale(pScale);
}

sf::Vector2f RectangleComponent::GetScale()
{
	return m_shape->getScale();
}


