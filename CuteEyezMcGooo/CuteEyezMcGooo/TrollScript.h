//Script for troll monster
#pragma once

#include "ScriptComponent.h"

class TrollScript : public ScriptComponent
{
public:
	TrollScript();
	~TrollScript();

	virtual void Update() override;

	virtual void Cleanup() override;

	virtual void OnCollision(GameObject* pOther) override;

	virtual void ExitCollision(GameObject* pOther) override;


private:

protected:

};