#include "stdafx.h"

#include "ObjectManager.h"

#include "ServiceLocator.h"
#include "RenderComponent.h"
#include "RenderSystem.h"
#include "AISystem.h"

#include "ControllerComponent.h"

ObjectManager::ptr ObjectManager::Create()
{
	return ObjectManager::ptr(new ObjectManager);
}

ObjectManager::~ObjectManager()
{

}

void ObjectManager::Update()
{
	//TEMP USAGE WILL BE IMPROVED
	std::vector<std::vector<unsigned int>> deathFlagged;
	for (unsigned int i = 0; i < RenderSystem::SIZE; i++)
	{
		std::vector<unsigned int> woop;
		deathFlagged.push_back(woop);
	}

	std::vector<GameObject*>* objs = m_objects[RenderSystem::GAME];
	if (!m_pause)
	{
		for (unsigned int i = 0; i < objs->size(); i++)
		{
			if (objs->at(i)->GetActive())
			{
				objs->at(i)->Update();

				RenderComponent* render = objs->at(i)->GetComponent<RenderComponent>(Component::RENDER);
				if (render)
				{
					ServiceLocator<RenderSystem>::GetService()->AddToQueue(render->GetDrawable(), render->GetLayer(), render->m_view);
				}
			}
			if (objs->at(i)->GetFlag())
				deathFlagged[RenderSystem::GAME].push_back(i);
		}
	}
	else
	{
		for (unsigned int i = 0; i < objs->size(); i++)
		{
			if (objs->at(i)->GetActive())
			{
				RenderComponent* render = objs->at(i)->GetComponent<RenderComponent>(Component::RENDER);
				if (render)
				{
					ServiceLocator<RenderSystem>::GetService()->AddToQueue(render->GetDrawable(), render->GetLayer(), render->m_view);
				}
			}
			if (objs->at(i)->GetFlag())
				deathFlagged[RenderSystem::GAME].push_back(i);
			if (objs->at(i)->GetType() == GameObject::TEXTEFFECT)
				objs->at(i)->Update();
		}
	}
	objs = m_objects[RenderSystem::EDITOR];
	for (unsigned int i = 0; i < objs->size(); i++)
	{
		if (objs->at(i)->GetActive())
		{
			objs->at(i)->Update();

			RenderComponent* render = objs->at(i)->GetComponent<RenderComponent>(Component::RENDER);
			if (render)
			{
				ServiceLocator<RenderSystem>::GetService()->AddToQueue(render->GetDrawable(), render->GetLayer(), render->m_view);
			}
		}
		if (objs->at(i)->GetFlag())
			deathFlagged[RenderSystem::GAME].push_back(i);
	}
	objs = m_objects[RenderSystem::EDITORTOP];
	for (unsigned int i = 0; i < objs->size(); i++)
	{
		if (objs->at(i)->GetActive())
		{
			objs->at(i)->Update();

			RenderComponent* render = objs->at(i)->GetComponent<RenderComponent>(Component::RENDER);
			if (render)
			{
				ServiceLocator<RenderSystem>::GetService()->AddToQueue(render->GetDrawable(), render->GetLayer(), render->m_view);
			}
		}
		if (objs->at(i)->GetFlag())
			deathFlagged[RenderSystem::GAME].push_back(i);
	}

	ServiceLocator<RenderSystem>::GetService()->Update();


	for (unsigned int i = 0; i < deathFlagged.size(); i++)
	{
		for (int j = deathFlagged[i].size(); j > 0; j--)
		{
			DeleteGameObject(deathFlagged[i][j - 1], i);
			//m_objects[i]->at(flagged[0][j - 1])->SetActive(false);
		}
	}
}

void ObjectManager::Cleanup()
{
	for (unsigned int i = 0; i < m_objects.size(); i++)
	{
		for (unsigned int j = m_objects[i]->size(); j > 0; j++)
		{
			DeleteGameObject(i, j);
		}
	}
}

ObjectManager::ObjectManager()
{
	Cleanup();
	for (unsigned int i = 0; i < RenderSystem::EViewport::SIZE; i++)
	{
		m_objects.push_back(new std::vector<GameObject*>);
	}
}

void ObjectManager::AttachGameObject(GameObject* pObject)
{
	RenderSystem::EViewport view = pObject->GetComponent<RenderComponent>(Component::RENDER)->m_view;
	m_objects[view]->push_back(pObject);
}

void ObjectManager::DeleteGameObject(GameObject* pObject, unsigned int pView)
{
	int o, oC = m_objects[pView]->size();
	for (o = 0; o < oC; o++)
	{
		if (m_objects[pView]->at(o) == pObject)
		{
			DeleteGameObject(o, pView);
			break;
		}
	}
}

void ObjectManager::DeleteGameObject(unsigned int pIndex, unsigned int pView)
{
	m_objects[pView]->at(pIndex)->Cleanup();
	delete m_objects[pView]->at(pIndex);
	m_objects[pView]->at(pIndex) = nullptr;
	m_objects[pView]->erase(m_objects[pView]->begin() + pIndex);
}

GameObject* ObjectManager::GetObjectWithType(GameObject::EObjectType pType)
{
	for (unsigned int i = 0; i < m_objects[0]->size(); i++)
	{
		if (m_objects[0]->at(i)->GetType() == pType && m_objects[0]->at(i)->GetActive())
			return m_objects[0]->at(i);
	}
	return nullptr;
}

std::vector<GameObject*> ObjectManager::GetObjectsWithinRadius(sf::Vector2f pPos, float pRadius)
{
	std::vector<GameObject*> acceptableObjects;
	for (unsigned int i = 0; i < m_objects[0]->size(); i++)
	{
		if (m_objects[0]->at(i)->GetType() != GameObject::WALL && m_objects[0]->at(i)->GetActive())
		{
			if (math::Distance(pPos, m_objects[0]->at(i)->position) < pRadius)
				acceptableObjects.push_back(m_objects[0]->at(i));
		}
	}

	return acceptableObjects;
}

void ObjectManager::Pause()
{
	m_pause = true;
}

void ObjectManager::Stop()
{
	int index = 0;
	for (unsigned int i = 0; i < m_objects[0]->size(); i++)
	{
		index++;
		m_objects[0]->at(i)->Reset();
	}
	m_pause = true;
	ServiceLocator<AISystem>::GetService()->ResetTrees();
}

void ObjectManager::Play()
{
	m_pause = false;
}

bool ObjectManager::GetPause()
{
	return m_pause;
}
