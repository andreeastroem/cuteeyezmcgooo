//This a node with one or more childs
//Selectors, sequencers etc
#pragma once

#include "BTNode.h"

class BTCompositeNode : public BTNode
{
public:
	BTCompositeNode();
	~BTCompositeNode();

	virtual void Initialise() = 0;
	virtual BTResult Process() override;
	virtual BTResult ProcessNode(BTNode* pNode);
	virtual void AttachChildNode(BTNode* pNode);
	virtual void Reset();

protected:
	std::vector<BTNode*> m_childNodes;
	BTNode* m_currentNode;
};