#pragma once 

#include <memory>

class ResourceManager
{
public:
	typedef std::unique_ptr<ResourceManager> ptr;
	static ptr Create();
	~ResourceManager();

	void Update();
	void Cleanup();

	sf::Font* LoadFont(std::string pFileName);

private:
	ResourceManager();

	sf::Font* AlreadyLoadedFont(std::string pPath);

public:

private:
	std::string m_directory;

	std::map<std::string, sf::Font*> m_fonts;
};