//Text effect
#pragma  once
#include "ScriptComponent.h"

#include "RenderComponent.h"

class BoingEffect : public ScriptComponent
{
public:
	BoingEffect();
	~BoingEffect();

	void Cleanup();
	void Update();
	virtual void SetActive(bool pState);

	bool Initialise(sf::Vector2f pPosition);
private:

public:

private:
	RenderComponent* m_text;

	float m_dY;
	float m_originalY;
	float m_Yspeed;
	float m_time;
	float m_Xspeed;
	sf::Color m_colour;

	float m_fadespeed;

	bool m_goingUp;

	float m_expiredTime;

	bool m_update;
};