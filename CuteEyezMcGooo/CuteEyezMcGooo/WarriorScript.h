//Game logic for the hero (warrior class)
#pragma once

#include "ScriptComponent.h"

class BTTree;
class ControllerComponent;
class Collider;

class WarriorScript : public ScriptComponent
{
public:
	WarriorScript();
	~WarriorScript();

	void Initialise(BTTree* pTree, sf::Vector2f pStartPosition);

	void Reset();
	virtual void Update() override;
	virtual void Cleanup() override;
	virtual void OnCollision(GameObject* pOther) override;
	virtual void ExitCollision(GameObject* pOther) override;
	virtual void SetActive(bool pState);
	sf::Vector2f GetStartPosition(){ return m_startPosition; };
	void PickedUpGold(){ m_pickedUpGold = true; };
	bool HasPickedUpGold(){ return m_pickedUpGold; };
	void AddTreasure(float pAmount){ m_treasure += pAmount; };

	bool FoundGold();
	GameObject* GetGold();

private:
	BTTree* m_BTtree;
	GameObject* m_gold;
	ControllerComponent* m_controller;

	unsigned int m_treasure;

	sf::Vector2f m_startPosition;

	std::vector<sf::Vector2f> m_crossroads;

	float m_senseTimer;
	float m_senseInterval;
	bool m_sense;
	bool m_pickedUpGold;
	float m_senseRange;
	std::vector<GameObject*> m_sensedObjects;
	std::vector<GameObject*> m_knownObjects;
	Collider* m_collider;
	GameObject* m_winText;
};