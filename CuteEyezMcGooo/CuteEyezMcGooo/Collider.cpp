#include "stdafx.h"

#include "Collider.h"
#include "GameObject.h"

Collider::Collider()
{

}
Collider::~Collider()
{

}

void Collider::Update()
{
	if (m_bodyDef.type == b2_dynamicBody)
	{

		if (!(m_body->GetPosition() == m_prevPosition))
		{
			m_owner->SetPosition(GetPosition());
		}
		if (!(m_body->GetAngle() == m_prevRotation))
		{
			m_owner->SetRotation(GetRotationInDegrees());
		}

		m_prevPosition = m_body->GetPosition();
		m_prevRotation = m_body->GetAngle();
	}
}

void Collider::Cleanup()
{
	Component::Cleanup();

	if (m_body)
	{
		m_body->GetWorld()->DestroyBody(m_body);
		m_body->SetUserData(nullptr);
		m_body = nullptr;
	}
}

void Collider::SetPosition(sf::Vector2f pPosition)
{
	b2Vec2 pos = b2Vec2(pPosition.x, -pPosition.y);
	m_body->SetTransform(pos, m_body->GetAngle());
	m_prevPosition = m_body->GetPosition();
}

void Collider::ApplyForce(sf::Vector2f pForce)
{
	m_body->ApplyForce(b2Vec2(pForce.x, pForce.y), m_body->GetPosition(), true);
}

void Collider::SetVelocity(sf::Vector2f pVelocity)
{
	m_body->SetLinearVelocity(b2Vec2(pVelocity.x, -pVelocity.y));
}

sf::Vector2f Collider::GetVelocity()
{
	return sf::Vector2f(m_body->GetLinearVelocity().x, m_body->GetLinearVelocity().y);
}

sf::Vector2f Collider::GetPosition()
{
	return sf::Vector2f(m_body->GetPosition().x, -m_body->GetPosition().y);
}


Collider::EType Collider::GetType()
{
	return m_type;
}

void Collider::SetCollisionGroup(int pGroupIndex)
{
	b2Filter filter = m_body->GetFixtureList()->GetFilterData();
	filter.groupIndex = static_cast<sf::Int16>(pGroupIndex);
	m_body->GetFixtureList()->SetFilterData(filter);
}

float Collider::GetRotationInDegrees()
{
	return math::RADTODEG(m_body->GetAngle());
}

void Collider::SetRotation(float pDegrees)
{
	m_body->SetTransform(m_body->GetPosition(), math::DEGTORAD(pDegrees));
}

b2Body* Collider::GetBody()
{
	return m_body;
}

GameObject* Collider::Raycast(sf::Vector2f pStartPosition, sf::Vector2f pEndPosition)
{
	RaycastCallback rccb;
	m_body->GetWorld()->RayCast(&rccb, b2Vec2(pStartPosition.x, -pStartPosition.y), b2Vec2(pEndPosition.x, -pEndPosition.y));
	return rccb.GetClosest();
}

bool Collider::CanSee(sf::Vector2f pStartPosition, sf::Vector2f pEndPosition, GameObject::EObjectType pType)
{
	RaycastCallback rccb;
	m_body->GetWorld()->RayCast(&rccb, b2Vec2(pStartPosition.x, -pStartPosition.y), b2Vec2(pEndPosition.x, -pEndPosition.y));
	return rccb.Found();
}


float32 RaycastCallback::ReportFixture(b2Fixture* pFixture, const b2Vec2& pPoint, const b2Vec2& pNormal, float32 pFraction)
{
	if (m_body)
	{
		if (pFraction < m_fraction)
		{
			m_body = pFixture->GetBody();
			m_fraction = pFraction;
			m_intersectPoint = sf::Vector2f(pPoint.x, -pPoint.y);
			m_normal = sf::Vector2f(pNormal.x, -pNormal.y);
			m_object = static_cast<Collider*>(m_body->GetUserData())->GetOwner();
		}
		m_objects.push_back(m_object);
	}
	else
	{
		m_body = pFixture->GetBody();
		m_fraction = pFraction;
		m_intersectPoint = sf::Vector2f(pPoint.x, -pPoint.y);
		m_normal = sf::Vector2f(pNormal.x, -pNormal.y);
		m_object = static_cast<Collider*>(m_body->GetUserData())->GetOwner();

		m_objects.push_back(m_object);
	}

	return 1;
}

GameObject* RaycastCallback::GetClosest()
{
	if (m_object)
		return m_object;
	else
		return nullptr;
}

bool RaycastCallback::Found(GameObject::EObjectType pType)
{
	if (pType != GameObject::DEFAULT)
	{
		if (GetClosest() && GetClosest()->GetType() == pType)
			return true;

	}
	else
	{
		if (GetClosest() && GetClosest()->GetType() != GameObject::WALL)
			return true;
	}
	return false;
}

sf::Vector2f RaycastCallback::GetIntersectionPoint()
{
	return m_intersectPoint;
}


RaycastCallback::RaycastCallback()
{
	m_object = nullptr;
	m_body = nullptr;
}

std::vector<GameObject*> RaycastCallback::GetAll()
{
	return m_objects;
}
