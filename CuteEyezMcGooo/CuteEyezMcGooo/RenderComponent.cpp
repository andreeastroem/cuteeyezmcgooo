#include "stdafx.h"

#include "RenderComponent.h"


RenderComponent::RenderComponent()
{
	m_view = RenderSystem::GAME;
	m_layer = 1;
}

RenderComponent::~RenderComponent()
{

}

void RenderComponent::Update()
{

}

void RenderComponent::Cleanup()
{

}

void RenderComponent::SetLayer(unsigned int pLayer)
{
	m_layer = pLayer;
}

unsigned int RenderComponent::GetLayer()
{
	return m_layer;
}
