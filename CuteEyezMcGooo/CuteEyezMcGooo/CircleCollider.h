#pragma once

#include "Collider.h"

class CircleCollider : public Collider
{
public:
	CircleCollider();
	~CircleCollider();

	bool Initialise(b2World& pWorld, sf::Vector2f pPosition, float pRadius,
		bool pDynamic = false, bool pIsSensor = false);

	virtual void EnterCollision(Collider* pCollider) override;
	virtual void LeaveCollision(Collider* pCollider) override;

private:
	b2CircleShape m_b2shape;
};