#pragma once

#ifndef BTTREE_GNOLL_H_
#define BTTREE_GNOLL_H_
#include "BTTree.h"

class BTTreeGnoll : public BTTree
{
public:
	BTTreeGnoll();
	~BTTreeGnoll();

	void Initialise();
};

#endif