#pragma once


#ifndef WORLD_SYSTEM_H
#define WORLD_SYSTEM_H
#include "SFML/Graphics.hpp"
#include "GameObject.h"

class Node
{
public:
	Node(){};
	~Node(){};

	void Init(const int p_x, const int p_y, bool p_walkable, bool p_occupied = false, std::vector<GameObject*> p_occupants = std::vector<GameObject*>(), float p_terrainToughness = 0);
	bool IsWalkable(){ return m_walkable; };
	void SetWalkable(bool p_walkable){ m_walkable = p_walkable; };

	sf::Vector2i GetPosition(){ return sf::Vector2i(m_x, m_y); };
	float GetTerrain(){ return m_terrainToughness; };

	bool IsOccupied(){ return m_occupied; };

	std::vector<GameObject*> GetOccupant() { return m_occupants; };
	void SetOccupant(std::vector<GameObject*> p_occupants, float p_terrainToughness, bool p_walkable);
	void SetOccupant(GameObject* p_occupant, float p_terrainToughness, bool p_walkable);
	void RemoveOccupant();

	bool operator ==(Node p_node)
	{
		sf::Vector2i pos = p_node.GetPosition();
		if (pos.x == m_x && pos.y == m_y)
			return true;
		return false;
	};
	bool operator !=(Node p_node)
	{
		return !(p_node == *this);
	};

private:
	int m_x, m_y;
	bool m_walkable, m_occupied;
	float m_terrainToughness;
	std::vector<GameObject*> m_occupants;
};

class WorldSystem
{
public:
	WorldSystem();
	~WorldSystem();


	bool CreateWorld(const char* p_path);
	bool CreateWorld(const int p_width, const int p_height, const int p_scale, const int p_offset);

	bool IsWalkable(const int p_x, const int p_y);
	void SetWalkable(const int p_x, const int p_y, bool p_walkable);

	Node& GetNode(const int p_x, const int p_y){ return GetNode(sf::Vector2i(p_x, p_y)); };
	Node& GetNode(sf::Vector2i p_pos);
	Node& GetNode(sf::Vector2f p_pos){ return GetNode((sf::Vector2i)p_pos); };
	Node& GetNode(const int p_i);

	sf::Vector2f WorldToGraphicsScale(sf::Vector2f p_position);
	sf::Vector2f GraphicsToWorldScale(sf::Vector2f p_position);
	sf::Vector2f SnapToGrid(sf::Vector2f p_position);


	int GetTileSize();
	int GetWidth();
	int GetHeight();
	sf::Vector2f GetSize();

	static const sf::Vector2f Forward;

private:
	int m_width, m_height, m_scale, m_offset;
	Node* m_world;
};

#endif