//The nodes for the warrior tree
#pragma once

#include "BTTree.h"
#include "WorldSystem.h"

class ControllerComponent;

class BTTreeWarrior : public BTTree
{
public:
	BTTreeWarrior();
	~BTTreeWarrior();

	virtual void Initialise() override;
	
	
private:

};

//Nodes
class BTWalkTo : public BTNode
{
public:

private:
	std::vector<Node*> m_path;
	unsigned int m_index;

	sf::Vector2f m_targetPosition;
	GameObject* m_actor;

	virtual BTResult Process() override;

	virtual void Initialise() override;

	virtual void Deinitialise() override;

};

class BTDisarm : public BTNode
{
public:
	virtual BTResult Process() override;

	virtual void Initialise() override;

	virtual void Deinitialise() override;

};

class BTAttack : public BTNode
{
public:
	virtual BTResult Process() override;

	virtual void Initialise() override;

	virtual void Deinitialise() override;

};

class BTFlee : public BTNode
{
public:
	virtual BTResult Process() override;

	virtual void Initialise() override;

	virtual void Deinitialise() override;

private:
	ControllerComponent* m_ownerController;
	ControllerComponent* m_enemyController;
};

class BTInteract : public BTNode
{
public:
	virtual BTResult Process() override;

	virtual void Initialise() override;

	virtual void Deinitialise() override;

};

class BTPickUp : public BTNode
{
public:
	virtual BTResult Process() override;

	virtual void Initialise() override;

	virtual void Deinitialise() override;

};

class BTWait : public BTNode
{
public:
	BTWait(){};

	virtual BTResult Process() override;

	virtual void Initialise() override;

	virtual void Deinitialise() override;

private:
	float m_currentTime;
	float m_waitTime;
};

class BTFindUnwalkedNode : public BTNode
{
public:

private:
	unsigned int width, height;
	unsigned int dX, dY;

	virtual BTResult Process() override;

	virtual void Initialise() override;

	virtual void Deinitialise() override;

};

class BTLose : public BTNode
{
public:

private:
	GameObject* m_text;

	virtual BTResult Process() override;

	virtual void Initialise() override;

	virtual void Deinitialise() override;

};