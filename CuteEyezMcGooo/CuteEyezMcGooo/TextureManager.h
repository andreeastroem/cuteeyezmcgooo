//A class for loading and containing textures
#pragma once

#include <memory>
#include "SFML/Graphics.hpp"

class TextureManager
{
	TextureManager();
	TextureManager(const std::string& pDirectory);
public:
	~TextureManager();

	typedef std::unique_ptr<TextureManager> ptr;
	static ptr Create(const std::string& pDirectory);

	int LoadTexture(const std::string& pFilename);
	sf::Texture* GetTexture(unsigned int pIndex);

	void Cleanup();

private:
	std::string m_directory;
	std::vector<sf::Texture*> m_textures;
	std::vector<std::string> m_filenames;
};