#include "stdafx.h"

#include "Math.h"

#define PI 3.14159265359f

float math::Distance(sf::Vector2f pVector2)
{
	return sqrtf(powf(pVector2.x, 2) + powf(pVector2.y, 2));
}

float math::Distance(float x, float y)
{
	return sqrtf(powf(x, 2) + powf(y, 2));
}

float math::Distance(sf::Vector2f pStart, sf::Vector2f pEnd)
{
	return sqrtf(powf(pEnd.x - pStart.x, 2) + powf(pEnd.y - pStart.y, 2));
}

sf::Vector2f math::Normalise(float pX, float pY)
{
	return (sf::Vector2f(pX, pY) / Distance(pX, pY));
}

sf::Vector2f math::Normalise(sf::Vector2f pVector2)
{
	float d = Distance(pVector2);
	return pVector2 / Distance(pVector2);
}

float math::RADTODEG(float pRadians)
{
	return ((-pRadians / PI) * 180);
}

float math::DEGTORAD(float pDegrees)
{
	return ((-pDegrees / 180.0f) * PI);
}

float math::Dot(sf::Vector2f& p_a, sf::Vector2f& p_b)
{
	return (p_a.x * p_b.x) + (p_a.y * p_b.y);
}
