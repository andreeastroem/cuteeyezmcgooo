//Base class for all Behaviour Tree nodes
#pragma once
#include <vector>

class BehaviourTaskBase;
class BTTree;

class BTNode
{
public:
	enum BTResult
	{
		FAILURE,
		SUCCESS,
		RUNNING
	};
public:
	BTNode();
	~BTNode();

	virtual BTResult Process() = 0;
	virtual void Initialise();
	virtual void Deinitialise();
	virtual void SetTree(BTTree* pTree);
	virtual void AddCondition(BehaviourTaskBase* p_condition);
	virtual BTResult CheckConditions();
	virtual std::string GetName();
	virtual void SetName(std::string pName);
	virtual void Reset();

protected:
	BTTree* m_tree;
	std::vector<BehaviourTaskBase*> m_conditions;
	std::string m_name;
};