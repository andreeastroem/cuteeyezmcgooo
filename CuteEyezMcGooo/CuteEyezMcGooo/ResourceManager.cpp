#include "stdafx.h"

#include "ResourceManager.h"


ResourceManager::ptr ResourceManager::Create()
{
	return ResourceManager::ptr(new ResourceManager());
}

ResourceManager::~ResourceManager()
{
	Cleanup();
}

void ResourceManager::Update()
{

}

void ResourceManager::Cleanup()
{
	//fonts
	auto it = m_fonts.begin();
	while (it != m_fonts.end())
	{
		if (it->second)
		{
			delete it->second;
			it->second = nullptr;
			it++;
		}
	}
	m_fonts.clear();

	//other stuff
}

ResourceManager::ResourceManager()
{
	m_directory = "../data/";
}

sf::Font* ResourceManager::LoadFont(std::string pFilename)
{
	std::string path = m_directory + "/fonts/" + pFilename;
	std::string message;

	sf::Font* font = AlreadyLoadedFont(path);
	
	//early out
	if (font)
		return font;

	font = new sf::Font();
	if (!font->loadFromFile(path))
	{
		message = "Could not load font in path: " + path;
		Log::Message(message.c_str(), Log::ERROR);
		return nullptr;
	}

	message = "Loaded font: " + path;
	Log::Message(message.c_str(), Log::SUCCESSFUL);
	std::pair<std::string, sf::Font*> p = std::pair<std::string, sf::Font*>(path, font);
	m_fonts.insert(p);
	
	return font;
}

sf::Font* ResourceManager::AlreadyLoadedFont(std::string pPath)
{
	std::map<std::string, sf::Font*>::iterator it = m_fonts.find(pPath);
	if (it != m_fonts.end())
	{
		return it->second;
	}
	return nullptr;
}

