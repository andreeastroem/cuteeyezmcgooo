#pragma once

#ifndef GNOLL_SCRIPT_H_
#define GNOLL_SCRIPT_H_
#include "ScriptComponent.h"
#include <vector>

class BTTree;
class ControllerComponent;
class ObjectManager;
class Collider;
class AISystem;

class GnollScript : public ScriptComponent
{
public:
	GnollScript();
	~GnollScript();

	void Initialise(BTTree* p_tree, sf::Vector2f p_startPosition);

	void Reset();
	virtual void Update();
	virtual void Cleanup();
	virtual void OnCollision(GameObject* p_other);
	virtual void ExitCollision(GameObject* p_other);
	bool IsHeroSpotted(){ return m_heroSpotted; };
	bool GnollPartOfGroup(GameObject* p_gnoll);
	std::vector<GnollScript*> GetGnollsOutOfGroup(float p_range);
	void SpotHero(sf::Vector2f p_position);
	void UnSpotHero();
	sf::Vector2f GetSpottedPosition(){ return m_lastSpottedPosition; };
	void AddToGroup(GameObject* member);
	bool HasEnoughDamage();
	void IgnoreDamage(){ m_ignoreDamage = true; };
	void DontIgnoreDamage(){ m_ignoreDamage = false; };
	float GetAttackRate(){ return AttackRate; };
	float GetDamage() { return Damage; };
	float GetTalkRange() { return TalkRange; };
	float GetFoV() { return FoV; };
	std::vector<GameObject*> GetGroup(){ return m_group; };

private:
	BTTree* m_BTree;
	GameObject* m_hero;
	ControllerComponent* m_controller;

	sf::Vector2f m_startPosition;
	Collider* m_collider;
	bool m_heroSpotted, m_ignoreDamage;
	sf::Vector2f m_lastSpottedPosition;

	float m_lastSense = 0.f;

	const float SenseInterval = 0.2f;
	const float SenseRange = 400.f;
	const float Damage = 2.f;
	const float Speed = 125.f;
	const float FoV = 90.f;
	const float GroupDamageToAttack = 8.f;
	const float AttackRate = 0.2f;
	const float TalkRange = 100;

	std::vector<GameObject*> m_knownObjects;
	std::vector<GameObject*> m_objectsInRange;
	std::vector<GameObject*> m_group;
	ObjectManager* m_objectManager;
	AISystem* m_aiSystem;
};


#endif