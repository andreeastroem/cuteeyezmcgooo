#pragma once

#ifndef TRAPS_H_
#define TRAPS_H_
#include "ControllerComponent.h"
#include "WorldSystem.h"
#include "ScriptComponent.h"

class TrapScript : public ScriptComponent
{
public:
	TrapScript(){};
	~TrapScript(){}


	virtual void Update() = 0;
	virtual void Cleanup() = 0;
	virtual void Trigger() = 0;
	virtual void Disarm();
	virtual void OnEnter() = 0;
	virtual void OnExit() = 0;

protected:
	ControllerComponent* m_controller;
	GameObject* m_hero;
	WorldSystem* m_world;
	sf::Vector2f m_position;
	bool m_initialised;
	bool m_armed;
};

class SpikeTrap : public TrapScript
{
public:
	SpikeTrap();
	~SpikeTrap();

	void Update();
	void Cleanup();
	void Trigger();
	void OnEnter();
	void OnExit();

private:
	int m_damage;
	GameObject* m_hero;
	WorldSystem* m_world;
};


class GlueTrap : public TrapScript
{
public:
	GlueTrap();
	~GlueTrap();

	void Update();
	void Cleanup();
	void Trigger();
	void Trigger(float p_speed);
	void OnEnter();
	void OnExit();

private:
	const float m_slowdown = 0.75f;
	bool m_onTrap;
};

#endif