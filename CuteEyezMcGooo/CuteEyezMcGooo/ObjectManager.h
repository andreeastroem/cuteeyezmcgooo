#pragma once

#include "GameObject.h"
#include <memory>

class ObjectManager
{
public:
	typedef std::unique_ptr<ObjectManager> ptr;
	static ptr Create();

	~ObjectManager();
	void Update();
	void Cleanup();

	void AttachGameObject(GameObject* pObject);

	void DeleteGameObject(GameObject* pObject, unsigned int pView = 0);
	void DeleteGameObject(unsigned int pIndex, unsigned int pView = 0);

	GameObject* GetObjectWithType(GameObject::EObjectType pType);
	std::vector<GameObject*> GetObjectsWithinRadius(sf::Vector2f pPos, float pRadius);

	void Pause();
	void Stop();
	void Play();

	bool GetPause();
private:
	ObjectManager();
private:
	std::vector<std::vector<GameObject*>*> m_objects;

	bool m_pause;
};