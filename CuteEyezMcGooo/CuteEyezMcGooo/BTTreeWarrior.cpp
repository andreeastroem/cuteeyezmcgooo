#include "stdafx.h"

#include "BTTreeWarrior.h"

#include "BTSelector.h"
#include "BTSequencer.h"

#include "ServiceLocator.h"
#include "AISystem.h"
#include "BTNode.h"
#include "Time.h"
#include "Factory.h"

#include "ControllerComponent.h"
#include "WarriorScript.h"

//Random
#include <ctime>
#include <cstdlib>

BTTreeWarrior::BTTreeWarrior()
{
	m_data = nullptr;
	m_root = nullptr;
}

BTTreeWarrior::~BTTreeWarrior()
{

}

void BTTreeWarrior::Initialise()
{
	BTSelector* root = new BTSelector();
	root->SetTree(this);

	m_root = root;

	//typedef [&]()->BTNode::BTResult Lamda0;

#pragma region root
	//left side of root


#pragma region knownObjects
	BTSelector* anyknownobjects = new BTSelector();
	anyknownobjects->SetTree(this);

	auto hasKnownObjects = [&](GameObject* actor)->BTNode::BTResult
	{
		ControllerComponent* controller = actor->GetComponent<ControllerComponent>(Component::CONTROLLER);
		GameObject* taget = controller->GetKnownObject();
		if (taget != nullptr)
		{
			controller->SetTargetObject(taget);
			return BTNode::SUCCESS;
		}
		else
			return BTNode::FAILURE;
	};

	BTTaskL1<decltype(hasKnownObjects), GameObject*>* l1 = new BTTaskL1<decltype(hasKnownObjects), GameObject*>(hasKnownObjects, m_data);
	anyknownobjects->AddCondition(l1);

	//left side of anyknownobjects
#pragma region hostile
	BTSelector* hostile = new BTSelector();
	hostile->SetTree(this);

	auto anyhostile = [&](GameObject* actor)->BTNode::BTResult
	{
		ControllerComponent* controller = actor->GetComponent<ControllerComponent>(Component::CONTROLLER);

		switch (controller->GetTargetObject()->GetType())
		{
		case GameObject::GNOLL:
		case GameObject::SPIKETRAP:
			return BTNode::SUCCESS;
			break;
		}

		return BTNode::FAILURE;
	};
	BTTaskL1<decltype(anyhostile), GameObject*>* l2 = new BTTaskL1<decltype(anyhostile), GameObject*>(anyhostile, m_data);
	hostile->AddCondition(l2);

	//left side of hostile
#pragma region ismonster
	BTSelector* ismonster = new BTSelector();
	ismonster->SetTree(this);

	auto anymonsters = [&](GameObject* actor)->BTNode::BTResult
	{
		ControllerComponent* controller = actor->GetComponent<ControllerComponent>(Component::CONTROLLER);

		switch (controller->GetTargetObject()->GetType())
		{
		case GameObject::GNOLL:
			return BTNode::SUCCESS;
			break;
		default:
			return BTNode::FAILURE;
			break;
		}

		return BTNode::FAILURE;
	};
	BTTaskL1<decltype(anymonsters), GameObject*>* l3 = new BTTaskL1<decltype(anymonsters), GameObject*>(anymonsters, m_data);
	ismonster->AddCondition(l3);


	//left side of ismonster
#pragma region islevellower

	//left side of islevellower
#pragma region attack
	BTSequencer* attack = new BTSequencer();
	attack->SetTree(this);

	auto LUpdateTargetPosition = [&](GameObject* actor)->BTNode::BTResult
	{
		ControllerComponent* controller = actor->GetComponent<ControllerComponent>(Component::CONTROLLER);
		if (controller->GetTargetObject()->position != controller->GetTargetPosition())
			controller->SetTargetPosition(controller->GetTargetObject()->position);
		return BTNode::SUCCESS;
	};
	BTTaskL1<decltype(LUpdateTargetPosition), GameObject*>* TUpdateTargetPosition = new BTTaskL1<decltype(LUpdateTargetPosition), GameObject*>(LUpdateTargetPosition, m_data);
	BTWalkTo* walkto1 = new BTWalkTo();
	walkto1->AddCondition(TUpdateTargetPosition);
	walkto1->SetTree(this);
	BTAttack* attack1 = new BTAttack();
	attack1->SetTree(this);

	attack->AttachChildNode(walkto1);
	attack->AttachChildNode(attack1);



	auto isMonsterlevelLowerThanHero = [&](GameObject* actor)->BTNode::BTResult
	{
		ControllerComponent* controller = actor->GetComponent<ControllerComponent>(Component::CONTROLLER);
		if (controller->GetLevel() > controller->GetTargetObject()->GetComponent<ControllerComponent>(Component::CONTROLLER)->GetLevel())
		{
			sf::Vector2f pos = controller->GetTargetObject()->position;
			pos = ServiceLocator<WorldSystem>::GetService()->SnapToGrid(pos);
			controller->SetTargetPosition(pos);
			return BTNode::SUCCESS;
		}
		return BTNode::FAILURE;
	};
	BTTaskL1<decltype(isMonsterlevelLowerThanHero), GameObject*>* l4 = new BTTaskL1<decltype(isMonsterlevelLowerThanHero), GameObject*>(isMonsterlevelLowerThanHero, m_data);

	attack->AddCondition(l4);
#pragma endregion attack
	//right side of islevellower
#pragma region flee
	BTSequencer* flee = new BTSequencer();
	flee->SetTree(this);

	BTFlee* flee1 = new BTFlee();
	flee1->SetTree(this);
	BTWalkTo* walkto2 = new BTWalkTo();
	walkto2->SetTree(this);

	flee->AttachChildNode(flee1);
	flee->AttachChildNode(walkto2);
#pragma endregion flee


#pragma endregion islevellower

	//right side of ismonster
#pragma region handletrap
	BTSequencer* handleTrap = new BTSequencer();
	handleTrap->SetTree(this);

	auto isValidTrap = [&](GameObject* actor)->BTNode::BTResult
	{
		WorldSystem* world = ServiceLocator<WorldSystem>::GetService();
		sf::Vector2f pos;
		Node from, to;
		switch (actor->GetComponent<ControllerComponent>(Component::CONTROLLER)->GetTargetObject()->GetType())
		{
		case GameObject::SPIKETRAP:
			pos = actor->GetComponent<ControllerComponent>(Component::CONTROLLER)->GetTargetObject()->position;
			pos = world->GraphicsToWorldScale(world->SnapToGrid(pos));
			to = world->GetNode(pos);

			pos = actor->position;
			pos = world->GraphicsToWorldScale(world->SnapToGrid(pos));
			from = world->GetNode(pos);

			to = *ServiceLocator<AISystem>::GetService()->GetPathToAdjacentNodeThatDoesNotInvolveTargetNode(&from, &to);

			pos = world->WorldToGraphicsScale((sf::Vector2f)to.GetPosition());

			actor->GetComponent<ControllerComponent>(Component::CONTROLLER)->SetTargetPosition(pos);

			return BTNode::SUCCESS;
			break;
		default:
			return BTNode::FAILURE;
			break;
		}

		return BTNode::FAILURE;
	};
	BTTaskL1<decltype(isValidTrap), GameObject*>* l8 = new BTTaskL1<decltype(isValidTrap), GameObject*>(isValidTrap, m_data);

	handleTrap->AddCondition(l8);

	BTWalkTo* walkto3 = new BTWalkTo();
	walkto3->SetTree(this);
	BTDisarm* disarm1 = new BTDisarm();
	disarm1->SetTree(this);

	handleTrap->AttachChildNode(walkto3);
	handleTrap->AttachChildNode(disarm1);
#pragma endregion handletrap

	ismonster->AttachChildNode(attack);
	ismonster->AttachChildNode(flee);
#pragma endregion ismonster

	//right side of hostile
#pragma region isinteractive
	BTSelector* isInteractiveObject = new BTSelector();
	isInteractiveObject->SetTree(this);

	auto isInteractive = [&](GameObject* actor)->BTNode::BTResult
	{
		return BTNode::FAILURE;
		switch (actor->GetComponent<ControllerComponent>(Component::CONTROLLER)->GetTargetObject()->GetType())
		{
		default:
			printf("There are no interactive items implemented yet (bttreewarrior)\n");
			break;
		}

		return BTNode::FAILURE;
	};
	BTTaskL1<decltype(isInteractive), GameObject*>* l5 = new BTTaskL1<decltype(isInteractive), GameObject*>(isInteractive, m_data);

	//left side of isinteractiveobject
#pragma region interact
	BTSequencer* interact = new BTSequencer();
	interact->SetTree(this);
	

	interact->AddCondition(l5);

	BTWalkTo* walkto4 = new BTWalkTo();
	walkto4->SetTree(this);
	BTInteract* interact1 = new BTInteract();
	interact1->SetTree(this);

	interact->AttachChildNode(walkto4);
	interact->AttachChildNode(interact1);
#pragma endregion interact
	//right side of isinteractiveobject
#pragma region pickup
	BTSequencer* pickup = new BTSequencer();
	pickup->SetTree(this);

	auto isGold = [&](GameObject* actor)->BTNode::BTResult
	{
		if (actor->GetComponent<WarriorScript>(Component::SCRIPT)->FoundGold())
		{
			sf::Vector2f pos = actor->GetComponent<WarriorScript>(Component::SCRIPT)->GetGold()->position;
			actor->GetComponent<ControllerComponent>(Component::CONTROLLER)->SetTargetPosition(pos);

			return BTNode::SUCCESS;
		}

		return BTNode::FAILURE;
	};

	BTTaskL1<decltype(isGold), GameObject*>* l7 = new BTTaskL1<decltype(isGold), GameObject*>(isGold, m_data);
	pickup->AddCondition(l7);

	BTWalkTo* walkto5 = new BTWalkTo();
	walkto5->SetTree(this);
	BTPickUp* pickup1 = new BTPickUp();
	pickup1->SetTree(this);

	pickup->AttachChildNode(walkto5);
	pickup->AttachChildNode(pickup1);
#pragma endregion pickup

	isInteractiveObject->AttachChildNode(interact);
	isInteractiveObject->AttachChildNode(pickup);

#pragma endregion isinteractive

	hostile->AttachChildNode(ismonster);
	hostile->AttachChildNode(handleTrap);

#pragma endregion hostile

	anyknownobjects->AttachChildNode(hostile);
	anyknownobjects->AttachChildNode(isInteractiveObject);

#pragma endregion knownObjects
	//right side of anyknownobjects
#pragma region lowonhp
	BTSelector* LowOnHp = new BTSelector();
	LowOnHp->SetTree(this);
	
	auto isLowOnHp = [&](GameObject* actor)->BTNode::BTResult
	{
		return BTNode::FAILURE;

		if (actor->GetComponent<ControllerComponent>(Component::CONTROLLER)->GetHPPercentage() < 0.3f)
		{
			return BTNode::SUCCESS;
		}

	};
	BTTaskL1<decltype(isLowOnHp), GameObject*>* l6 = new BTTaskL1<decltype(isLowOnHp), GameObject*>(isLowOnHp, m_data);

	//left side of lowOnHp
#pragma region regen
	BTSequencer* regen = new BTSequencer();
	regen->SetTree(this);
	regen->AddCondition(l6);

	BTWait* wait1 = new BTWait();
	wait1->SetTree(this);

	regen->AttachChildNode(wait1);
#pragma endregion regen
	//right side of 
#pragma region wander
	BTSequencer* wander = new BTSequencer();
	wander->SetTree(this);

	BTFindUnwalkedNode* wander1 = new BTFindUnwalkedNode();
	wander1->SetTree(this);
	BTWalkTo* walkto6 = new BTWalkTo();
	walkto6->SetTree(this);

	wander->AttachChildNode(wander1);
	wander->AttachChildNode(walkto6);
#pragma endregion wander

	LowOnHp->AttachChildNode(regen);
	LowOnHp->AttachChildNode(wander);
#pragma endregion lowonhp

	BTWalkTo* walkToStart = new BTWalkTo();
	walkToStart->SetTree(this);
	BTLose* lose = new BTLose();
	lose->SetTree(this);
	BTSequencer* runToStart = new BTSequencer();
	auto LHasPickedGold = [&](GameObject* actor)->BTNode::BTResult
	{
		if (actor->GetComponent<WarriorScript>(Component::SCRIPT)->HasPickedUpGold())
			return BTNode::SUCCESS;
		return BTNode::FAILURE;
	};
	BTTaskL1<decltype(LHasPickedGold), GameObject*>* THasPickedGold = new BTTaskL1<decltype(LHasPickedGold), GameObject*>(LHasPickedGold, m_data);
	runToStart->AddCondition(THasPickedGold);
	runToStart->AttachChildNode(walkToStart);
	runToStart->AttachChildNode(lose);
	runToStart->SetTree(this);

	root->AttachChildNode(runToStart);
	root->AttachChildNode(anyknownobjects);
	root->AttachChildNode(LowOnHp);
#pragma endregion root
}


//Nodes
BTNode::BTResult BTWalkTo::Process()
{
	if (CheckConditions() != BTNode::SUCCESS)
		return BTNode::FAILURE;

	if (ServiceLocator<WorldSystem>::GetService()->SnapToGrid(m_targetPosition) != ServiceLocator<WorldSystem>::GetService()->SnapToGrid(m_actor->GetComponent<ControllerComponent>(Component::CONTROLLER)->GetTargetPosition()))
		Initialise();

	while (m_index < m_path.size())
	{
		if (m_path[m_path.size() - m_index - 1]->IsWalkable() &&
			ServiceLocator<WorldSystem>::GetService()->GetNode(ServiceLocator<WorldSystem>::GetService()->GraphicsToWorldScale(ServiceLocator<WorldSystem>::GetService()->SnapToGrid(m_actor->position))).IsWalkable())
		{
			sf::Vector2f position = (sf::Vector2f)m_path[m_path.size() - m_index - 1]->GetPosition();
			position = ServiceLocator<WorldSystem>::GetService()->WorldToGraphicsScale(position);

			ControllerComponent::EResult result = m_actor->GetComponent<ControllerComponent>(Component::CONTROLLER)->Move(position);
			if (result == ControllerComponent::SUCCESS)
			{
				ServiceLocator<AISystem>::GetService()->BeenTo(m_path[m_path.size() - m_index - 1]->GetPosition().x, m_path[m_path.size() - m_index - 1]->GetPosition().y);
				m_index++;
			}

			//m_actor->LookTowards(m_actor->position + sf::Vector2f(1, 0));
			return BTNode::RUNNING;
		}
		else
		{
			m_actor->GetComponent<ControllerComponent>(Component::CONTROLLER)->SetVelocity(sf::Vector2f());
			m_actor->GetComponent<ControllerComponent>(Component::CONTROLLER)->ArrivedToTargetPosition();
			return BTNode::FAILURE;
		}
	}

	if (m_index == m_path.size())
	{
		m_actor->GetComponent<ControllerComponent>(Component::CONTROLLER)->SetVelocity(sf::Vector2f());
		m_actor->GetComponent<ControllerComponent>(Component::CONTROLLER)->ArrivedToTargetPosition();
	}

	return BTNode::SUCCESS;
}

void BTWalkTo::Initialise()
{
	m_actor = m_tree->GetData();

	m_targetPosition = m_actor->GetComponent<ControllerComponent>(Component::CONTROLLER)->GetTargetPosition();

	sf::Vector2f homepos = ServiceLocator<WorldSystem>::GetService()->GraphicsToWorldScale(ServiceLocator<WorldSystem>::GetService()->SnapToGrid(m_actor->position));
	sf::Vector2f targetpos = ServiceLocator<WorldSystem>::GetService()->GraphicsToWorldScale(m_targetPosition);

	Node* homeNode = &ServiceLocator<WorldSystem>::GetService()->GetNode(homepos);
	Node* targetNode = &ServiceLocator<WorldSystem>::GetService()->GetNode(targetpos);;

	m_path = ServiceLocator<AISystem>::GetService()->GetPath(homeNode, targetNode);

	////WHACK
	//if (m_path.size() == 0)
	//	printf("whoop\n");

	m_index = 0;
}

void BTWalkTo::Deinitialise()
{
	m_index = 0;
	Node* node;
	for each (node in m_path)
	{
		node = nullptr;
	}
}

BTNode::BTResult BTDisarm::Process()
{
	return BTNode::FAILURE;
}

void BTDisarm::Initialise()
{

}

void BTDisarm::Deinitialise()
{

}

BTNode::BTResult BTAttack::Process()
{
	ControllerComponent* controller = m_tree->GetData()->GetComponent<ControllerComponent>(Component::CONTROLLER);
	
	if (math::Distance(controller->GetOwner()->position, controller->GetTargetObject()->position) > 50)
	{
		return BTNode::FAILURE;
	}

	ServiceLocator<Factory>::GetService()->CreateGameObject(GameObject::TEXTEFFECT, controller->GetOwner()->position, std::to_string(controller->GetDamage()));
	controller->GetTargetObject()->GetComponent<ControllerComponent>(Component::CONTROLLER)->Damage(controller->GetDamage());

	return BTNode::SUCCESS;
}

void BTAttack::Initialise()
{
}

void BTAttack::Deinitialise()
{
}

BTNode::BTResult BTFlee::Process()
{
	/*sf::Vector2f ownerNode = ServiceLocator<WorldSystem>::GetService()->GraphicsToWorldScale(ServiceLocator<WorldSystem>::GetService()->SnapToGrid(m_ownerController->GetOwner()->position));
	sf::Vector2f enemyNode = ServiceLocator<WorldSystem>::GetService()->GraphicsToWorldScale(ServiceLocator<WorldSystem>::GetService()->SnapToGrid(m_enemyController->GetOwner()->position));
	int width = ServiceLocator<WorldSystem>::GetService()->GetWidth();
	int height = ServiceLocator<WorldSystem>::GetService()->GetHeight();

	int startx = ownerNode.x - enemyNode.x;
	int starty = ownerNode.y - enemyNode.y;

	int x, y;
	int x = rand() % (width - startx) + startx, y = rand() % (height - starty) + starty;*/

	return BTNode::FAILURE;
}

void BTFlee::Initialise()
{
	m_ownerController = m_tree->GetData()->GetComponent<ControllerComponent>(Component::CONTROLLER);
	m_enemyController = m_ownerController->GetTargetObject()->GetComponent<ControllerComponent>(Component::CONTROLLER);

	srand((unsigned int)std::time(0));
}

void BTFlee::Deinitialise()
{
	m_ownerController = nullptr;
	m_enemyController = nullptr;
}

BTNode::BTResult BTInteract::Process()
{
	return BTNode::FAILURE;
}

void BTInteract::Initialise()
{
}

void BTInteract::Deinitialise()
{
}

BTNode::BTResult BTPickUp::Process()
{
	WarriorScript& script = *m_tree->GetData()->GetComponent<WarriorScript>(Component::SCRIPT);
	ControllerComponent& controller = *m_tree->GetData()->GetComponent<ControllerComponent>(Component::CONTROLLER);
	GameObject* object = controller.GetTargetObject();
	object->SetActive(false);

	script.AddTreasure(2000);
	object->SetPosition(sf::Vector2f(0, -200));
	if (object->GetType() == GameObject::GOLD)
	{
		controller.SetTargetPosition(script.GetStartPosition());
		script.PickedUpGold();
		return BTNode::SUCCESS;
	}

	return BTNode::FAILURE;
}

void BTPickUp::Initialise()
{
}

void BTPickUp::Deinitialise()
{
}

BTNode::BTResult BTWait::Process()
{
	m_currentTime += ServiceLocator<Time>::GetService()->GetDeltaTime();
	if (m_currentTime < m_waitTime)
		return BTNode::RUNNING;

	return BTNode::SUCCESS;
}

void BTWait::Initialise()
{
	m_currentTime = 0.0f;
	m_waitTime = 3.0f;
}

void BTWait::Deinitialise()
{
	m_currentTime = 0.0f;
}

BTNode::BTResult BTFindUnwalkedNode::Process()
{
	int x = rand() % width, y = rand() % height;
	if (ServiceLocator<AISystem>::GetService()->HasBeenTo(x, y) || !ServiceLocator<WorldSystem>::GetService()->IsWalkable(x, y))
	{

		return BTNode::RUNNING;
	}
	else
	{
		ControllerComponent* controller = m_tree->GetData()->GetComponent<ControllerComponent>(Component::CONTROLLER);
		if (controller)
		{
			/*printf("Wander X: %i, Y: %i\n", x, y);*/
			controller->SetTargetPosition(ServiceLocator<AISystem>::GetService()->GetPositionFromNode(x, y));
			controller = nullptr;
			return BTNode::SUCCESS;
		}
		else
		{
			Log::Message("Wander node failed.", Log::ERROR);
			return BTNode::FAILURE;
		}
	}
}

void BTFindUnwalkedNode::Initialise()
{
	width = ServiceLocator<WorldSystem>::GetService()->GetWidth();
	height = ServiceLocator<WorldSystem>::GetService()->GetHeight();
	dX = width / 4;
	dY = height / 4;
	srand((unsigned int)std::time(0));
}

void BTFindUnwalkedNode::Deinitialise()
{

}

BTNode::BTResult BTLose::Process()
{
	if (!m_text)
	{
		m_text = ServiceLocator<Factory>::GetService()->CreateGameObject(GameObject::TEXT, m_tree->GetData()->GetComponent<WarriorScript>(Component::SCRIPT)->GetStartPosition() + sf::Vector2f(0, 50), "You Lose!");
	}
	return BTNode::RUNNING;
}

void BTLose::Initialise()
{
	if (m_text)
		m_text->SetFlag(true);
	m_text = nullptr;
}

void BTLose::Deinitialise()
{
	if (m_text)
		m_text->SetFlag(true);
	m_text = nullptr;
}
