#include "stdafx.h"

#include "BoingEffect.h"
#include "GameObject.h"

#include "ServiceLocator.h"
#include "Time.h"

BoingEffect::BoingEffect()
{

}

BoingEffect::~BoingEffect()
{

}

void BoingEffect::Cleanup()
{
	if (m_text)
		m_text = nullptr;
}

void BoingEffect::Update()
{
	if (!m_text)
	{
		m_text = m_owner->GetComponent<RenderComponent>(Component::RENDER);
		m_text->SetColour(m_colour);
	}
	if (m_update)
	{
		if (m_goingUp)
		{
			float deltatime = ServiceLocator<Time>::GetService()->GetDeltaTime();

			float a = m_text->GetPosition().x + (m_Xspeed * deltatime);

			m_text->SetPosition(sf::Vector2f(m_text->GetPosition().x + (m_Xspeed * deltatime),
				m_text->GetPosition().y - (m_Yspeed * deltatime)));
			m_text->SetScale(m_text->GetScale() * (1 + deltatime));

			m_colour.a += static_cast<sf::Uint8>(m_fadespeed * deltatime);
			m_text->SetColour(m_colour);

			m_expiredTime += deltatime;

			if (m_expiredTime > (m_time - 0.001))
				m_goingUp = false;

		}
		else
		{
			float deltatime = ServiceLocator<Time>::GetService()->GetDeltaTime();

			m_text->SetPosition(sf::Vector2f(m_text->GetPosition().x + (m_Xspeed * deltatime),
				m_text->GetPosition().y + (m_Yspeed * deltatime)));
			m_text->SetScale(m_text->GetScale() * (1 - deltatime));

			m_colour.a -= static_cast<sf::Uint8>(m_fadespeed * deltatime);
			m_text->SetColour(m_colour);

			if (m_colour.a <= 5.0f)
				m_update = false;
		}
	}
	else
	{
		m_owner->SetFlag(true);
	}
}

bool BoingEffect::Initialise(sf::Vector2f pPosition)
{
	m_time = 0.2f;
	m_originalY = pPosition.y;

	m_dY = 55;

	m_Yspeed = m_dY / m_time;
	m_Xspeed = 155;

	m_goingUp = true;

	m_colour.r = 255;
	m_colour.g = 255;
	m_colour.b = 255;
	m_colour.a = 50;

	m_fadespeed = (255 - m_colour.a) / m_time;

	m_expiredTime = 0.0f;

	m_update = true;
	return true;
}

void BoingEffect::SetActive(bool pState)
{
}
