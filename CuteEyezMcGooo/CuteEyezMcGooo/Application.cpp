#include "stdafx.h"
#include "Application.h"

#include "ServiceLocator.h"
#include "WorldSystem.h"
#include "AISystem.h"

//TEMP INCLUDES
#include "ControllerComponent.h"

#include "ControllerComponent.h"
#include "Events.h"

Application::Application()
{

}
Application::~Application()
{

}


bool Application::Initialise()
{
	m_window.create(sf::VideoMode(1024, 1024), "AI", sf::Style::Default);
	m_window.setFramerateLimit(60);

	m_clock = Time::Create();
	ServiceLocator<Time>::SetService(m_clock.get());
	Log::Message("Clock created.", Log::EOUTPUTTYPE::SUCCESSFUL);

	m_renderSystem = RenderSystem::Create(m_window);
	ServiceLocator<RenderSystem>::SetService(m_renderSystem.get());
	Log::Message("Rendersystem created.", Log::EOUTPUTTYPE::SUCCESSFUL);

	m_objectManager = ObjectManager::Create();
	ServiceLocator<ObjectManager>::SetService(m_objectManager.get());
	Log::Message("Object manager created.", Log::EOUTPUTTYPE::SUCCESSFUL);

	m_resourceManager = ResourceManager::Create();
	ServiceLocator<ResourceManager>::SetService(m_resourceManager.get());
	Log::Message("Resource manager created.", Log::SUCCESSFUL);

	m_textureManager = TextureManager::Create("../data/textures/");
	ServiceLocator<TextureManager>::SetService(m_textureManager.get());
	Log::Message("Texture manager created.", Log::EOUTPUTTYPE::SUCCESSFUL);

	m_factory = Factory::Create();
	ServiceLocator<Factory>::SetService(m_factory.get());
	Log::Message("Factory created.", Log::EOUTPUTTYPE::SUCCESSFUL);

	m_collisionSystem = CollisionSystem::Create();
	ServiceLocator<CollisionSystem>::SetService(m_collisionSystem.get());
	Log::Message("Collision system created.", Log::EOUTPUTTYPE::SUCCESSFUL);

	WorldSystem* ws = new WorldSystem();
	ServiceLocator<WorldSystem>::SetService(ws);
	Log::Message("World created.", Log::EOUTPUTTYPE::SUCCESSFUL);
	ws->CreateWorld("../data/level.wi");

	ServiceLocator<AISystem>::SetService(new AISystem());
	ServiceLocator<AISystem>::GetService()->Initialise();
	Log::Message("AI System Created.", Log::EOUTPUTTYPE::SUCCESSFUL);

	m_editor = LevelEditor::Create();
	ServiceLocator<LevelEditor>::SetService(m_editor.get());
	Log::Message("Level editor Created.", Log::EOUTPUTTYPE::SUCCESSFUL);

	m_nodeFactory = BTNodeFactory::Create();
	ServiceLocator<BTNodeFactory>::SetService(m_nodeFactory.get());
	Log::Message("Node factory Created.", Log::EOUTPUTTYPE::SUCCESSFUL);

	return true;
}

void Application::Run()
{
	m_running = true;

	while (m_running)
	{
		sf::Event event;
		while (m_window.pollEvent(event))
			HandleEvent(event);

		m_clock->Update();

		m_editor->Update();
		if (!m_objectManager->GetPause())
		{
			m_collisionSystem->Update();
			//m_collisionSystem->DrawDebug();
		}

		m_objectManager->Update();
		if (!m_objectManager->GetPause())
			ServiceLocator<AISystem>::GetService()->UpdateBTTrees();
		//m_renderSystem->Update();
	}
}

void Application::Cleanup()
{
}

void Application::HandleEvent(sf::Event& pEvent)
{
	switch (pEvent.type)
	{
	case sf::Event::Closed:
		m_window.close();
		m_running = false;
		break;
	case sf::Event::KeyPressed:
		if (pEvent.key.code == sf::Keyboard::Escape)
		{
			m_window.close();
			m_running = false;
		}
		if (pEvent.key.code == sf::Keyboard::Space)
		{
			m_objectManager->Pause();
		}
		if (pEvent.key.code == sf::Keyboard::Delete)
		{
			m_objectManager->Play();
		}
		if (pEvent.key.code == sf::Keyboard::BackSpace)
		{
			m_objectManager->Stop();
		}
		break;
	default:
		break;
	}
}
