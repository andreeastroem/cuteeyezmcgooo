#include "stdafx.h"
#include <vector>
#include "AISystem.h"
#include "Collider.h"
#include "GnollScript.h"
#include "BTTree.h"
#include "ObjectManager.h"
#include "ServiceLocator.h"
#include "ControllerComponent.h"
#include "Time.h"



GnollScript::GnollScript()
{
	m_objectManager = nullptr;
	m_aiSystem = nullptr;
}

GnollScript::~GnollScript()
{

}

void GnollScript::Initialise(BTTree* p_tree, sf::Vector2f p_startPosition)
{
	m_BTree = p_tree;
	m_startPosition = p_startPosition;
	m_objectManager = ServiceLocator<ObjectManager>::GetService();
	m_aiSystem = ServiceLocator<AISystem>::GetService();
	m_heroSpotted = false;
	m_ignoreDamage = false;
}

void GnollScript::Update()
{

	if (!m_controller)
	{
		m_collider = m_owner->GetComponent<Collider>(Component::COLLIDER);
		m_controller = m_owner->GetComponent<ControllerComponent>(Component::CONTROLLER);
		ControllerComponent::ControllerValues values;
		values.damage = 30;
		values.damaged = false;
		values.hasTargetPosition = false;
		values.hp = 100;
		values.hpLoss = 0;
		values.position = m_startPosition;
		values.speed = 100;
		values.speedMod = 1;
		values.targetPosition = sf::Vector2f();
		m_controller->Initialise(values);
	}


	//Sense
	if (m_lastSense > SenseInterval)
	{
		//Get Objects in range
		m_objectsInRange = m_objectManager->GetObjectsWithinRadius(m_owner->position, SenseRange);


		//Remove the once we already know about
		unsigned int i = 0, k, iC, kC = m_knownObjects.size();
		if (kC > 0)
		{
			while (i < m_objectsInRange.size())
			{
				for (k = 0; k < kC; k++)
				{
					if (m_objectsInRange[i] == m_owner || m_knownObjects[k] == m_objectsInRange[i])
					{
						m_objectsInRange.erase(m_objectsInRange.begin() + i--);
						break;
					}
				}
				i++;
			}
		}

		//See if we still know about all the once we think we know about
		i = 0;
		for (i = 0; i < m_knownObjects.size(); i++)
		{
			if (!m_aiSystem->CanSee(*m_collider->GetBody(), m_owner->rotation, m_knownObjects[i]->position, FoV))
				m_knownObjects.erase(m_knownObjects.begin() + i--);
			else
			{
				if (m_knownObjects[i]->GetType() == GameObject::WARRIOR)
				{
					if (!IsHeroSpotted())
						SpotHero(m_knownObjects[i]->position);
					else
						m_lastSpottedPosition = m_knownObjects[i]->position;
				}
			}
			m_lastSense = 0;
		}

		//See if we've found any new once
		iC = m_objectsInRange.size();
		for (i = 0; i < iC; i++)
		{
			if (m_objectsInRange[i] != m_owner && m_objectsInRange[i]->GetActive() &&
				m_aiSystem->CanSee(*m_collider->GetBody(), m_owner->rotation, m_objectsInRange[i]->position, FoV))
			{
				if (m_objectsInRange[i]->GetType() == GameObject::WARRIOR)
				{
					if (!IsHeroSpotted() && m_objectsInRange[i]->GetActive())
						SpotHero(m_objectsInRange[i]->position);
					else if (m_objectsInRange[i]->GetActive())
						m_lastSpottedPosition = m_objectsInRange[i]->position;
				}
				m_knownObjects.push_back(m_objectsInRange[i]);
			}
		}
	}
	m_lastSense += ServiceLocator<Time>::GetService()->GetDeltaTime();
}

void GnollScript::Cleanup()
{
	m_BTree->SetFlag(true);
	m_BTree = nullptr;
}

void GnollScript::OnCollision(GameObject* p_other)
{

}

void GnollScript::ExitCollision(GameObject* p_other)
{

}

bool GnollScript::GnollPartOfGroup(GameObject* p_gnoll)
{
	int g, gC = m_group.size();
	for (g = 0; g < gC; g++)
	{
		if (m_group[g] == p_gnoll)
			return true;
	}
	return false;
}

std::vector<GnollScript*> GnollScript::GetGnollsOutOfGroup(float p_range)
{
	std::vector<GameObject*> objects = m_objectManager->GetObjectsWithinRadius(m_owner->position, p_range);
	std::vector<GnollScript*> outOfGroupGnolls;
	if (!m_controller->HasTargetPosition() && m_controller->GetTargetObject())
		m_owner->LookTowards(m_controller->GetTargetObject()->position);

	int o = 0;
	int g, gC = m_group.size();
	outOfGroupGnolls.resize(0);
	for (o = 0; o < objects.size(); o++)
	{
		if (objects[o]->GetType() != GameObject::GNOLL || objects[o] == GetOwner())
		{
			objects.erase(objects.begin() + o--);
			continue;
		}
		bool inGroup = false;
		for (g = 0; g < gC; g++)
		{
			if (m_group[g] == objects[o])
			{
				inGroup = true;
				break;
			}
		}
		if (!inGroup && objects[o] != m_owner)
			outOfGroupGnolls.push_back(objects[o]->GetComponent<GnollScript>(Component::SCRIPT));
	}

	return outOfGroupGnolls;
}

void GnollScript::AddToGroup(GameObject* member)
{
	int g, gC = m_group.size();
	for (g = 0; g < gC; g++)
	{
		if (member == m_group[g])
			return;
	}
	m_group.push_back(member);
	member->GetComponent<GnollScript>(Component::SCRIPT)->AddToGroup(m_owner);
};

void GnollScript::Reset()
{
	m_ignoreDamage = false;
	m_heroSpotted = false;
	m_group.clear();
}

void GnollScript::SpotHero(sf::Vector2f p_position)
{ 
	if (!m_heroSpotted)
	{
		m_heroSpotted = true;
		m_lastSpottedPosition = p_position;
		m_controller->ModifySpeedModifier(10.f);
	}
};

void GnollScript::UnSpotHero()
{ 
	if (m_heroSpotted)
	{
		m_heroSpotted = false;
		m_lastSpottedPosition = sf::Vector2f(-1, -1);
		m_controller->ModifySpeedModifier(-10.f);
		m_group.clear();
	}
};

bool GnollScript::HasEnoughDamage()
{
	if (Damage * (m_group.size() + 1) >= GroupDamageToAttack || m_ignoreDamage) 
		return true; 
	return false;
};
