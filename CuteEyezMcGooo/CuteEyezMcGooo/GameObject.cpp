#include "stdafx.h"

#include "Factory.h"

#include "GameObject.h"

#include "ServiceLocator.h"
#include "WorldSystem.h"

//For Reset()
#include "ControllerComponent.h"

GameObject::GameObject()
{
	for (unsigned int i = 0; i < Component::EType::TYPESIZE; i++)
		m_component[i] = nullptr;

	m_activeFlag = true;
	m_destroyFlag = false;

	name = "";
}

GameObject::~GameObject()
{

}

void GameObject::Initialise(sf::Vector2f pPosition, GameObject::EObjectType pType, float pRotation)
{
	position = pPosition;
	rotation = pRotation;
	m_type = pType;

	if (m_type == WARRIOR && pType == WALL)
	{
		printf("what inside initialise\n");
	}
}

void GameObject::Update()
{
	for (unsigned int i = 0; i < Component::EType::TYPESIZE; i++)
	{
		if(m_component[i] != nullptr)
			m_component[i]->Update();
	}
}

void GameObject::Cleanup()
{
	for (unsigned int i = 0; i < Component::EType::TYPESIZE; i++)
	{
		DeleteComponent(static_cast<Component::EType>(i));
	}
}

void GameObject::AttachComponent(Component* pComponent, Component::EType pType)
{
	m_component[pType] = pComponent;
	pComponent->SetOwnerObject(this);
	pComponent->SetPosition(position);
	pComponent->SetRotation(rotation);
}

void GameObject::DeleteComponent(Component::EType pType)
{
	if (m_component[pType] != nullptr)
	{
		m_component[pType]->Cleanup();
		delete m_component[pType];
		m_component[pType] = nullptr;
	}
}

bool GameObject::GetActive()
{
	return m_activeFlag;
}

bool GameObject::GetFlag()
{
	return m_destroyFlag;
}

void GameObject::SetActive(bool pState)
{
	m_activeFlag = pState;
	int c, cC = 4;
	for (c = 0; c < cC; c++)
	{
		if (m_component[c])
			m_component[c]->SetActive(pState);
	}
}

void GameObject::SetFlag(bool pState)
{
	m_destroyFlag = pState;
}

void GameObject::OnCollision(GameObject* pOther)
{
	for (unsigned int i = 0; i < Component::TYPESIZE; i++)
	{
		if (m_component[i])
			m_component[i]->OnCollision(pOther);
	}
}
void GameObject::ExitCollision(GameObject* pOther)
{
	for (unsigned int i = 0; i < Component::TYPESIZE; i++)
	{
		if (m_component[i])
			m_component[i]->ExitCollision(pOther);
	}
}

void GameObject::SetPosition(sf::Vector2f pPosition)
{
	position = pPosition;

	for (unsigned int i = 0; i < Component::EType::TYPESIZE; i++)
	{
		if (m_component[i])
			m_component[i]->SetPosition(position);
	}
}

void GameObject::LookTowards(sf::Vector2f p_target)
{
	sf::Vector2f fwd = math::Normalise(position - p_target);
	float rot = atan2f(fwd.x, fwd.y);
	SetRotation(math::RADTODEG(rot));
}

void GameObject::SetForward(sf::Vector2f p_forward)
{
	float rot = atan2f(p_forward.x, p_forward.y);
	SetRotation(math::RADTODEG(rot));
}

void GameObject::SetRotation(float pRotation)
{
	rotation = pRotation;

	for (unsigned int i = 0; i < Component::EType::TYPESIZE; i++)
	{
		if (m_component[i])
			m_component[i]->SetRotation(rotation);
	}
}

GameObject::EObjectType GameObject::GetType()
{
	return m_type;
}

void GameObject::Reset()
{
	if (m_destroyFlag)
		m_destroyFlag = !m_destroyFlag;
	int c, cC = 4;
	for (c = 0; c < cC; c++)
	{
		if (m_component[c])
			m_component[c]->Reset();
	}

	m_activeFlag = true;
}
