#include "stdafx.h"

#include "RenderComponent.h"

#include "RenderSystem.h"

RenderSystem::RenderSystem(sf::RenderWindow& pWindow)
{
	sf::FloatRect rect;
	sf::View* view;
	sf::Vector2u& windowSize = pWindow.getSize();


	//Game
	rect = sf::FloatRect(0, 0.1f, 1.0f, 0.9f);
	view = new sf::View();
	view->setViewport(rect);
	view->setSize(sf::Vector2f(windowSize.x * rect.width, windowSize.y * rect.height));
	view->setCenter(sf::Vector2f(view->getSize().x *0.5f, view->getSize().y * 0.5f));
	m_cameras.push_back(view);


	//Editor
	rect = sf::FloatRect(0, 0.05f, 1, 0.05f);
	view = new sf::View();
	view->setViewport(rect);
	view->setSize(sf::Vector2f(windowSize.x * rect.width, windowSize.y * rect.height));
	view->setCenter(sf::Vector2f(view->getSize().x *0.5f, view->getSize().y * 0.5f));
	m_cameras.push_back(view);

	//EditorTop
	rect = sf::FloatRect(0, 0, 1, 0.05f);
	view = new sf::View(rect);
	view->setViewport(rect);
	view->setSize(sf::Vector2f(windowSize.x * rect.width, windowSize.y * rect.height));
	view->setCenter(sf::Vector2f(view->getSize().x *0.5f, view->getSize().y * 0.5f));
	m_cameras.push_back(view);

	m_window = &pWindow;

	m_renderQueues.push_back(new std::vector<sf::Drawable*>());
	m_renderQueues.push_back(new std::vector<sf::Drawable*>());
	m_renderQueues.push_back(new std::vector<sf::Drawable*>());
}
RenderSystem::~RenderSystem()
{
	m_window = nullptr;
	for (unsigned int i = 0; i < m_cameras.size(); i++)
	{
		if (m_cameras[i])
		{
			delete m_cameras[i];
			m_cameras[i] = nullptr;
		}
	}
}


RenderSystem::ptr RenderSystem::Create(sf::RenderWindow& pWindow)
{
	return RenderSystem::ptr(new RenderSystem(pWindow));
}
void RenderSystem::Update()
{
	ClearScreen(0x00, 0x00, 0x00);
	
	Sort();

	for (unsigned int v = 0; v < m_renderQueues.size(); v++)
	{
		m_window->setView(*m_cameras[v]);
		std::vector<sf::Drawable*>& m_viewObjs = *m_renderQueues[v];
		for (unsigned int i = 0; i < m_viewObjs.size(); i++)
		{
			if (m_viewObjs[i])
				Draw(m_viewObjs[i]);
		}
	}

	m_window->display();

	int q, qC = m_renderQueues.size();
	for (q = 0; q < qC; q++)
		m_renderQueues[q]->clear();
	m_layers.clear();
}
void RenderSystem::AddToQueue(sf::Drawable* pObject, unsigned int pLayer, EViewport pViewport)
{
	if (pObject)
	{
		m_renderQueues[pViewport]->push_back(pObject);

		if (pViewport == GAME)
			m_layers.push_back(pLayer);
	}
}
void RenderSystem::AddToQueue(std::vector<sf::Drawable*> pObjects, std::vector<unsigned int> pLayers, std::vector<EViewport> pViewports)
{
	for (unsigned int i = 0; i < pObjects.size(); i++)
	{
		if (pObjects[i])
		{
			m_renderQueues[pViewports[i]]->push_back(pObjects[i]);
			if (pViewports[i] == GAME)
				m_layers.push_back(pLayers[i]);
		}
	}
}

void RenderSystem::ClearScreen(sf::Uint8 pRed, sf::Uint8 pGreen, sf::Uint8 pBlue)
{
	m_window->clear(sf::Color(pRed, pGreen, pBlue));
}
void RenderSystem::Draw(sf::Drawable* pObject)
{
	m_window->draw(*pObject);
}

void RenderSystem::AddToCameras(sf::View* pCamera)
{
	m_cameras.push_back(pCamera);
}

void RenderSystem::Sort()
{
	int i, j;
	sf::Drawable* temp;
	unsigned int templayer;
	for (i = 1; i < m_renderQueues[GAME]->size(); i++)
	{
		for (j = i - 1; (j >= 0) && (m_layers[j] > m_layers[j+1]); j--)
		{
			temp = m_renderQueues[GAME]->at(j+1);
			templayer = m_layers[j+1];
			m_renderQueues[GAME]->at(j+1) = m_renderQueues[GAME]->at(j);
			m_layers[j+1] = m_layers[j];
			m_renderQueues[GAME]->at(j) = temp;
			m_layers[j] = templayer;
		}
	}
}
