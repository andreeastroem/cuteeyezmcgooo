#include "stdafx.h"
#include "Traps.h"
#include "GameObject.h"
#include "ObjectManager.h"
#include "ServiceLocator.h"
#include "RenderComponent.h"

//
void TrapScript::Disarm()
{
	m_armed = false;
}
//

SpikeTrap::SpikeTrap()
{
	m_initialised = false;
	m_controller = nullptr;
	m_hero = ServiceLocator<ObjectManager>::GetService()->GetObjectWithType(GameObject::WARRIOR);
	m_world = ServiceLocator<WorldSystem>::GetService();
	m_damage = 10;

	m_armed = true;
}

SpikeTrap::~SpikeTrap()
{
	Cleanup();
}

void SpikeTrap::Update()
{
	if (!m_initialised)
	{
		m_controller = m_owner->GetComponent<ControllerComponent>(Component::CONTROLLER);
		m_position = m_world->SnapToGrid(m_owner->position);
		m_initialised = true;
	}

	if (m_hero && m_armed)
	{
		sf::Vector2f heroPos = m_world->SnapToGrid(m_hero->position);
		if (heroPos == m_position)
			Trigger();
	}
	else
		m_hero = ServiceLocator<ObjectManager>::GetService()->GetObjectWithType(GameObject::WARRIOR);
}

void SpikeTrap::Cleanup()
{

}

void SpikeTrap::Trigger()
{
	m_armed = false;
	ControllerComponent* heroController = m_hero->GetComponent<ControllerComponent>(Component::CONTROLLER);
	heroController->AddToQue(new TaskM1<ControllerComponent, unsigned int>(heroController, &ControllerComponent::Damage, m_damage));
}

void SpikeTrap::OnEnter()
{

}

void SpikeTrap::OnExit()
{

}

GlueTrap::GlueTrap()
{
	m_initialised = false;
	m_onTrap = false;
	m_controller = nullptr;
	m_hero = ServiceLocator<ObjectManager>::GetService()->GetObjectWithType(GameObject::WARRIOR);
	m_world = ServiceLocator<WorldSystem>::GetService();

	m_armed = true;
}

GlueTrap::~GlueTrap()
{
	Cleanup();
}

void GlueTrap::Update()
{
	if (!m_initialised)
	{
		m_controller = m_owner->GetComponent<ControllerComponent>(Component::CONTROLLER);
		sf::Vector2f size = m_owner->GetComponent<RenderComponent>(Component::RENDER)->GetSize();
		size = sf::Vector2f(size.x * 0.5f, size.y * 0.5f);
		m_position = m_world->GraphicsToWorldScale(m_world->SnapToGrid(m_owner->position));
		m_initialised = true;
	}

	if (m_hero)
	{
		sf::Vector2f heroPos = m_world->GraphicsToWorldScale(m_world->SnapToGrid(m_hero->position));
		if (heroPos == m_position && !m_onTrap)
		{
			OnEnter();
			m_onTrap = true;
		}
		else if (heroPos != m_position && m_onTrap)
		{
			OnExit();
			m_onTrap = false;
		}
	}
	else
		m_hero = ServiceLocator<ObjectManager>::GetService()->GetObjectWithType(GameObject::WARRIOR);
}

void GlueTrap::Cleanup()
{

}

void GlueTrap::Trigger()
{
}

void GlueTrap::OnEnter()
{
	ControllerComponent* heroController = m_hero->GetComponent<ControllerComponent>(Component::CONTROLLER);
	heroController->AddToQue(new TaskM1<ControllerComponent, float>(heroController, &ControllerComponent::ModifySpeedModifier, -m_slowdown));
}

void GlueTrap::OnExit()
{
	ControllerComponent* heroController = m_hero->GetComponent<ControllerComponent>(Component::CONTROLLER);
	heroController->AddToQue(new TaskM1<ControllerComponent, float>(heroController, &ControllerComponent::ModifySpeedModifier, m_slowdown));
}
