#pragma once

#ifndef AI_SYSTEM_H_
#define AI_SYSTEM_H_
#include <vector>
#include "WorldSystem.h"
#include <memory>

#include "BTTree.h"

class b2Body;

class AISystem
{
public:
	AISystem()
	{
	}

	~AISystem()
	{
	}

	/************************************************************************/
	/* START OF BT TREE
	/************************************************************************/
public:
	enum EBTType
	{
		WARRIORHERO,
		GNOLL,
	};

	BTTree* FindAFittingBTTree();
	BTTree* CreateBTTree(EBTType pType, GameObject* pActor);
	void UpdateBTTrees();
	void DestroyBTTree(unsigned int pIndex);
	bool CanSee(b2Body& p_from, float p_fromRotation, sf::Vector2f& p_to, float p_fov);

private:
	std::vector<BTTree*> m_trees;

	/************************************************************************/
	/* END OF BT TREE
	/* START OF A*
	/************************************************************************/
public:

	std::vector<Node*> GetPath(Node* p_from, Node* p_to);
	Node* GetPathToAdjacentNodeThatDoesNotInvolveTargetNode(Node* p_from, Node* p_to);

private:
	struct WeightedNode
	{
		WeightedNode(Node* p_node, float p_f, float p_g, int p_h, WeightedNode* p_parent = nullptr)
		{
			m_parent = p_parent;
			m_node = p_node;
			m_f = p_f;
			m_g = p_g;
			m_h = p_h;
		}

		WeightedNode* m_parent;
		Node* m_node;
		float m_f, m_g;
		int m_h;

		bool operator > (WeightedNode p_node)
		{
			if (m_f > p_node.m_f)
				return true;
			return false;
		}
		bool operator < (WeightedNode p_node)
		{
			if (m_f < p_node.m_f)
				return true;
			return false;
		}
	};

	/************************************************************************/
	/* END OF A*
	/* START OF HERO
	/************************************************************************/
public:
	void Initialise();
	void Sees(sf::Vector2f pPosition, sf::Vector2f pDirection, float pLength);
	bool HasBeenTo(unsigned int pX, unsigned int pY);
	void BeenTo(unsigned int pX, unsigned int pY);
	void ResetTrees();
	sf::Vector2f GetPositionFromNode(unsigned int pX, unsigned int pY);

private:
	std::vector<std::vector<Node*>> m_mappedWorld;
	std::vector<std::vector<bool>> m_visited;
};


#endif