#include "stdafx.h"
#include "AISystem.h"
#include "ServiceLocator.h"

#include "BTNode.h"
#include "BTNodeFactory.h"
#include "CollisionSystem.h"

#include <iostream>
#include "GameObject.h"
#include "Box2D.h"

std::vector<Node*> AISystem::GetPath(Node* p_from, Node* p_to)
{
	//Function Preparation
	WeightedNode* start = new WeightedNode(p_from, 0, 0, 0);
	std::vector<WeightedNode*> openlist;
	std::vector<WeightedNode*> closedlist;
	openlist.push_back(start);
	WorldSystem& world = *ServiceLocator<WorldSystem>::GetService();


	//Loop Preparation
	WeightedNode* current = openlist[0];
	int currentI;
	sf::Vector2i goalPos = p_to->GetPosition();

	//Lambdas
	auto GetLowestF = [&]() -> int
	{
		WeightedNode* lowestF = openlist[0];
		int n, nC = openlist.size(), lowestIndex = 0;
		for (n = 0; n < nC; n++)
		{
			if (lowestF->m_f > openlist[n]->m_f)
			{
				lowestF = openlist[n];
				lowestIndex = n;
			}
		}
		return lowestIndex;
	};
	auto InClosedlist = [&](Node& node) -> bool
	{
		int n, nC = closedlist.size();
		for (n = 0; n < nC; n++)
		{
			if (node == *closedlist[n]->m_node)
				return true;
		}

		return false;
	};
	auto InOpenlist = [&](Node& node, int& out) -> bool
	{
		int n, nC = openlist.size();
		for (n = 0; n < nC; n++)
		{
			if (node == *openlist[n]->m_node)
			{
				out = n;
				return true;
			}
		}

		return false;
	};
	auto Backtrack = [&](WeightedNode* end, Node* start) -> std::vector<Node*>
	{
		std::vector<Node*> path;
		WeightedNode* current = end;
		while (current->m_node != start)
		{
			path.push_back(current->m_node);
			current = current->m_parent;
		}

		int n, nC = openlist.size();
		for (n = 0; n < nC; n++)
			delete openlist[n];
		nC = closedlist.size();
		for (n = 0; n < nC; n++)
			delete closedlist[n];

		closedlist.clear();
		openlist.clear();
		return path;
	};
	auto CheckNeighbor = [&](int x, int y)
	{
		sf::Vector2i parentPos = current->m_node->GetPosition();
		sf::Vector2i neighborPos(parentPos.x + x, parentPos.y + y);
		Node* n = &world.GetNode(neighborPos.x, neighborPos.y);
		if (n->IsWalkable() && !InClosedlist(*n))
		{
			int openIndex = 0;

			float g, f;
			int h;
			sf::Vector2i deltaPos = neighborPos - parentPos;
			g = current->m_g + 1 + n->GetTerrain();
			if (deltaPos.x != 0 && deltaPos.y != 0)
				g += 0.414f;
			h = abs(goalPos.x - neighborPos.x) + abs(goalPos.y - neighborPos.y);
			f = g + h;

			if (InOpenlist(*n, openIndex))
			{
				WeightedNode* neighborNode = openlist[openIndex];
				if (neighborNode->m_f < f)
					*neighborNode = WeightedNode(n, f, g, h, current);
			}

			else
			{
				WeightedNode* neighborNode = new WeightedNode(n, f, g, h, current);
				openlist.push_back(neighborNode);
			}
		}
	};

	while (openlist.size() > 0)
	{
		currentI = GetLowestF();
		current = openlist[currentI];
		openlist.erase(openlist.begin() + currentI);
		closedlist.push_back(current);

		if (current->m_node == p_to)
			return Backtrack(current, p_from);

		//Neighbor Check
		//CheckNeighbor(-1, -1);
		CheckNeighbor(0, -1);
		CheckNeighbor(1, -1);

		CheckNeighbor(-1,0);
		CheckNeighbor(1, 0);

		//CheckNeighbor(-1, 1);
		CheckNeighbor(0, 1);
		CheckNeighbor(1, 1);
		
	}

	int n, nC = openlist.size();
	for (n = 0; n < nC; n++)
		delete openlist[n];
	nC = closedlist.size();
	for (n = 0; n < nC; n++)
		delete closedlist[n];

	closedlist.clear();
	openlist.clear();

	return std::vector<Node*>();
}

Node* AISystem::GetPathToAdjacentNodeThatDoesNotInvolveTargetNode(Node* p_from, Node* p_to)
{
	int x = p_to->GetPosition().x;
	int y = p_to->GetPosition().y;
	WorldSystem* world = ServiceLocator<WorldSystem>::GetService();
	
	Node* to;
	std::vector<Node*> path;

	bool foundA = false;
	int index = 1;

	while (index < 5)
	{
		switch (index)
		{
#pragma region leftof
		case 1:
			if ((x - 1) > 0)
			{
				if (world->IsWalkable((x - 1), y))
				{
					to = &world->GetNode((x - 1), y);
					path = GetPath(p_from, to);

					for (unsigned int i = 0; i < path.size(); i++)
					{
						if (path[i] == p_to)
							foundA = true;
					}
					if (!foundA)
						return to;
				}
			}
			break;
#pragma endregion left of
#pragma region rightof
		case 2:
			if ((x + 1) < world->GetWidth())
			{
				if (world->IsWalkable((x + 1), y))
				{
					to = &world->GetNode((x + 1), y);
					path = GetPath(p_from, to);

					for (unsigned int i = 0; i < path.size(); i++)
					{
						if (path[i] == p_to)
							foundA = true;
					}
					if (!foundA)
						return to;
				}
			}
			break;
#pragma endregion rightof
#pragma region below
		case 3:
			if ((y + 1) < world->GetHeight())
			{
				if (world->IsWalkable(x, (y + 1)))
				{
					to = &world->GetNode(x, (y + 1));
					path = GetPath(p_from, to);

					for (unsigned int i = 0; i < path.size(); i++)
					{
						if (path[i] == p_to)
							foundA = true;
					}
					if (!foundA)
						return to;
				}
			}
			break;
#pragma endregion below
#pragma region above
		case 4:
			if ((y - 1) < world->GetHeight())
			{
				if (world->IsWalkable(x, (y - 1)))
				{
					to = &world->GetNode(x, (y - 1));
					path = GetPath(p_from, to);

					for (unsigned int i = 0; i < path.size(); i++)
					{
						if (path[i] == p_to)
							foundA = true;
					}
					if (!foundA)
						return to;
				}
			}
			break;
#pragma endregion above
		}
	}
}

BTTree* AISystem::FindAFittingBTTree()
{
	Log::Message("Not yet implemented: FindAFittingBTTree()", Log::ERROR);
	return nullptr;
}
BTTree* AISystem::CreateBTTree(EBTType pType, GameObject* pActor)
{
	BTTree* tree = ServiceLocator<BTNodeFactory>::GetService()->CreateBTTree(pType, pActor);
	if (tree)
	{
		/*std::cout << "type: " << tree->GetData()->GetType() << std::endl;
		printf("%p\r\n", tree->GetData());*/
		m_trees.push_back(tree);
	}

	return tree;
}
void AISystem::UpdateBTTrees()
{
	//SKA INTE FUNKA S�H�R SEN
	std::vector<unsigned int> indices;
	for (unsigned int i = 0; i < m_trees.size(); i++)
	{
		if (m_trees[i]->GetFlag())
			indices.push_back(i);
	}
	for (int i = indices.size()-1; i >= 0; i--)
		DestroyBTTree(indices[i]);
	
	for (unsigned int i = 0; i < m_trees.size(); i++)
	{
		if (m_trees[i]->GetData()->GetActive())
			m_trees[i]->Process();
	}
	
}
void AISystem::DestroyBTTree(unsigned int pIndex)
{
	if (pIndex < m_trees.size())
	{
		if (m_trees[pIndex])
		{
			delete m_trees[pIndex];
			m_trees[pIndex] = nullptr;
			m_trees.erase(m_trees.begin() + pIndex);
		}
	}
}

void AISystem::Initialise()
{
	WorldSystem* world = ServiceLocator<WorldSystem>::GetService();
	for (int w = 0; w < world->GetWidth(); w++)
	{
		std::vector<bool> visit;
		std::vector<Node*> column;
		for (int h = 0; h < world->GetHeight(); h++)
		{
			Node* node = new Node;
			node->Init(w, h, false);
			column.push_back(node);
			visit.push_back(false);
		}
		m_visited.push_back(visit);
		m_mappedWorld.push_back(column);
	}
}

void AISystem::Sees(sf::Vector2f pPosition, sf::Vector2f pDirection, float pLength)
{
	WorldSystem* world = ServiceLocator<WorldSystem>::GetService();
	sf::Vector2f target = sf::Vector2f(pPosition.x + pDirection.x * pLength, pPosition.y + pDirection.y * pLength);
	target = ServiceLocator<CollisionSystem>::GetService()->IntersectRaycast(pPosition, target);

	sf::Vector2f tilepos;
	unsigned int tilesize = world->GetTileSize();

	while (math::Distance(pPosition, target) > (tilesize / 2))
	{
		tilepos = world->GraphicsToWorldScale(sf::Vector2f(pPosition.x + (pDirection.x * tilesize / 2), pPosition.y + (pDirection.y * tilesize / 2)));
		m_mappedWorld[(unsigned int)tilepos.x][(unsigned int)tilepos.y]->SetWalkable(true);
		pPosition = sf::Vector2f(pPosition.x + (pDirection.x * tilesize / 2), pPosition.y + (pDirection.y * tilesize / 2));
	}
}

bool AISystem::CanSee(b2Body& p_from, float p_fromRotation, sf::Vector2f& p_to, float p_fov)
{
	sf::Vector2f from = sf::Vector2f(p_from.GetPosition().x, -p_from.GetPosition().y);
	if (from != p_to)
	{
		float d = math::Distance(p_to - from);
		WorldSystem& world = *ServiceLocator<WorldSystem>::GetService();
		{
			RaycastCallback rccb;
			p_from.GetWorld()->RayCast(&rccb, b2Vec2(from.x, -from.y), b2Vec2(p_to.x, -p_to.y));
			std::vector<GameObject*> intersects = rccb.GetAll();

			int i, iC = intersects.size();
			for (i = 0; i < iC; i++)
			{
				sf::Vector2f iPos = intersects[i]->position;
				if (!world.GetNode(world.GraphicsToWorldScale(iPos)).IsWalkable() && math::Distance(iPos - from) < d)
					return false;
			}
		}
		sf::Vector2f dir = math::Normalise(sf::Vector2f(p_to.x, p_to.y) - sf::Vector2f(from.x, from.y));
		float cs = cosf(math::DEGTORAD(p_fromRotation - 90)), sn = sinf(math::DEGTORAD(p_fromRotation - 90));
		sf::Vector2f fwd = sf::Vector2f(cs, -sn);

		float ang = math::RADTODEG(acosf(math::Dot(fwd, dir)));

		if (ang < (p_fov *0.5f) && ang > -(p_fov * 0.5f))
			return true;
		return false;
	}
	return false;
}

bool AISystem::HasBeenTo(unsigned int pX, unsigned int pY)
{
	return m_visited[pX][pY];
}

void AISystem::BeenTo(unsigned int pX, unsigned int pY)
{
	m_visited[pX][pY] = true;
}

sf::Vector2f AISystem::GetPositionFromNode(unsigned int pX, unsigned int pY)
{
	return ServiceLocator<WorldSystem>::GetService()->WorldToGraphicsScale(sf::Vector2f((float)pX, (float)pY));
}

void AISystem::ResetTrees()
{
	int t, tC = m_trees.size();
	for (t = 0; t < tC; t++)
		m_trees[t]->Reset();
}
