#include "stdafx.h"
#include "LevelEditor.h"
#include "ServiceLocator.h"
#include "RenderSystem.h"
#include "Events.h"
#include "ButtonScript.h"
#include <iostream>
#include <fstream>
#include "RenderComponent.h"
#include "Time.h"
#include "AISystem.h"


//Checks so that stuff is valid

LevelEditor::LevelEditor()
{
	auto LoadTypeAndCosts = [&]()
	{
		std::string line;
		std::ifstream file("../data/editorobjects.conf");
		if (!file.is_open())
			return;

		int b, bC = EBrushType::COUNT;
		for (b = 0; b < bC; b++)
		{
			m_brushes.push_back(std::vector<Brush>());
			m_brushObjects.push_back(std::vector<GameObject*>());
		}

		while (std::getline(file, line))
		{
			if (line[0] == '#')
			{
				line = line.substr(1);
				std::size_t found = line.find(",");
				if (found != std::string::npos)
				{
					//Factory Type
					std::string factoryType = line.substr(0, found), editorType = "\r\n";
					int cost = -1, walkable = -1, amount = -1;
					float toughness = -1;
					line = line.substr(++found);

					//EditorType
					found = line.find(",");
					if (found != std::string::npos)
					{
						editorType = line.substr(0, found);
						line = line.substr(++found);

						//Cost
						cost = stoi(line);

						//Toughness
						found = line.find(",");
						if (found != std::string::npos)
						{
							line = line.substr(++found);
							toughness = stof(line);
						}
						else
						{
							Log::Message("Failed to read Toughness Value", Log::ERROR);
							return;
						}

						//Walkable
						found = line.find(",");
						if (found != std::string::npos)
						{
							line = line.substr(++found);
							walkable = stoi(line);
						}
						else
						{
							Log::Message("Failed to read Walkable Value", Log::ERROR);
							return;
						}

						//Amount to Spawn
						found = line.find(",");
						if (found != std::string::npos)
						{
							line = line.substr(++found);
							amount = stoi(line);
						}
						else
						{
							Log::Message("Failed to read Amount Value", Log::ERROR);
							return;
						}
					}
					else
					{
						Log::Message("Failed to read EditorType Value", Log::ERROR);
						return;
					}


					if (editorType != "\r\n" && cost != -1 && toughness != -1 && walkable != -1)
					{
						#pragma region ObjectType
						GameObject::EObjectType objType = GameObject::ICON;
						if (factoryType == "Wall")
							objType = GameObject::WALL;
						else if (factoryType == "Hound")
							objType = GameObject::HOUND;
						else if (factoryType == "Gold")
							objType = GameObject::GOLD;
						else if (factoryType == "Warrior")
							objType = GameObject::WARRIOR;
						else if (factoryType == "GlueTrap")
							objType = GameObject::GLUETRAP;
						else if (factoryType == "SpikeTrap")
							objType = GameObject::SPIKETRAP;
						else if (factoryType == "Gnoll")
							objType = GameObject::GNOLL;
						#pragma endregion

						#pragma region burshType
						EBrushType brushType = EBrushType::ENVIRONMENTS;
						if (editorType == "Environment")
							brushType = EBrushType::ENVIRONMENTS;
						else if (editorType == "Monster")
							brushType = EBrushType::MONSTERS;
						else if (editorType == "Trap")
							brushType = EBrushType::TRAPS;
						#pragma endregion


						//Change Brush Click Stuff
						ChangeBrushClick* clickTask = new ChangeBrushClick(this, &LevelEditor::ChangeBrush, m_brushes[brushType].size());
						GameObject* obj = new GameObject();
						obj->Initialise(sf::Vector2f((m_brushObjects[brushType].size() * 50.0f) + 25.0f, m_world->GetTileSize() * 0.25f), GameObject::ICON);

						//Render Component
						obj->AttachComponent(m_factory->CreateRenderComponent(objType, (int)(m_world->GetTileSize() * 0.5f), RenderSystem::EDITOR), Component::RENDER);

						//Button
						ButtonScript* btn = new ButtonScript(obj);
						btn->RegisterOnClick(clickTask);
						obj->AttachComponent(btn, Component::SCRIPT);
						m_objectManager->AttachGameObject(obj);


						m_brushes[brushType].push_back(Brush(objType, cost, toughness, walkable == 0 ? false : true, amount));
						m_brushObjects[brushType].push_back(obj);
					}
				}
			}
		}
		ChangeToType(MONSTERS);
		ChangeToType(TRAPS);
		ChangeToType(ENVIRONMENTS);


		int bT;
		bC = m_brushes.size();
		for (bT = 0; bT < bC; bT++)
		{
			if (m_brushes[bT].size() > 0)
			{
				ChangeBrushTypeClick* clickTask = new ChangeBrushTypeClick(this, &LevelEditor::ChangeToType, (EBrushType) bT);
				GameObject* obj = new GameObject();
				obj->Initialise(sf::Vector2f((bT * 50.0f) + 25.0f, m_world->GetTileSize() * 0.25f), GameObject::ICON);

				obj->AttachComponent(m_factory->CreateRenderComponent(m_brushes[bT][0].m_type, (int)(m_world->GetTileSize() * 0.5f), RenderSystem::EDITORTOP), Component::RENDER);

				ButtonScript* btn = new ButtonScript(obj);
				btn->RegisterOnClick(clickTask);
				obj->AttachComponent(btn, Component::SCRIPT);
				m_objectManager->AttachGameObject(obj);

			}
		}
		m_panning = false;
	};

	m_objectManager = ServiceLocator<ObjectManager>::GetService();
	m_world = ServiceLocator<WorldSystem>::GetService();
	m_factory = ServiceLocator<Factory>::GetService();
	m_renderer = ServiceLocator<RenderSystem>::GetService();
	m_time = ServiceLocator<Time>::GetService();
	m_window = m_renderer->GetRenderWindow();
	m_gameView = ServiceLocator<RenderSystem>::GetService()->GetView(RenderSystem::GAME);
	float halftile = m_world->GetTileSize() * 0.5f;
	m_tileHalf = sf::Vector2f(halftile, halftile);
	m_resources = 10;
	m_resourceText = ServiceLocator<Factory>::GetService()->CreateGameObject(GameObject::TEXT, sf::Vector2f(0, 0));

	LoadTypeAndCosts();
 	auto CreatePlayPauseStop = [&]()
 	{
		sf::Vector2f viewSize = m_renderer->GetView(RenderSystem::EDITORTOP)->getSize();
 		//Play
 		//Change Brush Click Stuff
 		TaskM0<LevelEditor>* clickTask = new TaskM0<LevelEditor>(this, &LevelEditor::Play);
 		GameObject* obj = new GameObject();
		obj->Initialise(sf::Vector2f(viewSize.x - 75, m_world->GetTileSize() * 0.25f), GameObject::ICON);
 
 		//Render Component
		obj->AttachComponent(m_factory->CreateRenderComponent(GameObject::PLAYICON, (int)(m_world->GetTileSize() * 0.5f), RenderSystem::EDITORTOP), Component::RENDER);
 
 		//Button
 		ButtonScript* btn = new ButtonScript(obj);
 		btn->RegisterOnClick(clickTask);
 		obj->AttachComponent(btn, Component::SCRIPT);
 		m_objectManager->AttachGameObject(obj);
 
 		//Pause
 		//Change Brush Click Stuff
 		clickTask = new TaskM0<LevelEditor>(this, &LevelEditor::Pause);
		obj = new GameObject();
		obj->Initialise(sf::Vector2f(viewSize.x - 50, m_world->GetTileSize() * 0.25f), GameObject::ICON);
 
 		//Render Component
		obj->AttachComponent(m_factory->CreateRenderComponent(GameObject::PAUSEICON, (int)(m_world->GetTileSize() * 0.5f), RenderSystem::EDITORTOP), Component::RENDER);
 
 		//Button
 		btn = new ButtonScript(obj);
 		btn->RegisterOnClick(clickTask);
 		obj->AttachComponent(btn, Component::SCRIPT);
 		m_objectManager->AttachGameObject(obj);
 
 		//Stop
 		//Change Brush Click Stuff
 		clickTask = new TaskM0<LevelEditor>(this, &LevelEditor::Stop);
		obj = new GameObject();
		obj->Initialise(sf::Vector2f(viewSize.x - 25, m_world->GetTileSize() * 0.25f), GameObject::ICON);
 
 		//Render Component
		obj->AttachComponent(m_factory->CreateRenderComponent(GameObject::STOPICON, (int)(m_world->GetTileSize() * 0.5f), RenderSystem::EDITORTOP), Component::RENDER);
 
 		//Button
 		btn = new ButtonScript(obj);
 		btn->RegisterOnClick(clickTask);
 		obj->AttachComponent(btn, Component::SCRIPT);
 		m_objectManager->AttachGameObject(obj);
 
 	};
 	CreatePlayPauseStop();
	Stop();
}

LevelEditor::~LevelEditor()
{

}

void LevelEditor::Update()
{
	sf::Text* resourceText = static_cast<sf::Text*>(m_resourceText->GetComponent<RenderComponent>(Component::RENDER)->GetDrawable());
	std::string a = "Resources: ";
	a.append(std::to_string(m_resources));
	resourceText->setString(a);
	if (!m_isPlaying)
	{
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && !m_leftBtn)
		{
			Draw();
			m_leftBtn = true;
		}
		else if (!sf::Mouse::isButtonPressed(sf::Mouse::Left) && m_leftBtn)
			m_leftBtn = false;
		if (sf::Mouse::isButtonPressed(sf::Mouse::Right) && !m_rightBtn)
		{
			Erase();
			m_rightBtn = true;
		}
		else if (!sf::Mouse::isButtonPressed(sf::Mouse::Right) && m_rightBtn)
			m_rightBtn = false;
	}
	else if (!sf::Mouse::isButtonPressed(sf::Mouse::Right) && m_rightBtn)
		m_rightBtn = false;
	MoveCamera();
}

void LevelEditor::MoveCamera()
{
	float delta = m_time->GetDeltaTime();
	sf::Vector2f movement = sf::Vector2f(0, 0);
	if (sf::Mouse::isButtonPressed(sf::Mouse::Middle) && !m_panning)
	{
		m_panning = true;
		m_lastMouse = sf::Mouse::getPosition();
	}
	else if (sf::Mouse::isButtonPressed(sf::Mouse::Middle) && m_panning)
	{
		movement = (sf::Vector2f)(m_lastMouse - sf::Mouse::getPosition()) * m_mouseSense;
		m_lastMouse = sf::Mouse::getPosition();
	}
	else
	{
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Left) || sf::Keyboard::isKeyPressed(sf::Keyboard::A))
			movement.x -= 1;
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Right) || sf::Keyboard::isKeyPressed(sf::Keyboard::D))
			movement.x += 1;
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Up) || sf::Keyboard::isKeyPressed(sf::Keyboard::W))
			movement.y -= 1;
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::Down) || sf::Keyboard::isKeyPressed(sf::Keyboard::S))
			movement.y += 1;
		if (m_panning)
			m_panning = false;
	}

	if (movement != sf::Vector2f(0, 0))
	{
		if (sf::Keyboard::isKeyPressed(sf::Keyboard::LShift))
			movement *= 5.0f;
		m_renderer->GetView(RenderSystem::GAME)->move(movement * m_moveSpeed * delta);
	}
}

void LevelEditor::Draw()
{
	sf::Vector2f viewPos = m_window->mapPixelToCoords(sf::Mouse::getPosition(*m_window), *m_gameView);
	if (viewPos.x > 0 && viewPos.y > 0)
	{
		Erase();
		if (m_resources - m_brush.m_cost >= 0)
		{
			m_resources -= m_brush.m_cost;
			sf::Vector2f pos = m_world->SnapToGrid(viewPos);
			Node& node = m_world->GetNode(m_world->GraphicsToWorldScale(pos));

			std::vector<GameObject*> m_drawnObjects;
			int a;
			for (a = 0; a < m_brush.m_amout; a++)
			{
				int xOff = (a % 2) * 5;
				m_drawnObjects.push_back(m_factory->CreateGameObject(m_brush.m_type, pos + sf::Vector2f(xOff, xOff % 1)));
			}
			node.SetOccupant(m_drawnObjects, m_brush.m_toughness, m_brush.m_walkable);
		}
		else
			ServiceLocator<Factory>::GetService()->CreateGameObject(GameObject::TEXTEFFECT, viewPos, "Need More $$ Bills 'yall");
	}
}

void LevelEditor::Erase()
{
	sf::Vector2f viewPos = m_window->mapPixelToCoords(sf::Mouse::getPosition(*m_window), *m_gameView);

	if (viewPos.x > 0 && viewPos.y > 0)
	{
		sf::Vector2f pos = m_world->SnapToGrid(viewPos);
		Node& node = m_world->GetNode(m_world->GraphicsToWorldScale(pos));
		if (node.IsOccupied())
		{
			GameObject::EObjectType type = node.GetOccupant()[0]->GetType();
			node.RemoveOccupant();

			int t, tC = m_brushes.size(), st, stC;
			for (t = 0; t < tC; t++)
			{
				stC = m_brushes[t].size();
				for (st = 0; st < stC; st++)
				{
					if (type == m_brushes[t][st].m_type)
						m_resources += m_brushes[t][st].m_cost;
				}
			}
		}
	}

}

void LevelEditor::Play()
{
	GameObject* hero = ServiceLocator<ObjectManager>::GetService()->GetObjectWithType(GameObject::WARRIOR);
	GameObject* gold = ServiceLocator<ObjectManager>::GetService()->GetObjectWithType(GameObject::GOLD);
	if (gold && hero)
	{
		Node& hN = m_world->GetNode(m_world->GraphicsToWorldScale(hero->position));
		Node& gN = m_world->GetNode(m_world->GraphicsToWorldScale(gold->position));
		std::vector<Node*> testPath = ServiceLocator<AISystem>::GetService()->GetPath(&hN, &gN);
		if (testPath.size() > 0)
		{
			m_isPlaying = true;
			m_objectManager->Play();
		}
		else
			ServiceLocator<Factory>::GetService()->CreateGameObject(GameObject::TEXTEFFECT, sf::Vector2f(200, 200), "The warrior has to be able to get to the gold");
	}
	else
		ServiceLocator<Factory>::GetService()->CreateGameObject(GameObject::TEXTEFFECT, sf::Vector2f(200, 200), "Place Warrior and Gold before Starting");
}

void LevelEditor::Pause()
{
	m_objectManager->Pause();
}

void LevelEditor::Stop()
{
	m_objectManager->Stop();
	m_isPlaying = false;
}

void LevelEditor::ChangeBrush(int p_subtype)
{
	m_subType = p_subtype;

	m_brush = m_brushes[m_brushType][m_subType];
}

void LevelEditor::ChangeToType(EBrushType p_type)
{
	if (m_brushType != p_type)
	{
		int b, bC = m_brushObjects[m_brushType].size();
		for (b = 0; b < bC; b++)
			m_brushObjects[m_brushType][b]->SetActive(false);

		m_brushType = p_type;
		m_subType = 0;

		m_brush = m_brushes[m_brushType][m_subType];

		bC = m_brushObjects[m_brushType].size();
		for (b = 0; b < bC; b++)
			m_brushObjects[m_brushType][b]->SetActive(true);
	}
}
