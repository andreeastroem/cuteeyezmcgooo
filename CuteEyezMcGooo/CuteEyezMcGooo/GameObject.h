//Shell for gameobject

#pragma once

#include "Component.h"
#include "SFML/Graphics.hpp"

class GameObject
{
public:
	enum EObjectType
	{
		//monsters
		HENRIK,	//Is actually g�blin <- inside joke and whatnot
		HOUND,
		GNOLL,
		//heroes
		WARRIOR,
		//traps
		SPIKETRAP,
		GLUETRAP,
		//interactives
		//pickups
		GOLD,
		//walls
		WALL,
		//icons
		//effects
		TEXTEFFECT,
		//GUI
		TEXT,
		//rest
		ICON,
		PLAYICON,
		PAUSEICON,
		STOPICON,
		
		DEFAULT,
	};
public:
	GameObject();
	~GameObject();
	
	void Initialise(sf::Vector2f pPosition, GameObject::EObjectType pType = GameObject::WALL, float pRotation = 0.0f);
	void Update();
	void Cleanup();
	void AttachComponent(Component* pComponent, Component::EType pType);
	void OnCollision(GameObject* pOther);
	void ExitCollision(GameObject* pOther);

	//Access functions
	template <class T>
	bool HasComponentOfType(Component::EType pType);
	template <class T>
	T* GetComponent(Component::EType pType);

	bool GetActive();
	bool GetFlag();

	void SetActive(bool pState);
	void SetFlag(bool pState);

	void SetPosition(sf::Vector2f pPosition);
	void SetRotation(float pRotation);
	void LookTowards(sf::Vector2f p_target);
	void SetForward(sf::Vector2f p_forward);

	EObjectType GetType();

	void Reset();

private:
	void DeleteComponent(Component::EType pType);
public:
	sf::Vector2f position;
	float rotation;
	
	std::string name;
private:
	Component* m_component[Component::EType::TYPESIZE];
	
	GameObject::EObjectType m_type;
	bool m_destroyFlag;
	bool m_activeFlag;
};

/************************************************************************/
/* Access template classes
/************************************************************************/

template <class T>
bool GameObject::HasComponentOfType(Component::EType pType)
{
	if (m_component[pType] != nullptr)	//validate component of type
		return true;

	return false;						//Else
}

template <class T>
T* GameObject::GetComponent(Component::EType pType)
{
	if (HasComponentOfType<T>(pType))
		return static_cast<T*>(m_component[pType]);
	else
		return nullptr;
}