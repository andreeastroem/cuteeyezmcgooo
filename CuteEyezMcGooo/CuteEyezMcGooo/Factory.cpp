#include "stdafx.h"

#include "Factory.h"

#include "RenderSystem.h"
#include "ServiceLocator.h"
#include "WorldSystem.h"
#include "ObjectManager.h"
#include "TextureManager.h"
#include "BTNodeFactory.h"

#include "AISystem.h"
#include "BTTree.h"

/************************************************************************/
/* List of components
/************************************************************************/
#include "Component.h"

#include "ControllerComponent.h"

/************************************************************************/
/* Render
/************************************************************************/
#include "RectangleComponent.h"
#include "SpriteComponent.h"
#include "TextComponent.h"

/************************************************************************/
/* Collider
/************************************************************************/
#include "BoxCollider.h"
#include "CircleCollider.h"

/************************************************************************/
/* SCRIPTS
/************************************************************************/
#include "WarriorScript.h"
#include "GnollScript.h"
#include "Hound.h"
#include "Traps.h"
#include "BoingEffect.h"

Factory::Factory()
{

}
Factory::~Factory()
{

}

Factory::ptr Factory::Create()
{
	return Factory::ptr(new Factory());
}
GameObject* Factory::CreateGameObject(GameObject::EObjectType pType, sf::Vector2f pPosition, std::string pName)
{
	int tileSize = ServiceLocator<WorldSystem>::GetService()->GetTileSize();

	GameObject* object = new GameObject;
	object->Initialise(pPosition, pType);
	object->name = pName;

	ControllerComponent::ControllerValues cv = ControllerComponent::ControllerValues();
	switch (pType)
	{
	case GameObject::HENRIK:
		object->AttachComponent(CreateRenderComponent(pType, tileSize, RenderSystem::GAME), Component::RENDER);
		object->AttachComponent(CreateColliderComponent(pType, pPosition, tileSize), Component::COLLIDER);
		object->AttachComponent(CreateControllerComponent(pType), Component::CONTROLLER);

		break;
	case GameObject::WARRIOR:
		object->AttachComponent(CreateRenderComponent(pType, tileSize, RenderSystem::GAME), Component::RENDER);
		object->AttachComponent(CreateColliderComponent(pType, pPosition, tileSize), Component::COLLIDER);
		object->AttachComponent(CreateControllerComponent(pType), Component::CONTROLLER);
		object->AttachComponent(CreateScriptComponent(pType, object, pPosition), Component::SCRIPT);
		break;
	case GameObject::WALL:
		object->AttachComponent(CreateRenderComponent(pType, tileSize, RenderSystem::GAME), Component::RENDER);
		object->AttachComponent(CreateColliderComponent(pType, pPosition, tileSize), Component::COLLIDER);
		break;
	case GameObject::GOLD:
		object->AttachComponent(CreateRenderComponent(pType, tileSize, RenderSystem::GAME), Component::RENDER);
		object->AttachComponent(CreateColliderComponent(pType, pPosition, tileSize), Component::COLLIDER);
		break;
	case GameObject::SPIKETRAP:
	case GameObject::GLUETRAP:
		object->AttachComponent(CreateRenderComponent(pType, tileSize, RenderSystem::GAME), Component::RENDER);
		object->AttachComponent(CreateColliderComponent(pType, pPosition, tileSize), Component::COLLIDER);
		object->AttachComponent(CreateControllerComponent(pType), Component::CONTROLLER);
		cv.position = pPosition;
		object->GetComponent<ControllerComponent>(Component::CONTROLLER)->Initialise(cv);
		object->AttachComponent(CreateScriptComponent(pType, object, pPosition), Component::SCRIPT);
		break;
	case GameObject::GNOLL:
		object->AttachComponent(CreateRenderComponent(pType, tileSize, RenderSystem::GAME), Component::RENDER);
		object->AttachComponent(CreateColliderComponent(pType, pPosition, tileSize), Component::COLLIDER);
		object->AttachComponent(CreateControllerComponent(pType), Component::CONTROLLER);
		object->AttachComponent(CreateScriptComponent(pType, object, pPosition), Component::SCRIPT);
		break;
	case GameObject::TEXT:
		object->AttachComponent(CreateRenderComponent(pType, tileSize, RenderSystem::GAME, pPosition), Component::RENDER);
		break;
	case GameObject::TEXTEFFECT:
		object->AttachComponent(CreateRenderComponent(pType, tileSize, RenderSystem::GAME), Component::RENDER);
		object->AttachComponent(CreateScriptComponent(pType, object, pPosition), Component::SCRIPT);
		break;
	default:
		Log::Message("Type does not exist, or it has not yet been implemented", Log::ERROR);
		break;
	}

	ServiceLocator<ObjectManager>::GetService()->AttachGameObject(object);
	return object;
}
Component* Factory::CreateRenderComponent(GameObject::EObjectType pType, int pTileSize, RenderSystem::EViewport view, sf::Vector2f pPosition)
{
	Component* component;
	sf::Texture* texture;
	unsigned int id;
	sf::Vector2f size;

	switch (pType)
	{
	case GameObject::HENRIK:
		component = new RectangleComponent();
		size = sf::Vector2f(pTileSize * .16f, pTileSize * .1f);
		static_cast<RectangleComponent*>(component)->Initialise(size, sf::Color::Green);
		((RenderComponent*) component)->m_view = view;
		((RenderComponent*) component)->SetSize(size);
		return component;
		break;
	case GameObject::WARRIOR:
		component = new SpriteComponent();
		id = ServiceLocator<TextureManager>::GetService()->LoadTexture("Warrior");
		texture = ServiceLocator<TextureManager>::GetService()->GetTexture(id);
		static_cast<SpriteComponent*>(component)->Initialise(id, sf::Vector2f(static_cast<float>(pTileSize) / texture->getSize().x, static_cast<float>(pTileSize) / texture->getSize().y));
		((RenderComponent*) component)->m_view = view;
		((RenderComponent*) component)->SetSize(size);
		((RenderComponent*)component)->SetLayer(2);
		return component;
		break;
	case GameObject::GNOLL:
		component = new SpriteComponent();
		id = ServiceLocator<TextureManager>::GetService()->LoadTexture("Gnoll");
		texture = ServiceLocator<TextureManager>::GetService()->GetTexture(id);
		static_cast<SpriteComponent*>(component)->Initialise(id, sf::Vector2f(static_cast<float>(pTileSize) / texture->getSize().x * .4f, static_cast<float>(pTileSize) / texture->getSize().y * .4f));
		((RenderComponent*) component)->m_view = view;
		((RenderComponent*) component)->SetSize(size);
		((RenderComponent*)component)->SetLayer(2);
		return component;
		break;
	case GameObject::WALL:
		component = new SpriteComponent();
		//Transform function, target scale / texture x & y. T.ex. 10/64 = 0.15625
		id = ServiceLocator<TextureManager>::GetService()->LoadTexture("wall");
		texture = ServiceLocator<TextureManager>::GetService()->GetTexture(id);
		static_cast<SpriteComponent*>(component)->Initialise(id, sf::Vector2f(static_cast<float>(pTileSize) / texture->getSize().x, static_cast<float>(pTileSize) / texture->getSize().y));
		((RenderComponent*) component)->m_view = view;
		((RenderComponent*) component)->SetSize(sf::Vector2f((float)pTileSize, (float)pTileSize));
		return component;
		break;
	case GameObject::GOLD:
		component = new RectangleComponent();
		size = sf::Vector2f(pTileSize * .4f, pTileSize * .4f);
		static_cast<RectangleComponent*>(component)->Initialise(size, sf::Color::Yellow);
		((RenderComponent*) component)->m_view = view;
		((RenderComponent*) component)->SetSize(size);
		((RenderComponent*)component)->SetLayer(2);
		return component;
		break;
	case GameObject::SPIKETRAP:
		component = new RectangleComponent();
		size = sf::Vector2f((float)pTileSize, (float)pTileSize);
		static_cast<RectangleComponent*>(component)->Initialise(size, sf::Color::Magenta);
		((RenderComponent*) component)->m_view = view;
		((RenderComponent*) component)->SetSize(size);
		((RenderComponent*) component)->SetLayer(0);
		return component;
	case GameObject::GLUETRAP:
		component = new RectangleComponent();
		size = sf::Vector2f((float)pTileSize, (float)pTileSize);
		static_cast<RectangleComponent*>(component)->Initialise(size, sf::Color::Green);
		((RenderComponent*) component)->m_view = view;
		((RenderComponent*) component)->SetSize(size);
		((RenderComponent*) component)->SetLayer(0);
		return component;
	case GameObject::PLAYICON:
		component = new SpriteComponent();
		//Transform function, target scale / texture x & y. T.ex. 10/64 = 0.15625
		id = ServiceLocator<TextureManager>::GetService()->LoadTexture("play");
		texture = ServiceLocator<TextureManager>::GetService()->GetTexture(id);
		static_cast<SpriteComponent*>(component)->Initialise(id, sf::Vector2f(static_cast<float>(pTileSize) / texture->getSize().x, static_cast<float>(pTileSize) / texture->getSize().y));
		((RenderComponent*) component)->m_view = view;
		((RenderComponent*)component)->SetSize(sf::Vector2f((float)pTileSize, (float)pTileSize));
		return component;
	case GameObject::PAUSEICON:
		component = new SpriteComponent();
		//Transform function, target scale / texture x & y. T.ex. 10/64 = 0.15625
		id = ServiceLocator<TextureManager>::GetService()->LoadTexture("pause");
		texture = ServiceLocator<TextureManager>::GetService()->GetTexture(id);
		static_cast<SpriteComponent*>(component)->Initialise(id, sf::Vector2f(static_cast<float>(pTileSize) / texture->getSize().x, static_cast<float>(pTileSize) / texture->getSize().y));
		((RenderComponent*) component)->m_view = view;
		((RenderComponent*)component)->SetSize(sf::Vector2f((float)pTileSize, (float)pTileSize));
		return component;
	case GameObject::STOPICON:
		component = new SpriteComponent();
		//Transform function, target scale / texture x & y. T.ex. 10/64 = 0.15625
		id = ServiceLocator<TextureManager>::GetService()->LoadTexture("stop");
		texture = ServiceLocator<TextureManager>::GetService()->GetTexture(id);
		static_cast<SpriteComponent*>(component)->Initialise(id, sf::Vector2f(static_cast<float>(pTileSize) / texture->getSize().x, static_cast<float>(pTileSize) / texture->getSize().y));
		((RenderComponent*) component)->m_view = view;
		((RenderComponent*)component)->SetSize(sf::Vector2f((float)pTileSize, (float)pTileSize));
		return component;
	case GameObject::TEXTEFFECT:
		component = new TextComponent();
		static_cast<TextComponent*>(component)->SetPosition(pPosition);
		return component;
	case GameObject::TEXT:
		component = new TextComponent();
		static_cast<TextComponent*>(component)->SetPosition(pPosition);
		((RenderComponent*) component)->SetLayer(5);
		return component;
		break;
		break;
	default:
		Log::Message("No such component type exists or it has not yet been fully supported", Log::ERROR);
		break;
	}

	return nullptr;
}
Component* Factory::CreateControllerComponent(GameObject::EObjectType pType)
{
	Component* component;

	switch (pType)
	{
	case GameObject::HENRIK:
	case GameObject::SPIKETRAP:
	case GameObject::GLUETRAP:
	case GameObject::WALL:
	case GameObject::GNOLL:
		component = new ControllerComponent();
		return component;
		break;
	case GameObject::WARRIOR:
		component = new ControllerComponent();
		return component;
		break;
	default:
		Log::Message("No such component type exists or it has not yet been fully supported", Log::ERROR);
		break;
	}

	return nullptr;
}
Component* Factory::CreateColliderComponent(GameObject::EObjectType pType, sf::Vector2f pPosition, int pTileSize)
{
	Component* component;

	switch (pType)
	{
	case GameObject::HENRIK:
		component = new BoxCollider();
		static_cast<BoxCollider*>(component)->Initialise(*m_world, pPosition, sf::Vector2f(pTileSize * .16f, pTileSize * .1f), true);
		return component;
		break;
	case GameObject::WARRIOR:
		component = new BoxCollider();
		static_cast<BoxCollider*>(component)->Initialise(*m_world, pPosition, sf::Vector2f(pTileSize * .4f, pTileSize * .4f), true);
		return component;
		break;
	case GameObject::GNOLL:
		component = new BoxCollider();
		static_cast<BoxCollider*>(component)->Initialise(*m_world, pPosition, sf::Vector2f(pTileSize * .4f, pTileSize * .4f), true);
		return component;
		break;
	case GameObject::WALL:
		component = new BoxCollider();
		static_cast<BoxCollider*>(component)->Initialise(*m_world, pPosition, sf::Vector2f(static_cast<float>(pTileSize), static_cast<float>(pTileSize)));
		return component;
		break;
	case GameObject::GOLD:
		component = new BoxCollider();
		static_cast<BoxCollider*>(component)->Initialise(*m_world, pPosition, sf::Vector2f(pTileSize * .4f, pTileSize * .4f), false, true);
		return component;
		break;

	case GameObject::SPIKETRAP:
	case GameObject::GLUETRAP:
		component = new BoxCollider();
		static_cast<BoxCollider*>(component)->Initialise(*m_world, pPosition, sf::Vector2f((float)pTileSize, (float)pTileSize), false, true);
		return component;

	default:
		Log::Message("No such component type exists or it has not yet been fully supported", Log::ERROR);
		break;
	}
	return nullptr;
}
Component* Factory::CreateScriptComponent(GameObject::EObjectType pType, GameObject* pActor, sf::Vector2f pStartPosition)
{
	Component* component;
	BTTree* tree;

	switch (pType)
	{
	case GameObject::HENRIK:
		Log::Message("Object type have not native script. Henrik", Log::ERROR);
		break;
	case GameObject::WARRIOR:
		component = new WarriorScript();
		tree = ServiceLocator<AISystem>::GetService()->CreateBTTree(AISystem::WARRIORHERO, pActor);
		static_cast<WarriorScript*>(component)->Initialise(tree, pStartPosition);
		return component;
		break;
	case GameObject::GNOLL:
		component = new GnollScript();
		tree = ServiceLocator<AISystem>::GetService()->CreateBTTree(AISystem::GNOLL, pActor);
		static_cast<GnollScript*>(component)->Initialise(tree, pStartPosition);
		return component;
		break;
	case GameObject::SPIKETRAP:
		component = new SpikeTrap();
		return component;
	case GameObject::GLUETRAP:
		component = new GlueTrap();
		return component;
	case GameObject::TEXTEFFECT:
		component = new BoingEffect();
		static_cast<BoingEffect*>(component)->Initialise(pStartPosition);
		return component;
		break;
	default:
		Log::Message("Object type have not native script. Default.", Log::ERROR);
		break;
	}

	return nullptr;
}

void Factory::Setb2World(b2World* pWorld)
{
	m_world = pWorld;
}






