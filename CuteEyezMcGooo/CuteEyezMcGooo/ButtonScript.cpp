#include "stdafx.h"
#include "ButtonScript.h"
#include "ServiceLocator.h"
#include "RenderComponent.h"
#include "GameObject.h"
#include "Time.h"



ButtonScript::ButtonScript(GameObject* p_owner)
{
	m_onClick = nullptr;
	m_render = ServiceLocator<RenderSystem>().GetService();
	RenderComponent* rc = p_owner->GetComponent<RenderComponent>(Component::EType::RENDER);
	m_view = rc->m_view;

};

void ButtonScript::Update()
{
	if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && m_lastClick > m_clickCD)
	{
		sf::Vector2f viewPos = m_render->GetRenderWindow()->mapPixelToCoords(sf::Mouse::getPosition(*m_render->GetRenderWindow()), *m_render->GetView(m_view));
		RenderComponent* rc = m_owner->GetComponent<RenderComponent>(Component::EType::RENDER);
		sf::Vector2f pos = ((sf::Sprite*)rc->GetDrawable())->getPosition();
		sf::FloatRect bounds = sf::FloatRect(pos.x - 15, pos.y - 15, pos.x + 15, pos.y + 15);

		if (viewPos.x <= bounds.width && viewPos.x >= bounds.left &&
			viewPos.y <= bounds.height && viewPos.y >= bounds.top)
		{
			m_onClick->Run();
		}
		m_lastClick = 0;
	}
	m_lastClick += ServiceLocator<Time>::GetService()->GetDeltaTime();
}
