//Component - utility for the gameobjects
#pragma once

class GameObject;

class Component
{
public:
	enum EType
	{
		RENDER,
		COLLIDER,
		CONTROLLER,
		SCRIPT,
		TYPESIZE
	};
public:
	Component();
	~Component();

	/********************IMPORTANT NOTE**************************************/
	/* All components should implement an Initialise() function
	/* however since they would all need their own set of parameters it
	/* is not made into a pure virtual function
	/*
	/* Box render example:
	/* Initialise(vector2 size, vector2 position)
	/************************************************************************/

	virtual void Reset(){};
	virtual void Update() = 0;
	virtual void Cleanup() = 0;
	virtual void OnCollision(GameObject* pOther);
	virtual void ExitCollision(GameObject* pOther);

	//Access functions
	virtual void SetOwnerObject(GameObject* pObject);
	GameObject* GetOwner();
	bool GetActive();
	virtual void SetActive(bool pState);
	EType GetType();
	void SetType(EType pType);
	virtual void SetPosition(sf::Vector2f pPosition);
	virtual void SetRotation(float pRotation);

protected:
	GameObject* m_owner;
	bool m_activeFlag;
	EType m_type;
};