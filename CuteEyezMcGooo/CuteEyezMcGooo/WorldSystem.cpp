#include "stdafx.h"
#include "WorldSystem.h"
#include "ServiceLocator.h"
#include "Factory.h"
#include <iostream>
#include <fstream>
#include <string>
#include "ObjectManager.h"


void Node::Init(const int p_x, const int p_y, bool p_walkable, bool p_occupied, std::vector<GameObject*> p_occupants, float p_terrainToughness)
{
	m_x = p_x;
	m_y = p_y;
	m_walkable = p_walkable;
	m_occupied = p_occupied;
	int o, oC = p_occupants.size();
	for (o = 0; o < oC; o++)
		m_occupants.push_back(p_occupants[o]);
	m_terrainToughness = p_terrainToughness;
}

void Node::RemoveOccupant()
{
	m_occupied = false;
	m_terrainToughness = 0;
	m_walkable = true;
	int o, oC = m_occupants.size();
	for (o = 0; o < oC; o++)
		ServiceLocator<ObjectManager>::GetService()->DeleteGameObject(m_occupants[o]);
	m_occupants.clear();
};

void Node::SetOccupant(std::vector<GameObject*> p_occupants, float p_terrainToughness, bool p_walkable)
{
	int o, oC = p_occupants.size();
	for (o = 0; o < oC; o++)
		m_occupants.push_back(p_occupants[o]);
	m_occupied = true;
	m_walkable = p_walkable;
	m_terrainToughness = p_terrainToughness;

}

void Node::SetOccupant(GameObject* p_occupant, float p_terrainToughness, bool p_walkable)
{
	m_occupants.push_back(p_occupant);
	m_occupied = true;
	m_walkable = p_walkable;
	m_terrainToughness = p_terrainToughness;
}

const sf::Vector2f WorldSystem::Forward = sf::Vector2f(0, 1);

WorldSystem::WorldSystem()
{

}
WorldSystem::~WorldSystem()
{
	delete [] m_world;
}

bool WorldSystem::CreateWorld(const char* p_path)
{
	auto GetArgument = [&](std::string p_from, std::string p_lookFor, const char* p_endToken, std::string& p_out)
	{
		std::transform(p_from.begin(), p_from.end(), p_from.begin(), ::tolower);
		std::size_t found = p_from.find(p_lookFor);
		int argIndex = -1;
		if (found != std::string::npos)
		{
			argIndex = found;
			argIndex += p_lookFor.length();

			std::string p_sub = p_from.substr(argIndex, p_from.length() - 1);

			found = p_sub.find(p_endToken);
			if (found != std::string::npos)
				p_out = p_sub.substr(0, found);
		}
	};

	Factory& factory = *ServiceLocator<Factory>::GetService();

	#pragma region ReadFile
	//Storage
	int widht, height, scale, offset, x, y;
	widht = height = scale = offset = x = y = -1;
	bool worldStart = false;

	//File Variables
	std::string line, arg;
	std::ifstream file(p_path, std::ifstream::binary);
	if (file.is_open())
	{
		while (std::getline(file, line))
		{
			#pragma region DimentionsAndScales
			arg = "\0";
			if (widht < 0)
			{
				GetArgument(line, "width=\"", "\"", arg);
				if (arg != "\0")
				{
					widht = stoi(arg);
					arg = "\0";
				}
			}
			if (height < 0)
			{
				GetArgument(line, "height=\"", "\"", arg);
				if (arg != "\0")
				{
					height = stoi(arg);
					arg = "\0";
				}
			}
			if (scale < 0)
			{
				GetArgument(line, "scale=\"", "\"", arg);
				if (arg != "\0")
				{
					scale = stoi(arg);
					arg = "\0";
				}
			}
			if (offset < 0)
			{
				GetArgument(line, "offset=\"", "\"", arg);
				if (arg != "\0")
				{
					offset = stoi(arg);
					arg = "\0";
				}
			}
			#pragma endregion

			#pragma region CreateObjects
			if (widht != -1 && height != -1 && scale != -1 && offset != -1)
			{
				if (!worldStart)
				{
					CreateWorld(widht, height, scale, offset);
					std::size_t found = line.find("[s]");
					if (found != std::string::npos)
					{
						worldStart = true;
						x = 0;
						y = 0;
					}
				}
				else
				{
					x = 0;

					const char* str = line.c_str();
					while (x < m_width && str[x] != '\r')
					{
						sf::Vector2f pos = sf::Vector2f(x*(scale + offset) + (scale * 0.5f), y*(scale + offset) + (scale * 0.5f));

						switch (str[x])
						{
						case '@':
							m_world[x + (y*m_height)].SetOccupant(factory.CreateGameObject(GameObject::EObjectType::WALL, pos), 0, false);
							break;
						default:
							break;
						}

						x++;
					}


					if (y++ == height)
					{
						return false;
						break;
					}
				}
			}
			#pragma endregion
		}
		Log::Message("Loaded Level", Log::SUCCESSFUL);
		return true;
	}
	else
		return false;
	#pragma endregion
}
bool WorldSystem::CreateWorld(const int p_width, const int p_height, const int p_scale, const int p_offset)
{
	m_width = p_width;
	m_height = p_height;
	m_scale = p_scale;
	m_offset = p_offset;

	m_world = new Node[m_width * m_height];
	int x, y;
	for (y = 0; y < m_height; y++)
	{
		for (x = 0; x < m_width; x++)
			m_world[x + (y*m_width)].Init(x, y, true);
	}
	return true;
}

bool WorldSystem::IsWalkable(const int p_x, const int p_y)
{
	return m_world[p_x + (m_width * p_y)].IsWalkable();
}
void WorldSystem::SetWalkable(const int p_x, const int p_y, bool p_walkable)
{
	m_world[p_x + (m_width * p_y)].SetWalkable(p_walkable);
}

int WorldSystem::GetTileSize()
{
	return m_scale;
}

Node& WorldSystem::GetNode(sf::Vector2i p_pos)
{
	int n, nC = m_height * m_width;
	if (p_pos.x * p_pos.y < nC)
	{
		for (n = 0; n < nC; n++)
		{
			Node& current = m_world[n];
			if (current.GetPosition() == p_pos)
				return current;
		}
	}
	return m_world[0];
}
Node& WorldSystem::GetNode(const int p_i)
{
	if (p_i > m_height* m_width && p_i > -1)
		return m_world[p_i];
	return m_world[0];
}

sf::Vector2f WorldSystem::WorldToGraphicsScale(sf::Vector2f p_position)
{
	p_position.x = p_position.x * (m_scale + m_offset) + (m_scale * 0.5f);
	p_position.y = p_position.y * (m_scale + m_offset) + (m_scale * 0.5f);
	return p_position;
}
sf::Vector2f WorldSystem::GraphicsToWorldScale(sf::Vector2f p_position)
{
	p_position.x = (p_position.x - m_scale * 0.5f) / (m_scale + m_offset);
	p_position.y = (p_position.y - m_scale * 0.5f) / (m_scale + m_offset);
	return p_position;
}
sf::Vector2f WorldSystem::SnapToGrid(sf::Vector2f p_position)
{
	p_position = GraphicsToWorldScale(p_position);
	sf::Vector2i roundedPosition = (sf::Vector2i)p_position;
	p_position -= (sf::Vector2f)roundedPosition;
 	if (p_position.x >= 0.5f)
 		roundedPosition.x++;
 	if (p_position.y >= 0.5f)
 		roundedPosition.y++;

	p_position = WorldToGraphicsScale((sf::Vector2f)roundedPosition);
	return p_position;
}

int WorldSystem::GetWidth()
{
	return m_width;
}

int WorldSystem::GetHeight()
{
	return m_height;
}

sf::Vector2f WorldSystem::GetSize()
{
	return sf::Vector2f((float)m_width, (float)m_height);
}
