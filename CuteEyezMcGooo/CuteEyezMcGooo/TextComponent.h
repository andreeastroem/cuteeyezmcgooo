#pragma once

#include "RenderComponent.h"

class TextComponent : public RenderComponent
{
public:
	TextComponent();
	~TextComponent();

	virtual sf::Drawable* GetDrawable() override;

	virtual sf::Vector2f GetPosition() override;
	virtual void SetColour(sf::Color pColour) override;
	virtual void SetScale(sf::Vector2f pScale) override;
	virtual sf::Vector2f GetScale() override;

	virtual void Update() override;

	virtual void SetPosition(sf::Vector2f pPosition) override;

private:
	sf::Text* m_text;
};