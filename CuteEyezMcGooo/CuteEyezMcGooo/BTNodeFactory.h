//A factory for creating different nodes and attaching them to trees
#pragma once

#include "BTNode.h"
#include "BTTree.h"
#include <memory>
#include "AISystem.h"
#include "GameObject.h"

class BTNodeFactory
{
	BTNodeFactory();
public:
	~BTNodeFactory();

	typedef std::unique_ptr<BTNodeFactory> ptr;
	static ptr Create();

	BTTree* CreateBTTree(AISystem::EBTType pType, GameObject* pOwner);

private:
	BTTree* CreateWarriorHero(BTTree* pTree, GameObject* pOwner);
	BTTree* CreateGnollTree(BTTree* p_tree, GameObject* p_owner);
};