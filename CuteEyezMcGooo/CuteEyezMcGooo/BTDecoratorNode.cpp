#include "stdafx.h"

#include "BTDecoratorNode.h"


BTDecoratorNode::BTDecoratorNode()
{

}

BTDecoratorNode::~BTDecoratorNode()
{
	if (m_child)
	{
		delete m_child;
		m_child = nullptr;
	}
}

BTNode::BTResult BTDecoratorNode::Process()
{
	return BTNode::FAILURE;
}

BTNode::BTResult BTDecoratorNode::ProcessNode(BTNode* pNode)
{
	BTNode::BTResult result = pNode->Process();
	if (result != BTNode::RUNNING)
	{
		pNode->Deinitialise();
		return result;
	}

	return result;
}
