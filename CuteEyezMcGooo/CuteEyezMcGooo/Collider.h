#pragma once

#include "Box2D.h"
#include "GameObject.h"
#include "Component.h"

class Collider : public Component
{
public:
	enum EType
	{
		BOX,
		CIRCLE,
		TYPESIZE
	};

public:
	Collider();
	~Collider();

	//Essential functions
	void Update();
	void Cleanup();

	virtual void EnterCollision(Collider* pCollider) = 0;
	virtual void LeaveCollision(Collider* pCollider) = 0;


	virtual void ApplyForce(sf::Vector2f pForce);
	virtual void SetVelocity(sf::Vector2f pVelocity);
	virtual sf::Vector2f GetVelocity();
	virtual sf::Vector2f GetPosition();
	virtual EType GetType();
	virtual float GetRotationInDegrees();
	void SetPosition(sf::Vector2f pPosition);
	virtual void SetRotation(float pDegrees);
	virtual b2Body* GetBody();
	

	void SetCollisionGroup(int pGroupIndex);

	GameObject* Raycast(sf::Vector2f pStartPosition, sf::Vector2f pEndPosition);
	bool CanSee(sf::Vector2f pStartPosition, sf::Vector2f pEndPosition, GameObject::EObjectType pType = GameObject::DEFAULT);

private:
	b2Vec2 m_prevPosition;
	float32 m_prevRotation;
protected:

protected:
	b2Body* m_body;
	b2BodyDef m_bodyDef;
	b2FixtureDef m_fixtureDef;

	EType m_type;
};

class RaycastCallback : public b2RayCastCallback
{
public:
	RaycastCallback();
	virtual float32 ReportFixture(b2Fixture* fixture, const b2Vec2& point, const b2Vec2& normal, float32 fraction) override;

	GameObject* GetClosest();
	bool Found(GameObject::EObjectType pType = GameObject::DEFAULT);
	sf::Vector2f GetIntersectionPoint();

	std::vector<GameObject*> GetAll();

private:
	GameObject* m_object;
	b2Body* m_body;
	sf::Vector2f m_intersectPoint;
	float m_fraction;
	sf::Vector2f m_normal;

	std::vector<GameObject*> m_objects;
};