#include "stdafx.h"

#include "BoxCollider.h"

#include "GameObject.h"

BoxCollider::BoxCollider()
{

}
BoxCollider::~BoxCollider()
{
	Collider::Cleanup();
}

void BoxCollider::EnterCollision(Collider* pCollider)
{
	m_owner->OnCollision(pCollider->GetOwner());
}
void BoxCollider::LeaveCollision(Collider* pCollider)
{
	m_owner->ExitCollision(pCollider->GetOwner());
}
bool BoxCollider::Initialise(b2World& pWorld, sf::Vector2f pPosition, sf::Vector2f pSize, bool pDynamic /*= false*/, bool pIsSensor /*= false*/, float pDegrees /*= 0.0f*/)
{
	if (!pDynamic)
	{
		m_bodyDef.position.Set(pPosition.x, -pPosition.y);
		m_bodyDef.angle = math::DEGTORAD(pDegrees);

		m_body = pWorld.CreateBody(&m_bodyDef);

		m_b2shape.SetAsBox(pSize.x / 2, pSize.y / 2);

		m_fixtureDef.isSensor = pIsSensor;

		m_fixtureDef.shape = &m_b2shape;
		m_fixtureDef.friction = 0.0f;

		m_body->CreateFixture(&m_fixtureDef);
	}
	else
	{
		m_bodyDef.type = b2_dynamicBody;
		m_bodyDef.position.Set(pPosition.x, -pPosition.y);
		m_bodyDef.angularDamping = 1.0f;
		m_bodyDef.angle = math::DEGTORAD(pDegrees);

		m_body = pWorld.CreateBody(&m_bodyDef);

		m_b2shape.SetAsBox(pSize.x / 2, pSize.y / 2);

		m_fixtureDef.isSensor = pIsSensor;
		m_fixtureDef.shape = &m_b2shape;
		m_fixtureDef.density = 1.0f;
		m_fixtureDef.friction = 0.3f;

		m_body->CreateFixture(&m_fixtureDef);
	}

	m_body->SetUserData(this);

	return true;
}
