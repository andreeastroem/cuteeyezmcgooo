#include "stdafx.h"

#include "GameObject.h"
#include "GnollScript.h"

#include "BTLeafNodes.h"

#include "AISystem.h"
#include "WorldSystem.h"
#include "ServiceLocator.h"
#include "ObjectManager.h"
#include "Time.h"
#include "Factory.h"

#include "Component.h"
#include "ControllerComponent.h"
#include "Collider.h"

//Random
#include <ctime>
#include <cstdlib>

//----------------------------------------
BTWalkAlongPath::BTWalkAlongPath()
{

}
BTWalkAlongPath::~BTWalkAlongPath()
{
	for (unsigned int i = 0; i < m_path.size(); i++)
		m_path[i] = nullptr;
	if (m_actor)
		m_actor = nullptr;
}

BTNode::BTResult BTWalkAlongPath::Process()
{

	if (CheckConditions() != BTNode::SUCCESS)
		return BTNode::FAILURE;

	while (m_index < m_path.size())
	{
		if (m_path[m_path.size() - m_index - 1]->IsWalkable() && 
			ServiceLocator<WorldSystem>::GetService()->GetNode(ServiceLocator<WorldSystem>::GetService()->GraphicsToWorldScale(ServiceLocator<WorldSystem>::GetService()->SnapToGrid(m_actor->position))).IsWalkable())
		{
			sf::Vector2f position = (sf::Vector2f)m_path[m_path.size() - m_index - 1]->GetPosition();
			position = ServiceLocator<WorldSystem>::GetService()->WorldToGraphicsScale(position);

			ControllerComponent::EResult result = m_actor->GetComponent<ControllerComponent>(Component::CONTROLLER)->Move(position);
			if (result == ControllerComponent::SUCCESS)
			{
				ServiceLocator<AISystem>::GetService()->BeenTo(m_path[m_path.size() - m_index - 1]->GetPosition().x, m_path[m_path.size() - m_index - 1]->GetPosition().y);
				m_index++;
			}

			//m_actor->LookTowards(m_actor->position + sf::Vector2f(1, 0));
			return BTNode::RUNNING;
		}
		else
		{
			m_actor->GetComponent<ControllerComponent>(Component::CONTROLLER)->SetVelocity(sf::Vector2f());
			m_actor->GetComponent<ControllerComponent>(Component::CONTROLLER)->ArrivedToTargetPosition();
			return BTNode::FAILURE;
		}
	}

	if (m_index == m_path.size())
	{
		m_actor->GetComponent<ControllerComponent>(Component::CONTROLLER)->SetVelocity(sf::Vector2f());
		m_actor->GetComponent<ControllerComponent>(Component::CONTROLLER)->ArrivedToTargetPosition();
	}

	return BTNode::SUCCESS;
}

void BTWalkAlongPath::Initialise()
{
	m_actor = m_tree->GetData();

	m_targetPosition = m_actor->GetComponent<ControllerComponent>(Component::CONTROLLER)->GetTargetPosition();

	sf::Vector2f homepos = ServiceLocator<WorldSystem>::GetService()->GraphicsToWorldScale(ServiceLocator<WorldSystem>::GetService()->SnapToGrid(m_actor->position));
	sf::Vector2f targetpos = ServiceLocator<WorldSystem>::GetService()->GraphicsToWorldScale(m_targetPosition);

	Node* homeNode = &ServiceLocator<WorldSystem>::GetService()->GetNode(homepos);
	Node* targetNode = &ServiceLocator<WorldSystem>::GetService()->GetNode(targetpos);

	m_path = ServiceLocator<AISystem>::GetService()->GetPath(homeNode, targetNode);

	//WHACK
// 	if (m_path.size() == 0)
// 		printf("whoop");

	m_index = 0;
}

void BTWalkAlongPath::Deinitialise()
{
	m_index = 0;
	Node* node;
	for each (node in m_path)
	{
		node = nullptr;
	}
}

//------------------------------------------------
BTPickUpItem::BTPickUpItem()
{

}

BTPickUpItem::~BTPickUpItem()
{

}

void BTPickUpItem::Initialise()
{
	
}

BTNode::BTResult BTPickUpItem::Process()
{
	printf("Item picked up");

	return BTNode::SUCCESS;
}

void BTPickUpItem::Deinitialise()
{
	throw std::logic_error("The method or operation is not implemented.");
}


//----------------------------------------

BTAttackEnemy::BTAttackEnemy()
{

}

BTAttackEnemy::~BTAttackEnemy()
{

}

void BTAttackEnemy::Initialise()
{
	throw std::logic_error("The method or operation is not implemented.");
}

BTNode::BTResult BTAttackEnemy::Process()
{
	Log::Message("ATTACK!", Log::NORMAL);
	return BTNode::SUCCESS;
}

void BTAttackEnemy::Deinitialise()
{
	throw std::logic_error("The method or operation is not implemented.");
}

BTWander::BTWander()
{
	width = ServiceLocator<WorldSystem>::GetService()->GetWidth();
	height = ServiceLocator<WorldSystem>::GetService()->GetHeight();
	srand((unsigned int)std::time(0));
}

BTWander::~BTWander()
{

}

BTNode::BTResult BTWander::Process()
{
	int x = rand() % width, y = rand() % height;
	if (ServiceLocator<AISystem>::GetService()->HasBeenTo(x, y) || !ServiceLocator<WorldSystem>::GetService()->IsWalkable(x, y))
	{
		return BTNode::RUNNING;
	}
	else
	{
		ControllerComponent* controller = m_tree->GetData()->GetComponent<ControllerComponent>(Component::CONTROLLER);
		if (controller)
		{
			controller->SetTargetPosition(ServiceLocator<AISystem>::GetService()->GetPositionFromNode(x, y));
			controller = nullptr;
			return BTNode::SUCCESS;
		}
		else
		{
			Log::Message("Wander node failed.", Log::ERROR);
			return BTNode::FAILURE;
		}
	}
}

void BTWander::Initialise()
{
	
}

void BTWander::Deinitialise()
{
}


/************************************************************************/
/*						Gnoll Behaviours                                */
/************************************************************************/
BTLookForGnolls::BTLookForGnolls()
{

}

BTLookForGnolls::~BTLookForGnolls()
{

}

BTLeafNode::BTResult BTLookForGnolls::Process()
{
	GameObject* me = m_tree->GetData();
	std::vector<GameObject*> objects = m_objectManager->GetObjectsWithinRadius(me->position, 2000);
	int o = 0;
	while (o < objects.size())
	{
		if (objects[o]->GetType() != GameObject::GNOLL || objects[o] == me)
			objects.erase(objects.begin() + o);
		else
			o++;
	}
	o = 0;
	GameObject* target = nullptr;
	float distance = 0;
	std::vector<GameObject*> group = m_script->GetGroup();
	int g, gC = group.size();
	while (o < objects.size())
	{
		bool inGroup = false;
		for (g = 0; g < gC; g++)
		{
			if (objects[o] == group[g])
			{
				inGroup = true;
				break;
			}
		}
		if (!inGroup)
		{
			float oDistance = math::Distance(me->position, objects[o]->position);
			if (oDistance < distance || distance == 0)
			{
				distance = oDistance;
				target = objects[o];
			}
		}
		o++;
	}

	if (target)
	{
		m_tree->GetData()->GetComponent<ControllerComponent>(Component::CONTROLLER)->SetTargetObject(target);
		m_tree->GetData()->GetComponent<ControllerComponent>(Component::CONTROLLER)->SetTargetPosition(target->position);
		m_script->DontIgnoreDamage();
		return BTResult::SUCCESS;
	}
	else
		m_script->IgnoreDamage();
	return BTResult::FAILURE;
}

void BTLookForGnolls::Initialise()
{
	m_objectManager = ServiceLocator<ObjectManager>::GetService();
	m_script = m_tree->GetData()->GetComponent<GnollScript>(Component::SCRIPT);
}

void BTLookForGnolls::Deinitialise()
{
	m_script = nullptr;
	m_objectManager = nullptr;
}

BTGnollWander::BTGnollWander()
{

}

BTGnollWander::~BTGnollWander()
{

}

void BTGnollWander::Initialise()
{
	m_controller = m_tree->GetData()->GetComponent<ControllerComponent>(Component::CONTROLLER);
 	m_aiSystem = ServiceLocator<AISystem>::GetService();
	m_world = ServiceLocator<WorldSystem>::GetService();
	m_me = m_tree->GetData();
	m_direction = m_targetPosition = sf::Vector2f(-1, -1);
	m_pathIndex = 1;
}

void BTGnollWander::Deinitialise()
{
	m_controller = nullptr;
	m_direction = m_targetPosition = sf::Vector2f(-1, -1);
	m_controller = nullptr; 
	m_aiSystem = nullptr;
	m_world = nullptr;
	m_me = nullptr;
	m_pathIndex = -1;
	m_path.clear();
}

BTLeafNode::BTResult BTGnollWander::Process()
{
	if (m_targetPosition == sf::Vector2f(-1, -1))
	{
		m_target = nullptr;
		while (m_target == nullptr)
		{
			m_targetPosition = sf::Vector2f(rand() % m_world->GetWidth() + 1, rand() % m_world->GetHeight() + 1);
			m_target = &m_world->GetNode(m_targetPosition);
			if (!m_target->IsWalkable())
				m_target = nullptr;
		}
	}
	else if (m_path.size() == 0)
		m_path = m_aiSystem->GetPath(&m_world->GetNode(m_world->GraphicsToWorldScale(m_world->SnapToGrid(m_me->position))), m_target);
	else
	{
		sf::Vector2f snapPos = m_world->SnapToGrid(m_me->position);
		sf::Vector2f subTarget = m_world->WorldToGraphicsScale((sf::Vector2f)m_path[m_path.size() - m_pathIndex]->GetPosition());
		ControllerComponent::EResult result = m_controller->Move(subTarget);
		if (result == ControllerComponent::EResult::SUCCESS)
		{
			m_pathIndex++;
			if (m_pathIndex >= m_path.size())
			{
				m_pathIndex = 1;
				m_path.clear();
				m_targetPosition = sf::Vector2f(-1, -1);
			}
		}
	}
	return BTNode::RUNNING;
}

void BTAttackHero::Initialise()
{
	m_script = m_tree->GetData()->GetComponent<GnollScript>(Component::SCRIPT);
	m_attackRate = m_script->GetAttackRate();
	m_hero = ServiceLocator<ObjectManager>::GetService()->GetObjectWithType(GameObject::WARRIOR);
	if (m_hero)
		m_heroController = m_hero->GetComponent<ControllerComponent>(Component::CONTROLLER);
}

void BTAttackHero::Deinitialise()
{
}

BTNode::BTResult BTAttackHero::Process()
{
	Collider* c = m_script->GetOwner()->GetComponent<Collider>(Component::COLLIDER);
	if (!m_hero || !m_hero->GetActive())
		m_script->UnSpotHero();
	else
	{
		float distance = math::Distance(m_hero->position, m_tree->GetData()->position);
		m_script->GetOwner()->LookTowards(m_hero->position);
		if (distance > 50)
			return BTNode::FAILURE;
		if (m_lastAttack > m_attackRate)
		{
			ServiceLocator<Factory>::GetService()->CreateGameObject(GameObject::TEXTEFFECT, m_tree->GetData()->position, std::to_string((int)m_script->GetDamage()));
			m_heroController->Damage(m_script->GetDamage());
			m_lastAttack = 0;
		}

		m_lastAttack += ServiceLocator<Time>::GetService()->GetDeltaTime();
		return BTResult::SUCCESS;
	}
	return BTResult::FAILURE;
}
