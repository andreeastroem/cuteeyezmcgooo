#include "stdafx.h"

#include "GameObject.h"

#include "Component.h"

Component::Component()
{

}

Component::~Component()
{

}

void Component::Update()
{

}

void Component::Cleanup()
{
	if (m_owner)
		m_owner = nullptr;
}

void Component::SetOwnerObject(GameObject* pObject)
{
	m_owner = pObject;
}

bool Component::GetActive()
{
	return m_activeFlag;
}

void Component::SetActive(bool pState)
{
	m_activeFlag = pState;
}

GameObject* Component::GetOwner()
{
	return m_owner;
}

void Component::OnCollision(GameObject* pOther)
{
	(void)pOther;
}

void Component::ExitCollision(GameObject* pOther)
{
	(void)pOther;
}

Component::EType Component::GetType()
{
	return m_type;
}

void Component::SetType(EType pType)
{
	m_type = pType;
}

void Component::SetPosition(sf::Vector2f pPosition)
{
	(void)pPosition;
}

void Component::SetRotation(float pRotation)
{
	(void)pRotation;
}
