
#pragma once

#include <memory>
#include "Box2D.h"
#include "GameObject.h"
#include "RenderSystem.h"

class Component;

class Factory
{
public:
	enum EStatus
	{
		FAILURE,
		SUCCESS
	};
public:
	typedef std::unique_ptr<Factory> ptr;
	static ptr Create();

	~Factory();

	GameObject* CreateGameObject(GameObject::EObjectType pType, sf::Vector2f pPosition, std::string pName = "");

	void Setb2World(b2World* pWorld);

	Component* CreateRenderComponent(GameObject::EObjectType pType, int pTileSize, RenderSystem::EViewport view, sf::Vector2f pPosition = sf::Vector2f());
	Component* CreateControllerComponent(GameObject::EObjectType pType);
	Component* CreateColliderComponent(GameObject::EObjectType pType, sf::Vector2f pPosition, int pTileSize);
	Component* CreateScriptComponent(GameObject::EObjectType pType, GameObject* pActor, sf::Vector2f pStartPosition = sf::Vector2f());

	//Object seperating scripts
private:
	Factory();

	b2World* m_world;
};