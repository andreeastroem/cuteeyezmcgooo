/************************************************************************/
/*							David	Forssell                            */
/************************************************************************/

#pragma once
#ifndef EVENTS_H_
#define EVENTS_H_
#include "BTNode.h"
#include <functional>

/************************************************************************/
/*                         Task  System                                 */
/************************************************************************/
class TaskBase
{
public:
	virtual void Run(){};
};

class TaskS0 : public TaskBase
{
public:
	typedef void(*FncType)();
	FncType m_fnc;
	virtual void Run(){ (*m_fnc)(); };
	TaskS0(FncType p_fnc) : m_fnc(p_fnc){};
};

template <typename TArg0>
class TaskS1 : public TaskBase
{
public:
	typedef void(*FncType)(TArg0);
	FncType m_fnc;
	TArg0 m_arg0;
	virtual void Run(){ (*m_fnc)(m_arg0); };
	TaskS1(FncType p_fnc, TArg0 p_arg0) :m_fnc(p_fnc), p_arg0(m_arg0){};
};

template <typename TObj>
class TaskM0 : public TaskBase
{
public:
	typedef void(TObj::*FncType)();
	FncType m_fnc;
	TObj* m_obj;
	virtual void Run(){ (m_obj->*m_fnc)(); };
	TaskM0(TObj* p_obj, FncType p_fnc) :m_obj(p_obj), m_fnc(p_fnc){};
};

template <typename TObj, typename TArg0>
class TaskM1 : public TaskBase
{
public:
	typedef void(TObj::*FncType)(TArg0);
	FncType m_fnc;
	TObj* m_obj;
	TArg0 m_arg0;
	virtual void Run(){ (m_obj->*m_fnc)(m_arg0); };
	TaskM1(TObj* p_obj, FncType p_fnc, TArg0 p_arg0) :m_obj(p_obj), m_fnc(p_fnc), m_arg0(p_arg0){};
};

template < typename TObj, typename TArg0, typename TArg1 >
class TaskM2 : public TaskBase
{
public:
	typedef void(TObj::*FncType)(TArg0, TArg1);
	FncType m_fnc;
	TObj* m_obj;
	TArg0 m_arg0;
	TArg1 m_arg1;
	virtual void Run(){ (m_obj->*m_fnc)(m_arg0, m_arg1); };
	TaskM2(TObj* p_obj, FncType p_fnc, TArg0 p_arg0, TArg1 p_arg1) :m_obj(p_obj), m_fnc(p_fnc), m_arg0(p_arg0), m_arg1(p_arg1){};
};

template <typename TFunc>
class TaskL0 : public TaskBase
{
public:
	TFunc m_func;
	TaskL0(TFunc p_f) :m_func(p_f){};
	virtual void Run(){ m_func(); };
};

template <typename TFunc, typename TArg0>
class TaskL1 : public TaskBase
{
public:
	TFunc m_func;
	TArg0 m_arg0;
	TaskL1(TFunc p_func, TArg0 p_arg0) :m_func(p_func), m_arg0(p_arg0){};
	virtual void Run(){ m_func(m_arg0); };
};



namespace ke
{
	/************************************************************************/
	/*                         Event System                                 */
	/************************************************************************/

	template <typename TArg>
	class Event
	{
	public:
		Event(){};
		~Event(){};
		virtual void Run(TArg* p_arg){};
	};

	template <typename TArg>
	class StaticEvent : public Event<TArg>
	{
	public:
		typedef void(*Func)(TArg*);
		Func m_func;
		void Run(TArg* p_args){ m_func(p_args); };
		StaticEvent(Func p_func) :m_func(p_func){};
	};

	template <typename TObj, typename TArg >
	class MemberEvent : public Event<TArg>
	{
	public:
		typedef void(TObj::*Func)(TArg*);
		Func m_func;
		TObj* m_obj;
		void Run(TArg* p_args){ (m_obj->*m_func)(p_args); };
		MemberEvent(TObj* p_obj, Func p_func) :m_obj(p_obj), m_func(p_func){};
	};

	class EventArgs
	{
	public:
		virtual char* ToString() { return "I'm an EventArgs!"; };
		EventArgs(){};
		~EventArgs(){};
	protected:
	private:
	};


	template <typename TArg>
	class EventHandler
	{
		Event<TArg>** m_functions;
		int m_funcSize;

	public:
		typedef void(*Func)(TArg*);
		EventHandler(){ m_funcSize = 0; };
		~EventHandler(){};

		void operator()(TArg* p_arg)
		{
			int f, fS = m_funcSize;
			for (f = 0; f < fS; f++)
				m_functions[f]->Run(p_arg);
		};

		template<typename TObj>
		void Add(void(TObj::*mFunc)(TArg*), TObj* p_obj)
		{
			if (m_funcSize == 0)
			{
				m_functions = new Event<TArg>*[m_funcSize + 1];
				m_functions[m_funcSize++] = new MemberEvent<TObj, TArg>(p_obj, mFunc);
			}
			else
			{
				Event<TArg>** tempFuncs = m_functions;
				m_functions = new Event<TArg>*[m_funcSize + 1];
				memcpy(m_functions, tempFuncs, sizeof(Event<TArg>*) * m_funcSize);
				m_functions[m_funcSize++] = new MemberEvent<TObj, TArg>(p_obj, mFunc);
			}
		};
		void Add(Func p_function)
		{
			if (m_funcSize == 0)
			{
				m_functions = new Event<TArg>*[m_funcSize + 1];
				m_functions[m_funcSize++] = new StaticEvent<TArg>(p_function);
			}
			else
			{
				Event<TArg>** tempFuncs = m_functions;
				m_functions = new Event<TArg>*[m_funcSize + 1];
				memcpy(m_functions, tempFuncs, sizeof(Event<TArg>*) * m_funcSize);
				m_functions[m_funcSize++] = new StaticEvent<TArg>(p_function);
			}
		};
		void operator+= (Func p_function)
		{
			Add(p_function);
		}

	};
}


class BehaviourTaskBase
{
public:
	virtual BTNode::BTResult Run(){ return BTNode::FAILURE; };
};

template <typename TFunc>
class BTTaskL0 : public BehaviourTaskBase
{
public:
	TFunc m_func;
	BTTaskL0(TFunc p_f) : m_func(p_f){};
	virtual BTNode::BTResult Run(){ return m_func(); };
};

template <typename TFunc, typename TArg0>
class BTTaskL1 : public BehaviourTaskBase
{
public:
	TFunc m_func;
	TArg0 m_arg0;
	BTTaskL1(TFunc p_f, TArg0 p_arg0) : m_func(p_f), m_arg0(p_arg0){};
	virtual BTNode::BTResult Run(){ return m_func(m_arg0); };
};


template <typename TFunc, typename TArg0, typename TArg1>
class BTTaskL2 : public BehaviourTaskBase
{
public:
	TFunc m_func;
	TArg0 m_arg0;
	TArg1 m_arg1;
	BTTaskL2(TFunc p_f, TArg0 p_arg0, TArg1 p_arg1) : m_func(p_f), m_arg0(p_arg0), m_arg1(p_arg1){};
	virtual BTNode::BTResult Run(){ return m_func(m_arg0); };
};


template <typename TObj>
class BTTaskM0 : public BehaviourTaskBase
{
public:
	typedef BTNode::BTResult(TObj::*FncType)();
	FncType m_fnc;
	TObj* m_obj;
	virtual BTNode::BTResult Run(){ return (m_obj->*m_fnc)(); };
	BTTaskM0(TObj* p_obj, FncType p_fnc) :m_obj(p_obj), m_fnc(p_fnc){};
};

template <typename TObj, typename TArg0>
class BTTaskM1 : public BehaviourTaskBase
{
public:
	typedef BTNode::BTResult(TObj::*FncType)(TArg0);
	FncType m_fnc;
	TObj* m_obj;
	TArg0 m_arg0;
	virtual BTNode::BTResult Run(){ return (m_obj->*m_fnc)(m_arg0); };
	BTTaskM1(TObj* p_obj, FncType p_fnc, TArg0 p_arg0) :m_obj(p_obj), m_fnc(p_fnc), m_arg0(p_arg0){};
};

template < typename TObj, typename TArg0, typename TArg1 >
class BTTaskM2 : public BehaviourTaskBase
{
public:
	typedef BTNode::BTResult(TObj::*FncType)(TArg0, TArg1);
	FncType m_fnc;
	TObj* m_obj;
	TArg0 m_arg0;
	TArg1 m_arg1;
	virtual BTNode::BTResult Run(){ return(m_obj->*m_fnc)(m_arg0, m_arg1); };
	BTTaskM2(TObj* p_obj, FncType p_fnc, TArg0 p_arg0, TArg1 p_arg1) :m_obj(p_obj), m_fnc(p_fnc), m_arg0(p_arg0), m_arg1(p_arg1){};
};

#endif